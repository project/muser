<?php

declare(strict_types=1);

namespace Drupal\oneclick_unsubscribe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for One-Click Unsubscribe routes.
 */
final class UnsubscribeController extends ControllerBase {

  /**
   * Builds the response to the unsubscribe post.
   */
  public function unsubscribe(Request $request): RedirectResponse|array {

    $post_vars = $request->request->all();
    $get_vars = $request->query->all();
    $is_one_click = ($request->request->get('List-Unsubscribe') == 'One-Click');
    if (!$is_one_click) {
      // If it's not a One-Click unsubscribe request, redirect to the form.
      return $this->redirect('oneclick_unsubscribe.unsubscribe_form', [], ['query' => $get_vars]);
    }

    $mail = $get_vars['mail'] ?? '';
    $checksum = $get_vars['checksum'] ?? '';

    $unsubscribe = [
      'mail' => $mail,
      'checksum' => $checksum,
      'match' => ($checksum == oneclick_unsubscribe_get_checksum($mail, $get_vars, $post_vars)),
      'success' => FALSE,
      'message' => $this->t('The email address %email has been successfully unsubscribed.', ['%email' => $mail]),
      'invalid' => $this->t('The request to unsubscribe the email address %email was invalid.', ['%email' => $mail]),
      'error' => $this->t('There was an error unsubscribing the email address %email.', ['%email' => $mail]),
    ];

    // Allow modules to perform the unsubscribe.
    $this->moduleHandler()->alter('oneclick_unsubscribe_unsubscribe', $unsubscribe, $get_vars, $post_vars);

    $messages = [];
    if (empty($unsubscribe['match'])) {
      $messages['error'][] = $unsubscribe['invalid'];
    }
    elseif (empty($unsubscribe['success'])) {
      $messages['error'][] = $unsubscribe['error'];
    }
    else {
      $messages['status'][] = $unsubscribe['message'];
    }

    if (!empty($post_vars['redirect'])) {
      // We're coming from our own form-- redirect after setting messages.
      foreach ($messages as $type => $values) {
        foreach ($values as $message) {
          $this->messenger()->addMessage($message, $type);
        }
      }
      return $this->redirect('<front>');
    }

    $build['messages'] = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
    ];

    return $build;

  }

}
