<?php

namespace Drupal\muser_project\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Provides a 'ApplicationReviewHeader' block.
 *
 * @Block(
 *  id = "muser_application_review_header_block",
 *  admin_label = @Translation("Muser Application review header block"),
 * )
 */
class ApplicationReviewHeader extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * @var integer
   */
  protected $current_round_nid;

  /**
   * ApplicationReviewHeader constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CurrentRouteMatch $route_match
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => ['current_round'],
        'contexts' => ['user'],
      ],
    ];
    if ($round_nid = muser_project_get_current_round()) {
      if ($round = Node::load($round_nid)) {
        $this->current_round_nid = $round_nid;
      }
    }

    if ($uid = $this->routeMatch->getParameter('user')) {
      $account = User::load($uid);
    }

    if (empty($round) || empty($account)) {
      return $build;
    }

    $build['dates'] = $round->field_accept_applications->get(0)->view('default');
    $build['dates']['#prefix'] = '<div class="field field--name-field-accept-applications field--type-daterange field--label-above">'
      . '<div class="field__label">'
      . $this->t('@title application-review period', ['@title' => $round->label()])
      . '</div><div class="field__item">';
    $build['dates']['#suffix'] = '</div></div>';

    $build['#attributes']['data-applications-per-status'] = "{}";

    if (muser_project_allow_application_access($this->current_round_nid)) {

      if (!$counts = $this->getApplicationCounts($account)) {
        $text = $this->t('You have no submitted applications for your Projects.');
      }
      else {
        $build['#attributes']['data-applications-per-status'] = json_encode([
          'accepted' => $counts->accepted,
          'declined' => $counts->declined,
          'in_review' => $counts->in_review,
          'pending' => $counts->pending,
        ]);
        $build['#attached']['library'][] = 'muser_project/application-counts';

        $text = '';
        if ($info = $this->getCountText($counts, $account)) {
          if (!empty($info['text'])) {
            $text .= '<p>' . $info['text'] . '</p>';
          }
          if (!empty($info['todo'])) {
            $text .= '<p>' . $info['todo'] . '</p>';
          }
          if (!empty($info['downloads'])) {
            $text .= '<p>' . $info['downloads'];
            if (!empty($info['project_downloads'])) {
              $text .= ' <button class="download-by-project">' . $this->t('By project') . '</button>';
            }
            $text .= '</p>';
            if (!empty($info['project_downloads'])) {
              $text .= '<p class="download-by-project" style="display:none;">' . $info['project_downloads'] . '</p>';
            }
          }
        }
      }

      if ($text) {
        $build['count'] = [
          '#type' => 'inline_template',
          '#template' => '<div class="application-review-text">'
            . $text
            . '</div>',
        ];
      }

    } // In the correct period?

    $build['#cache'] = [
      'tags' => [
        'current_round',
        'node:' . $round_nid,
        'application_review_count:' . $account->id(),
      ],
      'contexts' => ['user'],
      'max-age' => muser_project_round_period_change_time('acceptance', $round_nid),
    ];
    $this->addEntityCacheTags($build['#cache']['tags'], $account);
    return $build;

  }

  /**
   * Return count of application with different status values.
   *
   * @param \Drupal\user\Entity\User $account
   *   Account to report on.
   *
   * @return mixed
   *   Object with counts.
   */
  protected function getApplicationCounts(User $account) {

    $query = \Drupal::database()->select('muser_applications_counts', 'mac');
    $query->join('node_field_data', 'nfd', 'nfd.nid = mac.project_nid');
    $query->addField('mac', 'project_nid');
    $query->addField('nfd', 'title');
    $query->addExpression('SUM(mac.submitted)', 'submitted');
    $query->addExpression('SUM(mac.pending)', 'pending');
    $query->addExpression('SUM(mac.in_review)', 'in_review');
    $query->addExpression('SUM(mac.accepted)', 'accepted');
    $query->addExpression('SUM(mac.declined)', 'declined');
    $query->addExpression('SUM(mac.no_decision)', 'no_decision');
    $query->condition('mac.round_nid', $this->current_round_nid);
    // Include the projects where they're an "Additional mentor", too.
    $subquery = \Drupal::database()->select('node__field_additional_mentors', 'fam');
    $subquery->addField('fam', 'entity_id');
    $subquery->condition('fam.field_additional_mentors_target_id', $account->id());
    $conditions = $query->orConditionGroup()
      ->condition('mac.project_uid', $account->id())
      ->condition('mac.project_nid', $subquery, 'IN');
    $query->condition($conditions);
    $query->groupBy('mac.project_nid');
    $query->groupBy('nfd.title');
    $query->orderBy('nfd.title');

    $counts = [
      'submitted' => 0,
      'pending' => 0,
      'in_review' => 0,
      'accepted' => 0,
      'declined' => 0,
      'no_decision' => 0,
      'projects' => [],
      'pending_projects' => 0,
      'in_review_projects' => 0,
    ];
    $counts = (object) $counts;
    $result = $query->execute();
    foreach ($result as $row) {
      $counts->projects[$row->project_nid] = new \Stdclass();
      foreach ($row as $key => $value) {
        $counts->projects[$row->project_nid]->{$key} = $value;
      }
      $counts->submitted += $row->submitted;
      $counts->pending += $row->pending;
      $counts->in_review += $row->in_review;
      $counts->accepted += $row->accepted;
      $counts->declined += $row->declined;
      $counts->no_decision += $row->no_decision;
      if ($counts->pending) {
        $counts->pending_projects++;
      }
      if ($counts->in_review) {
        $counts->in_review_projects++;
      }
    }
    return $counts;
  }

  /**
   * Add cache tags for project nodes and flaggings (applications).
   *
   * @param array $cache_tags
   *   Existing cache tags array.
   * @param \Drupal\user\Entity\User $account
   *   Current user.
   */
  protected function addEntityCacheTags(array &$cache_tags, User $account) {
    $query = \Drupal::database()->select('muser_applications', 'ma');
    $query->addField('ma', 'fid');
    $query->addField('ma', 'project_nid');
    $query->condition('ma.round_nid', $this->current_round_nid);
    $query->condition('ma.is_submitted', 1);
    // Include the projects where they're an "Additional mentor", too.
    $subquery = \Drupal::database()->select('node__field_additional_mentors', 'fam');
    $subquery->addField('fam', 'entity_id');
    $subquery->condition('fam.field_additional_mentors_target_id', $account->id());
    $conditions = $query->orConditionGroup()
      ->condition('ma.project_uid', $account->id())
      ->condition('ma.project_nid', $subquery, 'IN');
    $query->condition($conditions);
    $result = $query->execute();
    foreach ($result as $row) {
      // Add a cache tag for the application (for status changes).
      $cache_tags[] = 'flagging:' . $row->fid;
      if (!in_array('node:' . $row->project_nid, $cache_tags)) {
        // Add a cache tag for the project (mentor added).
        $cache_tags[] = 'node:' . $row->project_nid;
      }
    } // Loop thru rows.
  }

  /**
   * Return text describing counts.
   *
   * @param object $counts
   *   Array of count values.
   * @param \Drupal\user\Entity\User $account
   *   Mentor account.
   *
   * @return array
   *   Text to display.
   */
  protected function getCountText($counts, User $account) {

    if (empty($counts->submitted)) {
      return [];
    }

    $text = $this->formatPlural($counts->submitted,
      'You have 1 submitted application',
      'You have @count submitted applications')->__toString();
    $downloads_text = '';
    $project_downloads_text = '';

    $types = [];
    $downloads = [];
    $project_downloads = [];
    if (!empty($counts->pending)) {
      $types[] = Link::createFromRoute($this->t('@count pending', ['@count' => $counts->pending]), 'view.applications.page_new', ['user' => $account->id()])
        ->toString();
      $downloads[] = Link::createFromRoute($this->t('Download @count pending', ['@count' => $counts->pending]), 'muser_system.export_applications', ['stage' => 'pending'])
        ->toString();
    }
    if (!empty($counts->in_review)) {
      $types[] = Link::createFromRoute($this->t('@count in review', ['@count' => $counts->in_review]), 'view.applications.page_review', ['user' => $account->id()])
        ->toString();
      $downloads[] = Link::createFromRoute($this->t('Download @count in review', ['@count' => $counts->in_review]), 'muser_system.export_applications', ['stage' => 'in-review'])
        ->toString();
    }
    if ($counts->pending_projects > 1) {
      foreach ($counts->projects as $project) {
        if ($project->pending) {
          $project_downloads[] = Link::createFromRoute($this->t('Download @count pending for %title', [
            '@count' => $project->pending,
            '%title' => $project->title,
          ]), 'muser_system.export_applications',
            ['stage' => 'pending'],
            ['query' => ['project' => $project->project_nid]])
            ->toString();
        }
      } // Loop thru projects.
    }
    if ($counts->in_review_projects > 1) {
      foreach ($counts->projects as $project) {
        if ($project->in_review) {
          $project_downloads[] = Link::createFromRoute($this->t('Download @count in review for %title', [
            '@count' => $project->in_review,
            '%title' => $project->title,
          ]), 'muser_system.export_applications',
            ['stage' => 'in-review'],
            ['query' => ['project' => $project->project_nid]])
            ->toString();
        }
      } // Loop thru projects.
    }

    if (!empty($counts->accepted)) {
      $types[] = Link::createFromRoute($this->t('@count accepted', ['@count' => $counts->accepted]), 'view.applications.page_accepted', ['user' => $account->id()])
        ->toString();
    }
    if (!empty($counts->declined)) {
      $types[] = Link::createFromRoute($this->t('@count declined', ['@count' => $counts->declined]), 'view.applications.page_declined', ['user' => $account->id()])
        ->toString();
    }
    if ($types) {
      $text .= ' - ' . implode(', ', $types);
    }
    if ($downloads) {
      $downloads_text = '<i class="far fa-file-pdf"></i> ' . implode(', ', $downloads);
    }
    if ($project_downloads) {
      $project_downloads_text = '<i class="far fa-file-pdf"></i> ' . implode('<br><i class="far fa-file-pdf"></i> ', $project_downloads);
    }

    if (!empty($counts->no_decision)) {
      $todo = $this->formatPlural($counts->no_decision,
        'You still need to make a decision on (accept or decline) 1 application.',
        'You still need to make decisions on (accept or decline) @count applications.')
        ->__toString();
    }
    else {
      $todo = $this->t('You have completed your application review. <em>Well done!</em> Students will see a gold star next to your name for any projects that you post during the next round.')->__toString();
    }

    return [
      'text' => $text,
      'todo' => $todo,
      'downloads' => $downloads_text,
      'project_downloads' => $project_downloads_text,
    ];

  }

}
