<?php

namespace Drupal\muser_system\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\muser_system\RoundDatesTrait;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RoundInfo' block.
 *
 * @Block(
 *  id = "muser_round_info_block",
 *  admin_label = @Translation("Muser round info block"),
 * )
 */
class RoundInfo extends BlockBase implements ContainerFactoryPluginInterface {

  use RoundDatesTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * RoundInfo constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => ['current_round', 'user.roles'],
      ],
    ];
    if ($round_nid = muser_project_get_current_round()) {
      $round = Node::load($round_nid);
    }

    if (empty($round)) {
      if ($this->currentUser->hasPermission('create round content')) {
        $manage_url = Url::fromRoute('view.rounds.page_1')->toString();
        $create_url = Url::fromRoute('node.add', ['node_type' => 'round'])->toString();
        $build['my_projects'] = [
          '#theme' => 'status_messages',
          '#message_list' => [
            'warning' => [
              $this->t('There is no <em>current Round</em> set. You may <a href="@manage_url">manage Rounds</a> or <a href="@create_url">create a new Round</a>.', [
                '@manage_url' => $manage_url,
                '@create_url' => $create_url,
              ]),
            ],
          ],
          '#cache' => [
            'tags' => ['current_round', 'user.roles'],
          ],
        ];
      }
      return $build;
    }

    if (_muser_system_contracts_enabled(FALSE)) {
      if ($this->currentUser->hasPermission('create round content')) {
        if (!$round->get('field_sign_contracts')->get(0)) {
          $update_url = Url::fromRoute('entity.node.edit_form', ['node' => $round->id()])
            ->toString();
          $build['contracts_error'] = [
            '#theme' => 'status_messages',
            '#message_list' => [
              'error' => [
                $this->t('Expectations and Structure agreements are enabled, but related dates are not set for the current Round. You should <a href="@update_url">update the current Round</a> to set the these dates.', [
                  '@update_url' => $update_url,
                ]),
              ],
            ],
            '#cache' => [
              'tags' => [
                'node:' . $round_nid,
                'current_round',
                'user.roles',
              ],
            ],
          ];
        } // Contract dates set?
      } // Can they manage rounds?
    } // Contracts enabled?

    $build[] = [
      '#theme' => 'muser_round_info',
      '#title' => $this->t('Timeline for @title', ['@title' => $round->label()]),
      '#dates' => $this->getDates($round),
      '#mentor_title' => $this->t('For Research Mentors'),
      '#student_title' => $this->t('For Students'),
      '#show_contract_dates' => _muser_system_contracts_enabled(),
      '#cache' => [
        'tags' => [
          'node:' . $round_nid,
          'current_round',
          'user.roles',
        ],
      ],
    ];

    $renderer = \Drupal::service('renderer');
    $renderer->addCacheableDependency($build, $round);

    return $build;

  }

}
