<?php

declare(strict_types=1);

namespace Drupal\muser_system\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flag\Controller\ActionLinkController;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagServiceInterface;
use Drupal\muser_project\RelatedProjects;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Overrides Flag's controller.
 */
final class FlagController extends ActionLinkController {

  use StringTranslationTrait;

  /**
   * The flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flagService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The related projects service.
   *
   * @var \Drupal\muser_project\RelatedProjects
   */
  private $relatedProjects;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructor.
   *
   * @param \Drupal\flag\FlagServiceInterface $flag
   *   The flag service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\muser_project\RelatedProjects $related_projects
   *   The related projects service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(FlagServiceInterface $flag, RendererInterface $renderer, RequestStack $request_stack, RelatedProjects $related_projects, AccountInterface $current_user) {
    parent::__construct($flag, $renderer);
    $this->flagService = $flag;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
    $this->relatedProjects = $related_projects;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('flag'),
      $container->get('renderer'),
      $container->get('request_stack'),
      $container->get('muser_project.related_projects'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function flag(FlagInterface $flag, $entity_id, ?string $view_mode = NULL): ?AjaxResponse {

    $response = parent::flag($flag, $entity_id, $view_mode);
    $is_related = $this->requestStack->getCurrentRequest()->query->get('related');

    if ($flag->id() === 'favorites') {

      $query = \Drupal::database()->select('muser_applications', 'ma');
      $or_group = $query->orConditionGroup();

      // Does the current user have any favorites yet this round?
      $group = $query->andConditionGroup()
        ->condition('ma.application_uid', $this->currentUser->id())
        ->condition('ma.round_nid', muser_project_get_current_round())
        ->condition('ma.project_round_nid', $entity_id, '<>');
      $or_group->condition($group);
      // Or any favorites that they submitted an application with in any round?
      $group = $query->andConditionGroup()
        ->condition('ma.application_uid', $this->currentUser->id())
        ->condition('ma.is_submitted', TRUE);
      $or_group->condition($group);
      $query->condition($or_group);
      $count = $query->countQuery()
        ->execute()
        ->fetchField();

      if ($count == 0) {
        $config = \Drupal::config('muser_system.settings');
        $title = $config->get('application_process_instructions_title') ?? "";
        $content = $config->get('application_process_instructions_content') ?? [];
        if (!empty($content['value'])) {
          $dialog_options = [
            'minWidth' => 500,
            'resizable' => TRUE,
          ];
          $response->addCommand(new OpenModalDialogCommand($title, check_markup($content['value'], $content['format']), $dialog_options));
        }
      }
    }

    if ($flag->id() !== 'favorites' || $is_related) {
      return $response;
    }

    if ($projects = $this->relatedProjects->getRelatedProjects($entity_id, $flag, $this->currentUser)) {
      $view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['view-content'],
        ],
        'projects' => $projects['list'],
      ];
      $content = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['related-projects__content view-id-projects view-display-id-page'],
        ],
        'view' => $view,
      ];
    }
    else {
      $content = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['related-projects__content view-id-projects view-display-id-page no-results'],
        ],
        'content' => [
          '#type' => 'markup',
          '#markup' => $this->t('No related projects found.'),
        ],
      ];
    }
    $selector = '#related-projects';
    $response->addCommand(new HtmlCommand($selector, $content));
    $selector = '#related-projects__reason';
    $response->addCommand(new HtmlCommand($selector, $projects['reason'] ?? ''));
    return $response;
  }

}
