<?php

/**
 * @file
 * Provide views data for muser_ab_testing.module.
 */

/**
 * Implements hook_views_data().
 */
function muser_ab_testing_views_data() {
  $data['muser_ab_testing_cohort_membership'] = [
    'table' => [
      'group' =>  t('Muser AB Testing'),
      'provider' =>  t('muser_ab_testing'),
      'title' => t('Muser Cohort Membership'),
      'help'  => t('Show which Cohorts each user is part of.'),
      'base' => [
        'field' => 'uid',
        'title' => t('A/B Test Cohort Members'),
      ],
      'join' => [
        'node_field_data' => [
          'left_field' => 'nid',
          'field' => 'test_nid',
        ],
      ],
    ],
    'uid' => [
      'title' => t('User ID'),
      'label' => t('User ID'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
      'relationship' => [
        'title' => t('User'),
        'help' => t('The user that belongs to the specified cohort.'),
        'base' => 'users_field_data',
        'base field' => 'uid',
        'id' => 'standard',
      ],
    ],
    'cohort' => [
      'title' => t('Cohort'),
      'label' => t('Cohort'),
      'help'  => t('The cohort to which the user belongs'),
      'field' => [
        'handler' => 'views_handler_field',
        'id' => 'standard',
      ],
      'filter' => [
        'field' => 'cohort',
        'id' => 'string',
      ],
      'argument' => [
        'field' => 'cohort',
        'id' => 'string',
      ],
    ],
    'test_nid' => [
      'title' => t('A/B Test'),
      'label' => t('A/B Test'),
      'help'  => t('The A/B Test of the Cohort.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
      'relationship' => [
        'title' => t('Node'),
        'label' => t('Node'),
        'group' => t('Node'),
        'help' => t('The A/B test node that to which the cohort belongs.'),
        'base' => 'node_field_data',
        'base field' => 'nid',
        'relationship field' => 'test_nid',
        'id' => 'standard',
      ],
    ],
    'create_timestamp' => [
      'title' => t('Timestamp'),
      'label' => t('Timestamp'),
      'help'  => t('When the cohort assignment occurred.'),
      'field' => [
        'id' => 'date',
      ],
      'filter' => [
        'id' => 'date',
      ],
      'argument' => [
        'id' => 'date',
      ],
      'sort' => [
        'id' => 'date',
      ],
    ],
  ];

  return $data;
}
