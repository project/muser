<?php

/**
 * @file
 * Hooks for the oneclick_unsubscrib module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to process unsubscribe requests.
 *
 * $data = [
 *   'mail' => Email address to unsubscribe,
 *   'checksum' => Checksum of email address
 *   'match' => TRUE if checksum matches
 *   'success' => TRUE if unsubscribe was successful,
 *   'message' => Message to show on success,
 *   'invalid' => Message to show if checksum is invalid
 *   'error' => Message to show if unsubscribe fails
 * ];
 *
 * @param array $data
 *   Array of data for processing the unsubscribe request (per above).
 * @param array $get_vars
 *   Array of all GET values.
 * @param array $post_vars
 *   Array of all POST values.
 */
function hook_oneclick_unsubscribe_unsubscribe_alter(&$data, &$get_vars, &$post_vars) {
  if (!empty($data['match'])) {
    $data['success'] = FALSE;
    if ($user = user_load_by_mail($data['mail'])) {
      $user->field_subscribed->value = FALSE;
      $user->save();
      $data['success'] = TRUE;
    }
  }
}

/**
 * Allow modules to alter / calculate checksum for message.
 *
 * @param string $checksum
 *   The checksum for the email address.
 * @param array $context1
 *   Array of additional information.
 * @param array $context2
 *   Array of additional information.
 */
function hook_oneclick_unsubscribe_checksum_alter(&$checksum, $context1, $context2 = []) {
  $checksum = md5($context1['mail'] . $context1['key']);
}

/**
 * @} End of "addtogroup hooks".
 */
