/**
 * @file
 * Project list functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserCompactApplications = {

    attach: function(context, settings) {

      let addConfirmation = function($view) {
        let $checkboxes = $view.find('.views-row > .form-item.form-type-checkbox input')
        let $actions = $view.find('[data-drupal-selector="edit-header"] .form-actions');
        let $confirm_buttons = $actions.find('[data-confirm-dialog-label]')
        let numCheckedItems = function() {
          let count = 0;
          $checkboxes.each(function() {count += ($(this).prop('checked') ? 1 : 0)});
          return count;
        };
        $(once('confirm-button-processed', $confirm_buttons)).each(function() {
          let $button = $(this);
          $button.on('click', function(e) {
            let checkedItems = numCheckedItems();
            if (checkedItems === 0) {
              alert(Drupal.t('No applications selected.'));
              e.preventDefault();
              return false;
            }
            let dialogText = Drupal.formatPlural(checkedItems,
              "Are you sure you want to set 1 selected item to @new_state?",
              "Are you sure you want to set @count selected items to @new_state?",
              {"@new_state": $button.attr('data-confirm-dialog-label')})
            if (!confirm(dialogText)) {
              e.preventDefault();
            }
          })
        })
      }

      let $ajax_replaced = $('.view-content', context);
      if ($ajax_replaced.length < 1 || $ajax_replaced.hasClass('compact-processed')) {
        return;
      }
      $ajax_replaced.addClass('compact-processed')

      let $view = $ajax_replaced.parent('.view-id-applications');
      let $actions = $view.find('[data-drupal-selector="edit-header"] .form-actions');
      let $switcher = $("<div class='view__applications-compact-switcher applications-compact-switcher'></div>");
      let $switch_to_full = $('<button class="applications-compact-switcher__full"><i class="fas fa-stop"></i>' + Drupal.t("Full") + '</button>');
      let $switch_to_compact = $('<button class="applications-compact-switcher__compact"><i class="fas fa-pause fa-rotate-90"></i>' + Drupal.t("Compact") + '</button>');
      let pref_state_cookie_name = "musercompactapplicationpreferredstate"
      if (Drupal.muserGetCookie(pref_state_cookie_name) === "compact") {
        $view.addClass('view--compact-applications');
      }

      $switch_to_full.on('click', function(e) {
        e.preventDefault();
        $view.removeClass('view--compact-applications');
        Drupal.muserSetCookie(pref_state_cookie_name, 'full', 365)
      })
      $switch_to_compact.on('click', function(e) {
        e.preventDefault();
        $view.addClass('view--compact-applications');
        Drupal.muserSetCookie(pref_state_cookie_name, 'compact', 365)
      })
      $switcher.append($switch_to_full)
      $switcher.append($switch_to_compact)
      $actions.append($switcher)
      addConfirmation($view);

    }
  }

  Drupal.behaviors.muserApplications = {
    attach: function (context, settings) {

      let num_submitted = 0;
      let is_changed = 0;
      $(once('application-status-processed', '.muser-project-user-application')).each(function () {
        let form_status = parseInt($(this).attr('data-submitted'));
        let form_nid = $(this).attr('data-project-nid');
        let $status = $('article.node--nid--' + form_nid).find('.project__application-status .application-status--submitted');
        let markup_status = ($status.length > 0 ? 1 : 0);
        if (markup_status === form_status) {
          return;
        }
        is_changed = 1;
        num_submitted = parseInt($(this).attr('data-submitted-count'));
        if (form_status === 0) {
          $status.parent().html('');
        }
        else {
          let status = $('article.node--nid--' + form_nid).find('.project__application-status');
          status.show();
          let status_markup = $('.submitted-markup--' + form_nid).html();
          status.html(status_markup);
        }
      });

      if (is_changed) {
        $('.block-muser-application-count .submitted-text .placeholder').first().html(num_submitted);
      }

      let application_body = $('.application__body', context);
      application_body.find('a, input, textarea').attr('tabindex', -1);

      let toggle = '<button class="toggle-application application--closed"><span><i class="fas fa-chevron-down"></i></span><span style="display:none;"><i class="fas fa-chevron-up"></i></span></button>';

      let setHeight = function (application) {
        let $application = $(application);
        let height = $application.find('.application__body-height').height();
        $application.find('.application__body').css({maxHeight: height});
      };

      let $applications = $('article.application-collapsible', context);
      $(once('application--processed', $applications)).addClass('application--closed').each(function () {
        $(this).find('.application__main').prepend(toggle);
        setHeight(this)
      });

      let resizing = false;
      $(window).resize(function () {
        if (resizing) {
          return;
        }
        else {
          resizing = true;
          $applications.each(function () {
            setHeight(this)
          });
          setTimeout(function () {
            resizing = false;
            $applications.each(function () {
              setHeight(this)
            });
          }, 100)
        }
      });

      var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
      var observer = new MutationObserver(mutationHandler);
      var obs_config = {childList: true, characterData: false, attributes: false, subtree: true};

      $('.application__essay-wrapper').each(function() {
        observer.observe (this, obs_config);
      });

      function mutationHandler (mutationRecords) {
        mutationRecords.forEach ( function (mutation) {
          if (resizing) {
            return;
          }
          else {
            let $applications = $(mutation.target).parents('article.application-collapsible');
            resizing = true;
            setTimeout(function () {
              resizing = false;
              $applications.each(function () {
                setHeight(this)
              });
            }, 100)
            $applications.each(function () {
              setHeight(this)
            });
          }
        });
      }


      $(once('application-open-processed', '.project__internal-wrapper, .application__body, .application__details-bar')).on('click', function(e) {
        let application_wrapper = $(this).parents('article.application-collapsible');
        if (application_wrapper.hasClass('application--open')) {
          // we only want to open the application by clicking anywhere.
          return;
        }
        setHeight(application_wrapper);
        let application_body = application_wrapper.find('.application__body');
        application_body.find('a, input, textarea').attr('tabindex', 0);

        // if ($(e.target)) {
        //   console.log('e.target', e.target);
        // }
        toggle_application(application_wrapper);
      });

      $(once('application--processed', '.toggle-application', context)).each(function () {
        $(this).on('click', function () {
          let application_wrapper = $(this).parents('article.application-collapsible');
          setHeight(application_wrapper);
          toggle_application(application_wrapper);
          return false;
        });
      });

      let toggle_application = function (application) {
        application.toggleClass('application--closed application--open');
        let toggle_application_button = application.find('.toggle-application');

        toggle_application_button.toggleClass('application--closed application--open')
          .find('span').toggle();

        let application_body = application.find('.application__body');

        if (toggle_application_button.hasClass('application--closed')) {
          application_body.find('a, input, textarea').attr('tabindex', -1);
        }
        else {
          application_body.find('a, input, textarea').attr('tabindex', 0);
        }

        return false;
      }

      // Open the item set in the URL hash.
      $(once('hash--processed', 'body')).each(function () {
        let hash = window.location.hash;
        if (hash.indexOf('#open__') !== -1) {
          let application_wrapper = $(hash);
          if (application_wrapper.length > 0) {
            toggle_application(application_wrapper);
            let toolbar1 = $('#toolbar-bar')
            let toolbar2 = $('.toolbar-tray.is-active.toolbar-tray-horizontal');
            let offset = application_wrapper.offset().top;
            if (toolbar1.length) {
              offset -= toolbar1.height();
            }
            if (toolbar2.length) {
              offset -= toolbar2.height();
            }
            $(document).scrollTop(offset);
          }
        }
      });

    }
  };
})(jQuery, Drupal, once);

