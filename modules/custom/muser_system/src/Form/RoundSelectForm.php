<?php

namespace Drupal\muser_system\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a round selector form.
 */
class RoundSelectForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'muser_system_round_select';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#method'] = 'get';
    _muser_project_form_views_exposed_form_create_round_select($form);
    if (!$round = \Drupal::request()->get('round')) {
      $round = muser_project_get_current_round();
    }
    $form['round']['#default_value'] = $round;
    $form['round']['#title'] = $this->t('Round');
    $form['#after_build'] = ['::afterBuild'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    // Remove the form_token, form_build_id and form_id from the GET parameters.
    unset($element['form_token']);
    unset($element['form_build_id']);
    unset($element['form_id']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
