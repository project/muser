<?php declare(strict_types = 1);

namespace Drupal\muser_ab_testing\Theme;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines a theme negotiator that deals with the active theme on example page.
 */
final class MuserAbTestingNegotiator implements ThemeNegotiatorInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Service constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configfactory.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool {
    $route = $route_match->getRouteObject();
    if (!$route instanceof Route) {
      return FALSE;
    }
    $routes_to_match = ['node.add', 'entity.node.edit_form'];
    if (in_array($route_match->getRouteName(), $routes_to_match)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match): ?string {
    if ($route_match->getRouteName() == 'node.add') {
      if ($entity_type_param = $route_match->getParameter('node_type')) {
        if ($entity_type_param->id() === 'ab_test') {
          return $this->configFactory->get('system.theme')->get('admin');
        }
      }
    }
    if ($route_match->getRouteName() == 'entity.node.edit_form') {
      if ($entity_param = $route_match->getParameter('node')) {
        if ($entity_param->bundle() === 'ab_test') {
          return $this->configFactory->get('system.theme')->get('admin');
        }
      }
    }
    return NULL;
  }
}
