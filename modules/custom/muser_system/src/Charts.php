<?php

namespace Drupal\muser_system;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\Attribute;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;

/**
 * Charts service used to build Google charts.
 */
class Charts {

  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Array of chart info.
   *
   * @var array
   */

  /**
   * Array of chart info.
   *
   * @var array
   */
  public $chartsInfo = [];

  /**
   * Colorset info for the site theme.
   *
   * @var array
   */
  public $colorset = [];

  /**
   * Current timezone.
   *
   * @var \DateTimeZone
   */
  public $timeZone;

  /**
   * Constructs a Charts object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler) {
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
    $this->getChartsInfo();
    $this->colorset = \Drupal::service('state')->get('muser_theme_colorset');
    $this->timeZone = new \DateTimeZone(date_default_timezone_get());
  }

  /**
   * Builds chart info array.
   */
  private function getChartsInfo(): void {
    if (!$this->chartsInfo) {
      $this->chartsInfo = $this->moduleHandler->invokeAll('muser_charts_info');
      // Sort the charts by label.
      uasort($this->chartsInfo, function ($a, $b) {
        return strcasecmp($a['label'], $b['label']);
      });
      $this->moduleHandler->alter('muser_charts_info', $this->chartsInfo);
    }
  }

  protected function getRoundInfo($query) {
    $info = [
      'is_current' => FALSE,
      'current_round' => muser_project_get_current_round(TRUE),
      'period' => '',
      'period_end' => '',
    ];
    if (!$info['round_nid'] = $query['round'] ?? 0) {
      $info['round_nid'] = ($info['current_round']) ? $info['current_round']->id() : 0;
    }
    if ($info['current_round'] && $info['round_nid'] == $info['current_round']->id()) {
      $info['is_current'] = TRUE;
      if ($info['period'] = muser_project_get_current_period($info['round_nid'])) {
        $date_fields = [
          'posting' => 'field_post_projects',
          'application' => 'field_apply',
          'acceptance' => 'field_accept_applications',
          'contract' => 'field_sign_contracts',
        ];
        if (!empty($date_fields[$info['period']]) && $date_field = $info['current_round']->get($date_fields[$info['period']])) {
          $date_field = $date_field->get(0);
          if ($date_field && !$date_field->isEmpty()) {
            $values = $date_field->getValue();
            $date = new DrupalDateTime($values['end_value'], DateTimeItemInterface::STORAGE_TIMEZONE);
            $date->setTimezone($this->timeZone);
            $info['period_end'] = $date->format('Y-m-d');
          }
        }
      }
    }
    return $info;
  }

  public function getProjectTermQuery($vocabulary, $round_nid) {
    $select = $this->connection->select('muser_round_projects', 'mrp');
    $select->join('taxonomy_index', 'ti', 'ti.nid = mrp.project_nid');
    $select->join('taxonomy_term_field_data', 'tfd', 'tfd.tid = ti.tid');
    $select->join('node__field_num_students', 'fns', 'fns.entity_id = mrp.project_nid');
    $select->condition('mrp.status', 1);
    $select->condition('mrp.round_nid', $round_nid);
    $select->condition('tfd.vid', $vocabulary);
    $select->groupBy('ti.tid');
    $select->groupBy('tfd.name');
    $select->groupBy('tfd.weight');
    $select->addField('tfd', 'tid');
    $select->addField('tfd', 'name');
    $select->addField('tfd', 'weight');
    $select->addExpression('COUNT(mrp.project_nid)', 'num_projects');
    $select->addExpression('SUM(fns.field_num_students_value)', 'num_openings');
    return $select;
  }

  /**
   * Report function to show projects, openings for terms in a vocabulary.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function projectsByTermBarChart(string $chart_id, array $args = [], array $query = []): array {

    if (!$num = intval($query['num'] ?? 0)) {
      $num = 15;
    }
    $sort = $query['sort'] ?? '';

    $round_info = $this->getRoundInfo($query);

    $select = $this->getProjectTermQuery($args['vocabulary'], $round_info['round_nid']);

    if ($sort == 'name') {
      $select->orderBy('tfd.name', 'asc');
    }
    if (!empty($args['openings'])) {
      $title = $this->t('Number of openings');
      $select->orderBy('num_openings', 'desc');
      $select->orderBy('num_projects', 'desc');
    }
    else {
      $title = $this->t('Number of projects');
      $select->orderBy('num_projects', 'desc');
      $select->orderBy('num_openings', 'desc');
    }
    $select->range(0, $num);

    $chart_data = [];
    $data_key = (!empty($args['openings'])) ? 'num_openings' : 'num_projects';
    foreach ($select->execute() as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Category'),
          $title,
          ['role' => 'style'],
          ['role' => 'annotation'],
        ];
      }
      $chart_data[] = [
        $row->name,
        intval($row->{$data_key}),
        ($n % 2) ? $this->colorset['PRIMARY_COLOR'] : $this->colorset['SECONDARY_COLOR'],
        intval($row->{$data_key}),
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'posting') ? 60 : 0;
    $definition['height_override'] = max(100, count($chart_data) * 65);

    $definition['chart'] = [
      'chart_type' => 'bar',
      'packages' => ['corechart', 'bar'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '50%',
          'height' => '98%',
        ],
        'legend' => 'none',
        'hAxis' => [
          'minValue' => 0,
          'textPosition' => 'none',
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show applicants for terms in a vocabulary.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function applicantsByTermBarChart(string $chart_id, array $args = [], array $query = []): array {

    if (!$num = intval($query['num'] ?? 0)) {
      $num = 15;
    }
    $sort = $query['sort'] ?? ($args['sort'] ?? '');

    $round_info = $this->getRoundInfo($query);

    $select = $this->getProjectTermQuery($args['vocabulary'], $round_info['round_nid']);

    $select->join('muser_applications_counts', 'mac', 'mac.project_nid = mrp.project_nid AND mac.round_nid = mrp.round_nid');
    $select->addExpression('SUM(mac.submitted)', 'num_submitted');
    $select->addExpression('SUM(mac.accepted)', 'num_accepted');
    $select->addExpression('SUM(mac.declined)', 'num_declined');
    switch ($sort) {
      case 'name':
        $select->orderBy('tfd.name', 'asc');
        break;
      case 'weight':
        $select->orderBy('tfd.weight', 'asc');
        break;
    }
    $select->orderBy('num_submitted', 'desc');
    $select->orderBy('num_projects', 'desc');

    $select->range(0, $num);

    $chart_data = [];
    foreach ($select->execute() as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          $this->t('Label'),
          $this->t('Openings per project'),
          ['role' => 'annotation'],
          $this->t('Applications received / project'),
          ['role' => 'annotation'],
          $this->t('Applications received / opening'),
          ['role' => 'annotation'],
          $this->t('Accepted / project'),
          ['role' => 'annotation'],
          $this->t('Accepted / opening'),
          ['role' => 'annotation'],
          $this->t('Declined / project'),
          ['role' => 'annotation'],
          $this->t('Declined / opening'),
          ['role' => 'annotation'],
        ];
      }
      if (!$row->num_projects || !$row->num_openings) {
        // Make sure we don't divide by zero.
        continue;
      }
      $chart_data[] = [
        $this->formatPlural($row->num_projects,
          '@name (1 project)',
          "@name\n(@count projects)",
          ['@name' => $row->name]),
        doubleval(number_format($row->num_openings/$row->num_projects, 2)),
        doubleval(number_format($row->num_openings/$row->num_projects, 2)),
        doubleval(number_format($row->num_submitted/$row->num_projects, 2)),
        doubleval(number_format($row->num_submitted/$row->num_projects, 2)),
        doubleval(number_format($row->num_submitted/$row->num_openings, 2)),
        doubleval(number_format($row->num_submitted/$row->num_openings, 2)),
        doubleval(number_format($row->num_accepted/$row->num_projects, 2)),
        doubleval(number_format($row->num_accepted/$row->num_projects, 2)),
        doubleval(number_format($row->num_accepted/$row->num_openings, 2)),
        doubleval(number_format($row->num_accepted/$row->num_openings, 2)),
        doubleval(number_format($row->num_declined/$row->num_projects, 2)),
        doubleval(number_format($row->num_declined/$row->num_projects, 2)),
        doubleval(number_format($row->num_declined/$row->num_openings, 2)),
        doubleval(number_format($row->num_declined/$row->num_openings, 2)),
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'posting') ? 60 : 0;
    $definition['height_override'] = max(500, count($chart_data) * 125);

    $definition['chart'] = [
      'chart_type' => 'bar',
      'packages' => ['corechart', 'bar'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '55%',
          'height' => '98%',
        ],
        'colors' => [
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
          $this->alterColor($this->colorset['SECONDARY_COLOR'], .5),
          $this->colorset['MESSAGE_STATUS_BG'],
          $this->alterColor($this->colorset['MESSAGE_STATUS_BG'], .5),
          $this->colorset['MESSAGE_ERROR_BG'],
          $this->alterColor($this->colorset['MESSAGE_ERROR_BG'], .5),
        ],
        'hAxis' => [
          'minValue' => 0,
          'textPosition' => 'none',
          'format' => '##.##',
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show student interests.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function studentPrimaryInterests(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    if (!$num = intval($query['num'] ?? 0)) {
      $num = 15;
    }
    $sort = $query['sort'] ?? '';

    $round_info = $this->getRoundInfo($query);
    if (!$dates = $this->getRoundDates($round_info['round_nid'])) {
      return [];
    }

    $users_subquery = $this->connection->select('muser_login_history', 'mlh');
    $users_subquery->condition('is_student', 1);
    $users_subquery->addField('mlh', 'uid');
    $users_subquery->distinct(TRUE);
    $users_subquery->where("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') >= :start", [
      ':start' => $dates['start'],
      ':offset' => $tz_offset,
    ]);
    $users_subquery->where("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') <= :end", [
      ':end' => $dates['end'],
      ':offset' => $tz_offset,
    ]);

    $projects_query = $this->getProjectTermQuery('categories', $round_info['round_nid']);
    $projects_count = $projects_query->execute()->fetchAllAssoc('tid');

    $select = $this->connection->select('user__field_primary_category_interest', 'pci');
    $select->join('taxonomy_term_field_data', 'tfd', 'tfd.tid = pci.field_primary_category_interest_target_id');
    $select->condition('pci.entity_id', $users_subquery, 'IN');
    $select->groupBy('tfd.tid');
    $select->groupBy('tfd.name');
    $select->addField('tfd', 'tid');
    $select->addField('tfd', 'name');
    $select->addExpression('COUNT(tfd.tid)', 'num_times');
    if ($sort == 'name') {
      $select->orderBy('tfd.name', 'asc');
    }
    else {
      $select->orderBy('num_times', 'desc');
    }
    $select->range(0, $num);

    if (!empty($args['openings'])) {
      $projects_title = $this->t('Number of openings');
    }
    else {
      $projects_title = $this->t('Number of projects');
    }
    $project_data_key = (!empty($args['openings'])) ? 'num_openings' : 'num_projects';

    $chart_data = [];
    foreach ($select->execute() as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Category'),
          $this->t('Interested Students'),
          ['role' => 'annotation'],
          $projects_title,
          ['role' => 'annotation'],
        ];
      }
      $projects_num = $projects_count[$row->tid]->{$project_data_key} ?? 0;
      $chart_data[] = [
        $row->name,
        intval($row->num_times),
        intval($row->num_times),
        intval($projects_num),
        intval($projects_num),
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'posting') ? 60 : 0;
    $definition['height_override'] = max(100, count($chart_data) * 75);

    $definition['chart'] = [
      'chart_type' => 'bar',
      'packages' => ['corechart', 'bar'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '50%',
          'height' => '98%',
        ],
        'colors' => [
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
        ],
        'hAxis' => [
          'minValue' => 0,
          'textPosition' => 'none',
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Add missing days to by-day data.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $select
   *   Select query with data.
   * @param string $date_key
   *   Row key with date value.
   * @param string $formatted_date_key
   *   Row key with formatted date value.
   * @param string $last_day
   *   Last day of period.
   *
   * @return array
   *   Data with any missing days filled in.
   */
  protected function getByDayData(SelectInterface $select, string $date_key, string $formatted_date_key, string $last_day = ''): array {

    $all_rows = [];
    $rows = [];
    $first = [];

    // First, get the data we have by-day.
    foreach ($select->execute() as $row) {
      if (!$first) {
        $keys = array_keys((array) $row);
        $first = $row;
      }
      $rows[$row->{$date_key}] = $row;
    } // Loop thru rows.

    if (!$rows) {
      return [];
    }

    $last = $row;
    $this_date = $first->{$date_key};
    $one_day = \DateInterval::createFromDateString('1 day');
    $datetime = new DrupalDateTime($this_date);
    $end_date = max($last_day, $last->{$date_key});
    $last_data_day = NULL;

    while ($this_date <= $end_date) {
      if (!empty($rows[$this_date])) {
        // Row exists.
        $all_rows[$this_date] = $rows[$this_date];
        $last_data_day = $this_date;
      }
      else {
        // Row does not exist-- create it.
        $all_rows[$this_date] = (object) array_fill_keys($keys, '');
        $all_rows[$this_date]->{$date_key} = $this_date;
        $all_rows[$this_date]->{$formatted_date_key} = $datetime->format('j M Y');
        $all_rows[$this_date]->date_filler_added = TRUE;
      }
      $datetime->add($one_day);
      $this_date = $datetime->format('Y-m-d');
    }

    foreach ($all_rows as $this_date => &$values) {
      $values->show_row_total = ($this_date <= $last_data_day);
    }

    return $all_rows;

  }

  /**
   * Report function to show student actions by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function studentActionsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    $select = $this->connection->select('muser_application_action_dates_summary', 'mad');
    $select->condition('mad.round_nid', $round_info['round_nid']);
    $or_condition = $select->orConditionGroup()
      ->condition('mad.favorited', 1)
      ->condition('mad.submitted', 1);
    $select->condition($or_condition);
    $select->addExpression('COUNT(mad.favorited)', 'favorited');
    $select->addExpression('COUNT(mad.submitted)', 'submitted');
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'action_day', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_action_day', [':offset' => $tz_offset]);
    $select->groupBy('action_day');
    $select->groupBy('formatted_action_day');
    $select->orderBy('action_day', 'asc');

    $totals = [
      'favorited' => 0,
      'submitted' => 0,
    ];
    $chart_data = [];
    $last_day = ($round_info['period'] == 'application') ? $round_info['period_end'] : '';
    $rows = $this->getByDayData($select, 'action_day', 'formatted_action_day', $last_day);
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Date'),
          t('Projects favorited'),
          t('Total projects favorited'),
          t('Applications submitted'),
          t('Total applications submitted'),
        ];
      }
      $num_favorited = intval($row->favorited);
      $num_submitted = intval($row->submitted);
      $totals['favorited'] += $num_favorited;
      $totals['submitted'] += $num_submitted;
      $chart_data[] = [
        $row->formatted_action_day,
        $num_favorited,
        ($row->show_row_total) ? $totals['favorited'] : NULL,
        $num_submitted,
        ($row->show_row_total) ? $totals['submitted'] : NULL,
      ];

    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'application') ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'left' => '10%',
          'width' => '65%',
          'height' => '70%',
        ],
        'colors' => [
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
        ],
        'vAxis' => [
          'format' => '0',
        ],
        'seriesType' => 'bars',
        'series' => [
          1 => ['type' => 'line'],
          3 => ['type' => 'line'],
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show mentor actions by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function mentorActionsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    $select = $this->connection->select('muser_applications', 'ma')
      ->condition('ma.round_nid', $round_info['round_nid'])
      ->condition('ma.is_submitted', 1);
    $select->addExpression('COUNT(ma.fid)', 'num_applications');
    $num_applications_total = intval($select->execute()->fetchField());

    $select = $this->connection->select('muser_application_action_dates_summary', 'mad');
    $select->condition('mad.round_nid', $round_info['round_nid']);
    $or_condition = $select->orConditionGroup()
      ->condition('mad.reviewed', 1)
      ->condition('mad.decided', 1)
      ->condition('mad.declined', 1)
      ->condition('mad.accepted', 1);
    $select->condition($or_condition);
    $select->addExpression('COUNT(mad.reviewed)', 'reviewed');
    $select->addExpression('COUNT(mad.decided)', 'decided');
    $select->addExpression('COUNT(mad.declined)', 'declined');
    $select->addExpression('COUNT(mad.accepted)', 'accepted');
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'action_day', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_action_day', [':offset' => $tz_offset]);
    $select->groupBy('action_day');
    $select->groupBy('formatted_action_day');
    $select->orderBy('action_day', 'asc');

    $totals = [
      'reviewed' => 0,
      'decided' => 0,
      'declined' => 0,
      'accepted' => 0,
    ];
    $chart_data = [];
    $last_day = ($round_info['period'] == 'acceptance') ? $round_info['period_end'] : '';
    $rows = $this->getByDayData($select, 'action_day', 'formatted_action_day', $last_day);
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Date'),
          t('Total applications submitted'),
          t('Applications reviewed'),
          t('Total applications reviewed'),
          t('Applications decided on'),
          t('Total applications decided on'),
          t('Applications declined'),
          t('Total applications declined'),
          t('Applications accepted'),
          t('Total applications accepted'),
        ];
      }
      $num_reviewed = intval($row->reviewed);
      $num_decided = intval($row->decided);
      $num_declined = intval($row->declined);
      $num_accepted = intval($row->accepted);
      $totals['reviewed'] += $num_reviewed;
      $totals['decided'] += $num_decided;
      $totals['declined'] += $num_declined;
      $totals['accepted'] += $num_accepted;
      $chart_data[] = [
        $row->formatted_action_day,
        $num_applications_total,
        $num_reviewed,
        ($row->show_row_total) ? $totals['reviewed'] : NULL,
        $num_decided,
        ($row->show_row_total) ? $totals['decided'] : NULL,
        $num_declined,
        ($row->show_row_total) ? $totals['declined'] : NULL,
        $num_accepted,
        ($row->show_row_total) ? $totals['accepted'] : NULL,
      ];

    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'acceptance') ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '60%',
          'height' => '60%',
        ],
        'colors' => [
          $this->colorset['MESSAGE_STATUS_BG'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['MESSAGE_WARNING_BG'],
          $this->colorset['MESSAGE_WARNING_BG'],
          $this->colorset['MESSAGE_ERROR_BG'],
          $this->colorset['MESSAGE_ERROR_BG'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
        ],
        'vAxis' => [
          'format' => '0',
        ],
        'seriesType' => 'bars',
        'series' => [
          0 => ['type' => 'line'],
          2 => ['type' => 'line'],
          4 => ['type' => 'line'],
          6 => ['type' => 'line'],
          8 => ['type' => 'line'],
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show a particular mentor's actions by round.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function mentorActionsByRound(string $chart_id, array $args = [], array $query = []): array {

    if (!empty($args['required_parameters']['uuid']) && empty($query['uuid'])) {
      return [];
    }

    $select = $this->connection->select('muser_applications_counts', 'mac');
    $select->join('users', 'u', 'u.uid = mac.project_uid');
    $select->join('node__field_start_date', 'nfsd', 'nfsd.entity_id = mac.round_nid');
    $select->join('node_field_data', 'nfd', 'nfd.nid = mac.round_nid');
    if (!empty($query['uuid'])) {
      $select->condition('u.uuid', $query['uuid']);
    }

    $select->addExpression('SUM(mac.submitted)', 'submitted');
    $select->addExpression('SUM(mac.pending)', 'pending');
    $select->addExpression('SUM(mac.in_review)', 'in_review');
    $select->addExpression('SUM(mac.accepted)', 'accepted');
    $select->addExpression('SUM(mac.declined)', 'declined');
    $select->addExpression('sum(mac.declined) + sum(mac.accepted)', 'decided');
    $select->addExpression('SUM(mac.in_review) + sum(mac.declined) + sum(mac.accepted)', 'reviewed');
    $select->addField('nfsd', 'field_start_date_value');
    $select->addField('mac', 'round_nid');
    $select->addField('nfd', 'title');
    $select->groupBy('round_nid');
    $select->groupBy('field_start_date_value');
    $select->groupBy('title');
    $select->orderBy('field_start_date_value', 'DESC');

    $chart_data = [];
    foreach ($select->execute() as $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Label'),
          t('Applications received'),
          t('Did not review'),
          t('Reviewed'),
          t('Left in review'),
          t('Decision made'),
          t('Accepted'),
          t('Declined'),
        ];
      }

      $title = $row->title;
      if (intval($row->decided) == intval($row->submitted)) {
        $title = "★ " . $title;
      }
      $chart_data[] = [
        $title,
        intval($row->submitted),
        intval($row->pending),
        intval($row->reviewed),
        intval($row->in_review),
        intval($row->decided),
        intval($row->accepted),
        intval($row->declined),
      ];
    }

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = 60;
    $definition['height_override'] = max(100, count($chart_data) * 75);

    $definition['chart'] = [
      'chart_type' => 'bar',
      'packages' => ['corechart', 'bar'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '50%',
          'height' => '98%',
        ],
        'colors' => [
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['MESSAGE_ERROR_BG'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['MESSAGE_WARNING_BG'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['MESSAGE_STATUS_BG'],
          $this->colorset['MESSAGE_ERROR_BG'],
        ],
        'hAxis' => [
          'minValue' => 0,
          'textPosition' => 'none',
        ],
      ],
    ];

    return $definition;
  }

  /**
   * Report function to show contract actions by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function contractActionsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    $select = $this->connection->select('muser_applications', 'ma')
      ->condition('ma.round_nid', $round_info['round_nid'])
      ->condition('ma.is_contracted', 1)
      ->condition('ma.status', 'accepted');
    $select->addExpression('COUNT(ma.fid)', 'num_contracts');
    $num_contracts_total = intval($select->execute()->fetchField());

    $select = $this->connection->select('muser_application_action_dates_summary', 'mad');
    $select->condition('mad.round_nid', $round_info['round_nid']);
    $or_condition = $select->orConditionGroup()
      ->condition('mad.contract_signed_student', 1)
      ->condition('mad.contract_signed_mentor', 1);
    $select->condition($or_condition);
    $select->addExpression('COUNT(mad.contract_signed_student)', 'contract_signed_student');
    $select->addExpression('COUNT(mad.contract_signed_mentor)', 'contract_signed_mentor');
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'action_day', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((mad.action_date + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_action_day', [':offset' => $tz_offset]);
    $select->groupBy('action_day');
    $select->groupBy('formatted_action_day');
    $select->orderBy('action_day', 'asc');

    $totals = [
      'contract_signed_student' => 0,
      'contract_signed_mentor' => 0,
    ];
    $chart_data = [];
    $last_day = ($round_info['period'] == 'contract') ? $round_info['period_end'] : '';
    $rows = $this->getByDayData($select, 'action_day', 'formatted_action_day', $last_day);
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Date'),
          t('Total contracts'),
          t('Agreed by students'),
          t('Total agreed by students'),
          t('Agreed by mentors'),
          t('Total agreed by mentors'),
        ];
      }
      $contract_signed_student = intval($row->contract_signed_student);
      $contract_signed_mentor = intval($row->contract_signed_mentor);
      $totals['contract_signed_student'] += $contract_signed_student;
      $totals['contract_signed_mentor'] += $contract_signed_mentor;
      $chart_data[] = [
        $row->formatted_action_day,
        $num_contracts_total,
        $contract_signed_student,
        ($row->show_row_total) ? $totals['contract_signed_student'] : NULL,
        $contract_signed_mentor,
        ($row->show_row_total) ? $totals['contract_signed_mentor'] : NULL,
      ];

    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'contract') ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '60%',
          'height' => '60%',
        ],
        'colors' => [
          $this->colorset['MESSAGE_STATUS_BG'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
        ],
        'vAxis' => [
          'format' => '0',
        ],
        'seriesType' => 'bars',
        'series' => [
          0 => ['type' => 'line'],
          2 => ['type' => 'line'],
          4 => ['type' => 'line'],
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show projects activated by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function projectsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    $select = $this->connection->select('muser_round_projects', 'mrp');
    $select->join('node_field_data', 'nfr', 'nfr.nid = mrp.project_round_nid');
    if (!empty($args['openings'])) {
      // Chart ID: project-openings-posted-by-day.
      $select->join('node__field_num_students', 'nfns', 'mrp.project_nid = nfns.entity_id');
      $select->addExpression('SUM(nfns.field_num_students_value)', 'num_projects');
    }
    else {
      // Chart ID: projects-posted-by-day.
      $select->addExpression('COUNT(mrp.project_nid)', 'num_projects');
    }
    $select->condition('mrp.round_nid', $round_info['round_nid']);
    $select->addExpression("FROM_UNIXTIME((nfr.created + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'date_added', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((nfr.created + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_date_added', [':offset' => $tz_offset]);
    $select->groupBy('date_added');
    $select->groupBy('formatted_date_added');
    $select->orderBy('date_added', 'asc');

    $total = 0;
    $chart_data = [];
    $last_day = ($round_info['period'] == 'posting') ? $round_info['period_end'] : '';
    $rows = $this->getByDayData($select, 'date_added', 'formatted_date_added', $last_day);
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        if (!empty($args['openings'])) {
          $chart_data[] = [
            t('Date'),
            t('Openings posted'),
            t('Total openings'),
          ];
        }
        else {
          $chart_data[] = [
            t('Date'),
            t('Projects posted'),
            t('Total projects'),
          ];
        }
      }
      $num_projects = intval($row->num_projects);
      $total += $num_projects;
      $chart_data[] = [
        $row->formatted_date_added,
        $num_projects,
        ($row->show_row_total) ? $total : NULL,
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current'] && $round_info['period'] == 'posting') ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '80%',
          'height' => '60%',
        ],
        'legend' => 'none',
        'colors' => [
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
        ],
        'vAxis' => [
          'title' => (empty($args['openings'])) ? $this->t('Projects') : $this->t('Openings'),
          'format' => '0',
        ],
        'seriesType' => 'bars',
        'series' => [1 => ['type' => 'line']],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show logins by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function loginsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    if (!$dates = $this->getRoundDates($round_info['round_nid'])) {
      return [];
    }

    $select = $this->connection->select('muser_login_history_aggregated', 'mlh');
    $select->where("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') >= :start", [
      ':start' => $dates['start'],
      ':offset' => $tz_offset,
    ]);
    $select->where("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') <= :end", [
      ':end' => $dates['end'],
      ':offset' => $tz_offset,
    ]);
    $select->addExpression('COUNT(mlh.is_student)', 'num_students');
    $select->addExpression('COUNT(mlh.is_mentor)', 'num_mentors');
    $select->addExpression("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'login_date', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((mlh.login + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_login_date', [':offset' => $tz_offset]);
    $select->groupBy('login_date');
    $select->groupBy('formatted_login_date');
    $select->orderBy('login_date', 'asc');

    $totals = [
      'mentors' => 0,
      'students' => 0,
    ];
    $chart_data = [];
    $rows = $this->getByDayData($select, 'login_date', 'formatted_login_date');
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Date'),
          t('Student logins'),
          t('Total student logins'),
          t('Mentor logins'),
          t('Total mentor logins'),
        ];
      }
      $num_students = intval($row->num_students);
      $totals['students'] += $num_students;
      $num_mentors = intval($row->num_mentors);
      $totals['mentors'] += $num_mentors;
      $chart_data[] = [
        $row->formatted_login_date,
        $num_students,
        ($row->show_row_total) ? $totals['students'] : NULL,
        $num_mentors,
        ($row->show_row_total) ? $totals['mentors'] : NULL,
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current']) ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '70%',
          'height' => '60%',
        ],
        'vAxis' => [
          'title' => t('Logins'),
          'format' => '0',
        ],
        'colors' => [
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
        ],
        'seriesType' => 'bars',
        'series' => [
          1 => ['type' => 'line'],
          3 => ['type' => 'line'],
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Report function to show actions by day.
   *
   * @param string $chart_id
   *   Chart ID to show.
   * @param array $args
   *   Args from chart info.
   * @param array $query
   *   Query variables from URL.
   *
   * @return array
   *   Array of data to return via JSON.
   */
  public function actionsByDay(string $chart_id, array $args = [], array $query = []): array {

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $round_info = $this->getRoundInfo($query);

    if (!$dates = $this->getRoundDates($round_info['round_nid'])) {
      return [];
    }

    $select = $this->connection->select('muser_user_actions_aggregated', 'mua');
    $select->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') >= :start", [
      ':start' => $dates['start'],
      ':offset' => $tz_offset,
    ]);
    $select->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') <= :end", [
      ':end' => $dates['end'],
      ':offset' => $tz_offset,
    ]);
    $select->addExpression('COUNT(mua.is_student)', 'num_students');
    $select->addExpression('COUNT(mua.is_mentor)', 'num_mentors');
    $select->addExpression("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')", 'action_date', [':offset' => $tz_offset]);
    $select->addExpression("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%e %b %Y')", 'formatted_action_date', [':offset' => $tz_offset]);
    $select->groupBy('action_date');
    $select->groupBy('formatted_action_date');
    $select->orderBy('action_date', 'asc');

    $totals = [
      'mentors' => 0,
      'students' => 0,
    ];
    $chart_data = [];
    $rows = $this->getByDayData($select, 'action_date', 'formatted_action_date');
    foreach ($rows as $n => $row) {
      if (!$chart_data) {
        $chart_data[] = [
          t('Date'),
          t('Active Students'),
          t('Total active students'),
          t('Active Mentors'),
          t('Total active mentors'),
        ];
      }
      $num_students = intval($row->num_students);
      $totals['students'] += $num_students;
      $num_mentors = intval($row->num_mentors);
      $totals['mentors'] += $num_mentors;
      $chart_data[] = [
        $row->formatted_action_date,
        $num_students,
        ($row->show_row_total) ? $totals['students'] : NULL,
        $num_mentors,
        ($row->show_row_total) ? $totals['mentors'] : NULL,
      ];
    } // Loop thru rows.

    $definition['chart_id'] = $chart_id;
    $definition['timeout'] = ($round_info['is_current']) ? 60 : 0;

    $definition['chart'] = [
      'chart_type' => 'combo',
      'packages' => ['corechart'],
      'data' => $chart_data,
      'options' => [
        'chartArea' => [
          'width' => '70%',
          'height' => '60%',
        ],
        'vAxis' => [
          'title' => t('Active users'),
          'format' => '0',
        ],
        'colors' => [
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['PRIMARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
          $this->colorset['SECONDARY_COLOR'],
        ],
        'seriesType' => 'bars',
        'series' => [
          1 => ['type' => 'line'],
          3 => ['type' => 'line'],
        ],
      ],
    ];

    return $definition;

  }

  /**
   * Return start/end dates for a round.
   *
   * @param int $round_nid
   *   The NID of the Round.
   *
   * @return array
   *   Array with start, end date.
   * @throws \Exception
   */
  public function getRoundDates(int $round_nid): array {

    if (!$round = Node::load($round_nid)) {
      return [];
    }

    $dates = [];

    $date = new DrupalDateTime($round->field_start_date->value, DateTimeItemInterface::STORAGE_TIMEZONE);
    $date->setTimezone($this->timeZone);
    $dates['start'] = $date->format('Y-m-d');

    if (!empty($round->field_sign_contracts->end_value)) {
      $date = new DrupalDateTime($round->field_sign_contracts->end_value, DateTimeItemInterface::STORAGE_TIMEZONE);
      $date->setTimezone($this->timeZone);
      $contracts = $date->format('Y-m-d');
    }
    else {
      $contracts = '';
    }

    $date = new DrupalDateTime($round->field_accept_applications->end_value, DateTimeItemInterface::STORAGE_TIMEZONE);
    $date->setTimezone($this->timeZone);
    $review = $date->format('Y-m-d');

    $dates['end'] = max($contracts, $review);
    return $dates;

  }

  /**
   * Returns build array for given chart.
   *
   * @param string $chart_id
   *   ID of chart to be $query_string.
   * @param string $query_string
   *   URL query string to be used.
   *
   * @return array|string[]
   *   Build array for chart.
   */
  public function getChartMarkup(string $chart_id, string $query_string = ''): array {
    $chart = $this->chartsInfo[$chart_id] ?? [];
    if (!$chart) {
      return [
        '#markup' => '<div class="muser-chart-error">'
        . $this->t('Chart not found.')
        . '</div>',
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    $wrapper_attributes = new Attribute();
    $wrapper_attributes->addClass('muser-chart__' . $chart_id);
    if (!empty($chart['classes'])) {
      foreach ($chart['classes'] as $class) {
        $wrapper_attributes->addClass($class);
      }
    }
    $id = Html::getUniqueId($chart_id);
    $chart_attributes = new Attribute();
    $chart_attributes->setAttribute('id', $id);
    $chart_attributes->setAttribute('data-chart-id', $chart_id);
    $chart_attributes->setAttribute('data-chart-pass-query-parameters', !empty($chart['pass_query_parameters']) ? 1 : 0);
    $chart_attributes->setAttribute('data-chart-query-string', $query_string);
    $build = [
      '#theme' => 'muser_chart',
      '#chart_id' => $chart_id,
      '#chart' => $chart,
      '#label' => $chart['label'] ?? '',
      '#description' => $chart['description'] ?? '',
      '#wrapper_attributes' => $wrapper_attributes,
      '#chart_attributes' => $chart_attributes,
      '#messages_id' => 'messages--' . $id,
      '#loading_message' => $chart['loading_message'] ?? $this->t('Loading data...'),
      '#no_data_message' => $chart['no_data_message'] ?? $this->t('No data found for this round yet.'),
      '#error_message' => $chart['error_message'] ?? $this->t('Error loading data.'),
      '#cache' => [
        'contexts' => [
          'url.query_args',
        ],
        'tags' => [
          'node_list',
          'current_round',
        ],
      ],
    ];
    $build['#attached']['library'][] = 'muser_system/charts';
    return $build;
  }

  /**
   * Calls specified function to return definition of given chart.
   *
   * @param string $chart_id
   *   ID of chart to be rendered.
   * @param array $query
   *   Array of URL query parameters.
   *
   * @return array
   *   Chart definition array to be passed to Google charts.
   */
  public function getChartDefinition(string $chart_id, array $query = []): array {
    $chart = $this->chartsInfo[$chart_id] ?? [];
    $args = $chart['args'] ?? [];
    if (empty($chart['pass_query_parameters'])) {
      $query = [];
    }
    if (!empty($chart['function'])) {
      $context = [
        'args' => $args,
        'query' => $query,
      ];
      if (method_exists($this, $chart['function'])) {
        $definition = $this->{$chart['function']}($chart_id, $args, $query);
        $this->moduleHandler->alter('muser_charts_data', $definition, $chart_id, $context);
        return $definition;
      }
      elseif (function_exists($chart['function'])) {
        $definition = $chart['function']($chart_id, $args, $query);
        $this->moduleHandler->alter('muser_charts_data', $definition, $chart_id, $context);
        return $definition;
      }
    }
    // @todo Throw exception.
    return [];
  }

  /**
   * Apply opacity to color.
   *
   * @param $color
   *   Hex color to alter.
   * @param $opacity
   *   Opacity to apply (0-1).
   *
   * @return string
   *   Resulting hex color.
   */
  function alterColor($color, $opacity) {
    $use_pound = FALSE;
    $color_array = str_split($color);
    if ($color_array[0] === '#') {
      array_shift($color_array);
      $use_pound = TRUE;
    }
    list($red, $green, $blue) = sscanf(implode('', $color_array), '%02x%02x%02x');
    foreach ([&$red, &$green, &$blue] as &$part) {
      $part = intval((255 - $opacity * (255 - $part)) + .5);
      if ($part > 255) {
        $part = 255;
      }
      elseif ($part < 0) {
        $part = 0;
      }
    }
    return ($use_pound ? '#' : '') . base_convert(($blue | ($green << 8) | ($red << 16)), 10, 16);
  }

}
