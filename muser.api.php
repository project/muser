<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

use Drupal\node\Entity\Node;

/**
 * Allow modules to alter/add colorsets for the theme.
 *
 * See muser_base_get_colorsets() in muser/themes/muser_base/includes/theme_settings.inc
 *
 * Alternatively, you may specify colorsets using two other methods:
 *
 * 1. Add custom colorsets in settings.local.php:
 *
 * $settings['custom_colorsets'] = [
 *   'colorset_ocean' => [
 *     'name' => 'Ocean (Blue)',
  *    'PRIMARY_COLOR' => '#001A57',
  *    'SECONDARY_COLOR' => '#339898',
  *    'BACKGROUND_COLOR' => '#F3F2F1',
  *    'TITLE_COLOR' => '#001A57',
  *    'TEXT_COLOR' => '#262626',
  *    'MESSAGE_ERROR_BG' => '#C84E00',
  *    'MESSAGE_WARNING_BG' => '#E89923',
  *    'MESSAGE_STATUS_BG' => '#A1B70D',
  *    'MESSAGE_ERROR_TEXT' => '#FFFFFF',
  *    'MESSAGE_WARNING_TEXT' => '#FFFFFF',
  *    'MESSAGE_STATUS_TEXT' => '#FFFFFF',
  *    'TEXT_OVER_PRIMARY' => '#FFFFFF',
  *    'TEXT_OVER_SECONDARY' => '#FFFFFF',
  *    'PRIMARY_TEXT_OVER_WHITE' => 'var(--primary)',
  *    'SECONDARY_TEXT_OVER_WHITE' => 'var(--secondary)',
 *   ],
 * ];
 *
 * 2. Add colorsets in a YAML file located in public://colorsets.yml. See the
 * colorsets.yml file in the profiles/muser/defaults/ directory for an example.
 *
 * @param array $colorsets
 *   Array of existing colorsets.
 *
 * @return array
 *   Updated array of colorsets.
 */
function hook_muser_colorsets_alter(&$colorsets) {
  // Add a new colorset.
  $colorsets['colorset_dusk'] = [
    'name' => t('Dusk (Gray/Yellow)'),
    'PRIMARY_COLOR' => '#5B6780',
    'SECONDARY_COLOR' => '#EAAA10',
    'BACKGROUND_COLOR' => '#F3F2E1',
    'TITLE_COLOR' => '#603D30',
    'TEXT_COLOR' => '#262626',
    'MESSAGE_ERROR_BG' => '#A6193E',
    'MESSAGE_WARNING_BG' => '#FFD110',
    'MESSAGE_STATUS_BG' => '#B7BF20',
    'MESSAGE_ERROR_TEXT' => '#FFFFFF',
    'MESSAGE_WARNING_TEXT' => '#262626',
    'MESSAGE_STATUS_TEXT' => '#262626',
    'TEXT_OVER_PRIMARY' => '#FFFFFF',
    'TEXT_OVER_SECONDARY' => '#262626',
  ];
  return $colorsets;
}

/**
 * Sets chart types available to Charts service.
 *
 * @return array[]
 *   Array of charts.
 */
function hook_muser_charts_info(): array {
  return [
    'projects-by-category' => [
      'label' => t('Number of Projects by Category'),
      'description' => t('Ordered by count'),
      // By default, the function is a method of the Charts service.
      'function' => 'projectsByCategory',
      'args' => [],
      'pass_query_parameters' => TRUE,
      'no_data_message' => t('No projects found.'),
    ],
    'projects-by-category-openings' => [
      'label' => t('Number of Openings by Category'),
      'function' => 'projectsByCategory',
      'args' => ['openings' => TRUE],
      'pass_query_parameters' => TRUE,
    ],
    'some-other-chart' => [
      'label' => t('Our Data'),
      // The function can also be a normal function.
      'function' => 'example_get_chart_data',
      'args' => ['sort' => TRUE],
      'classes' => ['class1', 'class2'],
    ],
  ];
}

/**
 * Alters chart types available to Charts service.
 *
 * @param array $info
 *   Array of charts.
 */
function hook_muser_charts_info_alter(&$info) {
  $info['projects-by-category-openings-rev'] = [
    'function' => 'projectsByCategory',
    'args' => ['openings' => TRUE, 'rev' => TRUE],
    'pass_query_parameters' => TRUE,
  ];
}

/**
 * Alters chart definition array sent to Google Charts.
 *
 * @param array $definition
 *   Chart definition to be sent to Google Charts.
 * @param string $chart_id
 *   Chart ID to show.
 * @param array $context
 *   Array with keys of 'args' and 'query'.
 */
function hook_muser_charts_data_alter(&$definition, $chart_id, $context) {
  $definition['chart']['chart_type'] = 'somethingelse';
  if (count($definition['chart']['data']) > 10) {
    // Do something.
  };
}

/**
 * Alters admin dashboard build array.
 *
 * @param array $build
 *   Build array..
 * @param int $round_nid
 *   Node ID of round being shown.
 */
function hook_muser_admin_dashboard_alter(&$build, $round_nid) {
  $build['new_content'] = [
    '#markup' => '<div>' . t('More content for dashboard.') . '</div>',
  ];
}


/**
 * Perform actions after a new "current round" is set.
 *
 * @param \Drupal\node\Entity\Node $new
 *   New "current round" node.
 * @param \Drupal\node\Entity\Node|null $old
 *   Previous "current round" node (if set).
 */
function hook_muser_set_current_round(Node $new, Node|null $old) {
  \Drupal::logger('muser')->notice('The current round was changed!');
}

/**
 * Perform actions when the current period in a round changes.
 *
 * Possible values:
 * - before
 * - posting
 * - application
 * - acceptance
 * - contract (if enabled)
 * - after
 *
 * @param string $new_period
 *   The new current period in the round.
 * @param string|null $old_period
 *   The previous value, if set.
 */
function hook_muser_change_current_round_period(string $new_period, string|null $old_period) {
  \Drupal::logger('muser')->notice('The current period in the round was changed!');
}
