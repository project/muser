<?php

namespace Drupal\muser_system\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\muser_system\Charts;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to show if a user was active in a given round.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("user_active_in_round")
 */
class UserActiveInRoundFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  protected $valueFormType = 'select';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The charts service.
   *
   * @var \Drupal\muser_system\Charts
   */
  protected $charts;

  /**
   * Constructs a new UserActiveInRoundFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\muser_system\Charts $charts
   *   The charts service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, Charts $charts) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->charts = $charts;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('muser_system.charts')
    );
  }

  /**
   * @{inheritdoc}
   */
  public function getValueOptions() {
    if (isset($this->valueOptions)) {
      return $this->valueOptions;
    }
    $this->valueOptions = muser_system_get_rounds();
    return $this->valueOptions;
  }

  /**
   * @{inheritdoc}
   */
  public function query() {

    $this->ensureMyTable();
    $round_nid = $this->value[0] ?? 0;

    if (!$round_nid || !$dates = $this->charts->getRoundDates($round_nid)) {
      // Round wasn't valid.
      $where = "1 = 0";
      $this->query->addWhereExpression($this->options['group'], $where);
      return;
    }

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();
    $subquery = $this->connection->select('muser_user_actions_aggregated', 'mua');
    $subquery->addField('mua', 'uid');
    $subquery->distinct(TRUE);
    $subquery->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') >= :start", [
      ':start' => $dates['start'],
      ':offset' => $tz_offset,
    ]);
    $subquery->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') <= :end", [
      ':end' => $dates['end'],
      ':offset' => $tz_offset,
    ]);

    $configuration = [
      'type' => 'INNER',
      'table formula' => $subquery,
      'field' => 'uid',
      'left_table' => $this->tableAlias,
      'left_field' => 'uid',
      'operator' => '=',
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('muser_user_actions_aggregated', $join, $this->tableAlias);

  }

}
