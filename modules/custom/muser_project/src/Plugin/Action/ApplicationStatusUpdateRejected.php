<?php

namespace Drupal\muser_project\Plugin\Action;

/**
 * Provides an action to update the flagging status to Declined.
 *
 * @Action(
 *   id = "muser_project_update_flagging_status__rejected",
 *   label = @Translation("Set selected to Declined"),
 *   type = "flagging",
 *   category = @Translation("Muser")
 * )
 */
class ApplicationStatusUpdateRejected extends ApplicationStatusUpdateBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToUpdate() {
    return ['field_status' => 'declined'];
  }

}
