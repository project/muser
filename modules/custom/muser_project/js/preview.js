/**
 * @file
 * Preview functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserProjectPreview = {
    attach: function (context, settings) {
      $(once('teaser-processed', 'a.node--view-mode-teaser')).off('click').on('click', function (e) {
        // In preview, don't allow clicking on card.
        e.preventDefault();
        return false;
      });

    }
  }
})(jQuery, Drupal, once);
