/**
 * @file
 * Custom script for vanilla icon picker.
 */

(function (Drupal, once) {
  Drupal.behaviors.myfeature = {
    attach(context) {
      const elements = once('fontawesomeIconPickerVanillaIconPicker', '.fontawesomeIconPickerVanillaIconPicker', context);
      // `elements` is always an Array.
      elements.forEach(processingCallback);
    }
  };
  // The parameters are reversed in the callback between jQuery `.each` method
  // and the native `.forEach` array method.
  function processingCallback(iconElementInput, index) {
    const option = JSON.parse(iconElementInput.getAttribute('data-option'));
    let iconPicker = new IconPicker(iconElementInput, option);
    let icon = iconPicker.element.value;
    let container = iconElementInput.closest('div.form-item');
    container.classList.add('iconpicker-container');
    container.insertAdjacentHTML('beforeend', '<span class="input-group-addon__wrapper"><span class="input-group-addon"><i class="' + icon + '"></i></span><i class="edit-icon-button fas fa-angle-down"></i></span>');
    let inputGroupAddon = container.getElementsByClassName('input-group-addon')[0];
    iconPicker.on('select', instance => {
      inputGroupAddon.innerHTML = '<i class="' + instance.value + '">';
    });
  }
}(Drupal, once));
