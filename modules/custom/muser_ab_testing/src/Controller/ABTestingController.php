<?php

namespace Drupal\muser_ab_testing\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\IntegrityConstraintViolationException;

/**
 * Controller for basic Muser pages.
 */
class ABTestingController extends ControllerBase {

  protected function getTestsForUser($uid) {
    $select = \Drupal::database()->select('node_field_data', 'nfd');
    $select->join('node__field_cohorts_serialized', 'nfcs', 'nfd.nid = nfcs.entity_id AND nfcs.deleted = 0');
    $select->leftJoin('muser_ab_testing_cohort_membership', 'mabcm', 'mabcm.test_nid = nfd.nid and mabcm.uid = :uid', [':uid' => $uid]);
    $select->addField('nfcs', 'field_cohorts_serialized_value', 'test_cohorts');
    $select->addField('mabcm', 'cohort', 'user_cohort_identifier');
    $select->addField('nfd', 'nid', 'test_nid');
    $select->condition('nfd.status', 1);
    $select->condition('nfd.type', 'ab_test');

    $data = [];

    foreach ($select->execute() as $n => $row) {
      $test_cohorts = unserialize($row->test_cohorts);

      $data[$row->test_nid] = [
        'user_cohort_identifier' => $row->user_cohort_identifier,
        'test_nid' => $row->test_nid,
        'available_cohorts' => array_keys($test_cohorts),
        'test_cohorts' => $test_cohorts,
      ];
    } // Loop thru rows.
    return $data;
  }

  protected function addUserToNewRandomCohort($uid, $test_nid, $available_cohorts) {
    return $this->addUserToRandomCohort($uid, $test_nid, $available_cohorts, TRUE);
  }

  protected function addUserToRandomCohort($uid, $test_nid, $available_cohorts, $override_existing = FALSE) {
    $connection = \Drupal::service('database');
    $new_cohort = $available_cohorts[rand(0, count($available_cohorts) - 1)];
    if ($uid === 0) {
      $new_cohort = $available_cohorts[0];
    }

    try {
      if ($override_existing) {
        $delete = $connection->delete('muser_ab_testing_cohort_membership');
        $delete->condition('uid', $uid);
        $delete->condition('test_nid', $test_nid);
        $delete->execute();
      }

      $connection->insert('muser_ab_testing_cohort_membership')
        ->fields([
          'uid' => $uid,
          'cohort' => $new_cohort,
          'test_nid' => $test_nid,
          'create_timestamp' => \Drupal::time()->getCurrentTime(),
        ])
        ->execute();
    }
    catch (IntegrityConstraintViolationException $e) {
      $select = $connection->select('muser_ab_testing_cohort_membership', 'mabcm');
      $select->addField('mabcm', 'cohort', 'user_cohort_identifier');
      $select->condition('mabcm.uid', $uid);
      $select->condition('mabcm.test_nid', $test_nid);
      $select->execute();

      foreach ($select->execute() as $n => $row) {
        if (in_array($row->cohort, $available_cohorts)) {
          return $row->cohort;
        }
        else {
          return $this->addUserToNewRandomCohort($uid, $test_nid, $available_cohorts);
        }
      }
    }
    catch (\Exception $e) {
      return NULL;
    }

    return $new_cohort;
  }

  /**
   * Get current.
   *
   * @param string $chart_id
   *   The ID of the chart data to return.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON of chart data for Google charts.
   */
  public function activeTests() {
    $uid = \Drupal::currentUser()->id();
    $user_tests = $this->getTestsForUser($uid);

    foreach ($user_tests as $test_nid => &$test) {
      if (!$test['user_cohort_identifier'] || !in_array($test['user_cohort_identifier'], $test['available_cohorts'])) {
        $exists_but_invalid = $test['user_cohort_identifier'] && !in_array($test['user_cohort_identifier'], $test['available_cohorts']);
        if ($exists_but_invalid) {
          $new_cohort_id = $this->addUserToNewRandomCohort($uid, $test_nid, $test['available_cohorts']);
        }
        else {
          $new_cohort_id = $this->addUserToRandomCohort($uid, $test_nid, $test['available_cohorts']);
        }
        if ($new_cohort_id) {
          $test['user_cohort_identifier'] = $new_cohort_id;
        }
      }

      if ($uid === 0) {
        $test['user_cohort'] = reset($test['test_cohorts']);
      }
      else {
        if (!$test['user_cohort_identifier']) {
          $test['user_cohort'] = [];
        }
        else {
          $test['user_cohort'] = $test['test_cohorts'][$test['user_cohort_identifier']];
        }
      }
      $test['actions'] = $test['user_cohort']['actions'] ?? [];
    }

    $cohorts_data = [
      'cohorts' => array_map(function($test) {
        return [
          'identifier' => $test['user_cohort_identifier'],
          'actions' => $test['actions'],
        ];
      }, $user_tests)
    ];

    $cache_data = [
      'contexts' => [
        'user',
      ],
      'tags' => [
        'muser_ab_testing'
      ]
    ];

    $response = new CacheableJsonResponse($cohorts_data);
    $cohorts_data['#cache'] = $cache_data;
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($cohorts_data));
    return $response;
  }
}
