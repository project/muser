/**
 * @file
 * A/B Testing functionality.
 */

(function ($, Drupal, once) {

  Drupal.applyCohortActions = function(cohort, context) {
    cohort.actions.forEach(action => {
      if (action.type === 'hide') {
        Drupal.hideAction(action, context, cohort.identifier)
      }
      else if (action.type === 'replace') {
        Drupal.replaceAction(action, context, cohort.identifier)
      }
      else {
        console.warn('Muser A/B testing: Action not recognized: ', action.type);
      }
    })
  }

  Drupal.hideAction = function(action, context, identifier) {
    $(once('hide_action_id_' + identifier, action.selector, context)).hide();
  }

  Drupal.replaceAction = function(action, context, identifier) {
    if (action.selector.startsWith('bodyMatch')) {
      let body_class_regex = /^bodyMatch([^ ]+) (.+)/gi
      let results = body_class_regex.exec(action.selector)
      if (results[1] && results[2]) {
        let matching_body = $(once('bodymatch_action_id_' + identifier, 'body' + results[1]));
        if (matching_body.length > 0) {
          $(once('replace_action_id_' + identifier, results[2], context)).text(action.replacement);
        }
      }
    }
    else {
      $(once('replace_action_id_' + identifier, action.selector, context)).text(action.replacement);
    }
  }

  Drupal.behaviors.muserABTesting = {
    attach: function (context, settings) {
      const cohorts_data_url = "/api/ab-test";
      if (typeof Drupal.ab_testing_cohort_data === 'undefined') {
        Drupal.ab_testing_cohort_data = []
        $.get(cohorts_data_url, (cohorts_data) => {
          Drupal.ab_testing_cohort_data = cohorts_data
          if (typeof Drupal.ab_testing_cohort_data.cohorts !== 'undefined' && Object.keys(Drupal.ab_testing_cohort_data.cohorts).length > 0) {
            Object.values(Drupal.ab_testing_cohort_data.cohorts).forEach(cohort => {
              Drupal.applyCohortActions(cohort, context)
            })
          }
        });
      }
      else {
        if (typeof Drupal.ab_testing_cohort_data.cohorts !== 'undefined' && Object.keys(Drupal.ab_testing_cohort_data.cohorts).length > 0) {
          Object.values(Drupal.ab_testing_cohort_data.cohorts).forEach(cohort => {
            Drupal.applyCohortActions(cohort, context)
          })
        }
      }
    }
  };
})(jQuery, Drupal, once);
