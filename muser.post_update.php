<?php

/**
 * @file
 * Post-update functions for the Muser installation profile.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Add contract star to all users.
 */
function muser_post_update_add_user_contract_stars(&$sandbox) {
  $uids = \Drupal::entityQuery('user')
    ->condition('uid', 1, '>')
    ->accessCheck(FALSE)
    ->execute();
  if ($uids) {
    $storage_handler = \Drupal::entityTypeManager()->getStorage('user');
    foreach ($uids as $uid) {
      $user = $storage_handler->load($uid);
      $user->field_has_contract_star->value = 1;
      $user->save();
    } // Loop thru users.
  } // Got users?
}

/**
 * Set initial value for profile needs_review.
 */
function muser_post_update_set_needs_review(&$sandbox) {
  $results = \Drupal::entityQuery('user')
    ->accessCheck(FALSE)
    ->execute();
  foreach ($results as $uid) {
    if ($account = User::load($uid)) {
      $account->field_needs_review->value = 1;
      $account->save();
    }
  } // Loop thru users.
}

/**
 * Set values for "Student application period start/end email sent" fields.
 */
function muser_post_update_set_student_apply_values(&$sandbox) {
  // Set the value of field_student_apply_start|end_email for existing rounds
  // where the "Days when students can apply" start/end date has passed.
  $now = new DrupalDateTime('now', DateTimeItemInterface::STORAGE_TIMEZONE);
  $updates = [
    'value' => 'field_student_apply_start_email',
    'end_value' => 'field_student_apply_end_email',
  ];
  foreach ($updates as $db_field => $field_name) {
    $results = \Drupal::entityQuery('node')
      ->condition('type', 'round')
      ->condition('field_apply.' . $db_field,  $now->format('Y-m-d\TH:i:s'), '<')
      ->accessCheck(FALSE)
      ->execute();
    foreach ($results as $nid) {
      if ($round = Node::load($nid)) {
        $round->{$field_name}->value = 1;
        $round->save();
      }
    } // Loop thru rounds.
  } // Loop thru updates.
}

/**
 * Set default values for new Muser config settings.
 */
function muser_post_update_set_new_muser_config(&$sandbox) {
  $muser_config = \Drupal::configFactory()->getEditable('muser_system.settings');
  $new_values = [
    'dashboard_charts_to_show',
    'application_process_instructions_title',
    'application_process_instructions_content',
    'student_apply_start_email_offset',
    'student_apply_start_email_subject',
    'student_apply_start_email_body',
    'student_apply_end_email_offset',
    'student_apply_end_email_subject',
    'student_apply_end_email_body',
  ];
  $config_path = \Drupal::service('extension.path.resolver')->getPath('profile', 'muser') . '/config/install';
  $source = new FileStorage($config_path);
  $data = $source->read('muser_system.settings');
  foreach ($new_values as $new_value) {
    $muser_config->set($new_value, $data[$new_value]);
  }
  // Turn these off so no emails go out for current round.
  $muser_config->set('student_apply_recipient_timeframe', '');
  $muser_config->set('student_apply_end_recipients', 'same');
  $muser_config->save();

}

/**
 * Update Block admin menu link.
 */
function muser_post_update_update_block_admin_menu_link(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('menu_link_content');
  if ($menu_link = $storage->loadByProperties(['link.uri' => 'internal:/admin/content/block-content'])) {
    /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $menu_link */
    $menu_link = current($menu_link);
    $link = [
      'uri' => 'internal:/admin/content/block',
      'title' => $menu_link->link->title,
      'options' => $menu_link->link->options,
    ];
    $menu_link->set('link', $link);
    $menu_link->save();
  }
}
