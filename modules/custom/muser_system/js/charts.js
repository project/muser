/**
 * @file
 * Charts functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserCharts = {
    attach: function (context, settings) {

      let charts = {};
      let chartSettings = {};

      let setChartHeight = function (element, chartInfo) {
        let height = chartInfo['height_override'];
        if (height) {
          $(element).attr('style', 'height: ' + height + 'px');
        }
      }

      let drawChart = function (id) {
        let data = google.visualization.arrayToDataTable(chartSettings[id]['data']);
        switch (chartSettings[id]['chart_type']) {
          case 'bar':
            charts[id] = new google.visualization.BarChart(document.getElementById(id));
            break;
          case 'combo':
            charts[id] = new google.visualization.ComboChart(document.getElementById(id));
            break;
        }
        charts[id].draw(data, chartSettings[id]['options']);
      }

      let processChart = function (chartElement, chartInfo, id, chartId, url, $loading, $noData, $error) {
        if (!chartInfo['chart_id']) {
          $('#' + id).hide();
          $noData.show();
          return false;
        }
        setChartHeight(chartElement, chartInfo);
        if (chartInfo['chart']['data'].length === 0) {
          $('#' + id).hide();
          $noData.show();
          return false;
        }
        let timeOut = chartInfo['timeout'];
        chartSettings[id] = chartInfo['chart'];
        if (timeOut) {
          setTimeout(updateChart, timeOut * 1000, chartElement, id, chartId, url, $loading, $noData, $error);
        }
        return true;
      }

      let updateChart = function (chartElement, id, chartId, url, $loading, $noData, $error) {
        let updating = $(chartElement)
          .siblings('.muser-chart-label').first()
          .find('.muser-chart-updating')
          .css('display', 'inline-block');
        $.get(url, {}, (chartInfo) => {
          updating.hide();
          if (processChart(chartElement, chartInfo, id, chartId, url, $loading, $noData, $error)) {
            drawChart(id);
          }
        }).fail(function () {
          updating.hide();
          $loading.hide();
          $noData.hide();
          $error.show();
          console.error(
            Drupal.t(
              'An error occurred loading data for chart !id', {'!id': id},
            )
          );
        });
      }

      $(once('charts--processed', '.muser-chart')).each( function () {

        let id = $(this).attr('id');
        let chartId = $(this).attr('data-chart-id');
        let queryParams = $(this).attr('data-chart-query-string');
        let passQueryParams = $(this).attr('data-chart-pass-query-parameters');
        let url = '/api/charts-data/' + chartId;
        if (queryParams) {
          url += '?' + queryParams;
        }
        else if (passQueryParams) {
          url += window.location.search;
        }

        let $loading = $('#messages--' + id + ' .muser-chart-message-loading');
        let $noData = $('#messages--' + id + ' .muser-chart-message-no-data');
        let $error = $('#messages--' + id + ' .muser-chart-message-error');

        $.get(url, {}, (chartInfo) => {
          $loading.hide();
          if (processChart(this, chartInfo, id, chartId, url, $loading, $noData, $error)) {
            google.charts.load('current', {packages: chartSettings[id]['packages']});
            google.charts.setOnLoadCallback(function () {
              drawChart(id);
            });
          }
        }).fail(function () {
          $loading.hide();
          $noData.hide();
          $error.show();
          console.error(
            Drupal.t(
              'An error occurred loading data for chart !id', {'!id': id},
            )
          );
        });

      });

    }
  };
})(jQuery, Drupal, once);
