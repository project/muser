DROP VIEW IF EXISTS muser_applications;

CREATE VIEW muser_applications AS
SELECT f.id AS fid, f.uid AS application_uid, f.entity_id AS project_round_nid,
       fr.field_round_target_id AS round_nid, fp.field_project_target_id AS project_nid,
       nfd.uid AS project_uid,
       fis.field_is_submitted_value AS is_submitted,
       IFNULL(fuc.field_use_contract_value, 0) AS is_contracted,
       IFNULL(csm.field_contract_signed_mentor_value, 0) AS contract_signed_mentor,
       UNIX_TIMESTAMP(cdm.field_contract_date_mentor_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS contract_date_mentor,
       IFNULL(css.field_contract_signed_student_value, 0) AS contract_signed_student,
       UNIX_TIMESTAMP(cds.field_contract_date_student_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS contract_date_student,
       UNIX_TIMESTAMP(sd.field_submitted_date_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS submitted_date,
       UNIX_TIMESTAMP(rd.field_review_date_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS review_date,
       UNIX_TIMESTAMP(dd.field_decision_date_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS decision_date,
       fs.field_status_value AS `status`,
       f.created,
       UNIX_TIMESTAMP(lu.field_last_updated_value) + TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW()) AS last_updated
FROM flagging f
       INNER JOIN node__field_round fr ON fr.entity_id = f.entity_id AND fr.bundle = 'project_round'
       INNER JOIN node__field_project fp ON fp.entity_id = f.entity_id AND fp.bundle = 'project_round'
       INNER JOIN flagging__field_is_submitted fis ON fis.entity_id = f.id AND fis.bundle = f.flag_id
       INNER JOIN flagging__field_status fs ON fs.entity_id = f.id AND fs.bundle = f.flag_id
       LEFT JOIN flagging__field_contract_signed_mentor csm ON csm.entity_id = f.id AND csm.bundle = f.flag_id
       LEFT JOIN flagging__field_contract_signed_student css ON css.entity_id = f.id AND css.bundle = f.flag_id
       LEFT JOIN flagging__field_contract_date_mentor cdm ON cdm.entity_id = f.id AND cdm.bundle = f.flag_id
       LEFT JOIN flagging__field_contract_date_student cds ON cds.entity_id = f.id AND cds.bundle = f.flag_id
       LEFT JOIN flagging__field_last_updated lu ON lu.entity_id = f.id AND lu.bundle = f.flag_id
       LEFT JOIN flagging__field_submitted_date sd ON sd.entity_id = f.id AND sd.bundle = f.flag_id
       LEFT JOIN flagging__field_review_date rd ON rd.entity_id = f.id AND rd.bundle = f.flag_id
       LEFT JOIN flagging__field_decision_date dd ON dd.entity_id = f.id AND dd.bundle = f.flag_id
       INNER JOIN node_field_data nfd ON nfd.nid = fp.field_project_target_id
       LEFT JOIN node__field_use_contract fuc ON fuc.entity_id = fp.field_project_target_id AND fuc.deleted = 0
WHERE (f.flag_id = 'favorites')
  AND (f.entity_type = 'node')
  AND nfd.status = 1;


DROP VIEW IF EXISTS muser_applications_aggregation;

CREATE VIEW muser_applications_aggregation AS
SELECT
  fid, project_round_nid, round_nid, project_nid, project_uid,
  IF(fid IS NOT NULL, 1, NULL) favorited,
  IF(is_submitted = 1, 1, NULL) submitted,
  IF(is_submitted = 0, 1, NULL) not_submitted,
  IF(`status` = 'pending' AND is_submitted = 1, 1, NULL) pending,
  IF(review_date IS NOT NULL, 1, NULL) was_reviewed,
  IF(`status` = 'in_review', 1, NULL) in_review,
  IF(`status` IN ('pending', 'in_review') AND is_submitted = 1, 1, NULL) no_decision,
  IF(`status` = 'accepted', 1, NULL) accepted,
  IF(`status` = 'declined', 1, NULL) declined,
  IF(`status` = 'accepted' AND is_contracted = 1, 1, NULL) contract_required,
  IF(`status` = 'accepted' AND contract_signed_mentor = 1, 1, NULL) contract_signed_mentor,
  IF(`status` = 'accepted' AND contract_signed_student = 1, 1, NULL) contract_signed_student
FROM muser_applications;


DROP VIEW IF EXISTS muser_applications_counts;

CREATE VIEW muser_applications_counts AS
SELECT project_round_nid, round_nid, project_nid, project_uid,
COUNT(favorited) AS favorited,
COUNT(not_submitted) AS not_submitted, COUNT(submitted) AS submitted,
COUNT(pending) AS pending, COUNT(in_review) AS in_review, COUNT(no_decision) AS no_decision,
COUNT(accepted) AS accepted, COUNT(declined) AS declined,
COUNT(contract_required) AS contract_required,
COUNT(contract_signed_mentor) AS contract_signed_mentor, COUNT(contract_signed_student) AS contract_signed_student,
(COUNT(contract_required) - COUNT(contract_signed_mentor)) AS contract_required_mentor,
(COUNT(contract_required) - COUNT(contract_signed_student)) AS contract_required_students
FROM muser_applications_aggregation maa
GROUP BY project_round_nid, round_nid, project_nid, project_uid;


DROP VIEW IF EXISTS muser_round_projects;

CREATE VIEW muser_round_projects AS
SELECT np.nid AS project_nid, np.title, np.uid AS project_uid, np.status, np.created, np.changed,
       fr.entity_id AS project_round_nid, fr.field_round_target_id AS round_nid
FROM node_field_data np
       INNER JOIN node__field_project fp ON fp.field_project_target_id = np.nid
       INNER JOIN node__field_round fr ON fr.entity_id = fp.entity_id
WHERE np.type = 'project';


DROP VIEW IF EXISTS muser_login_history;

CREATE VIEW muser_login_history AS
SELECT lh.uid, lh.login,
  IF(ur.roles_target_id = 'student', 1, NULL) is_student,
  IF(ur.roles_target_id = 'mentor', 1, NULL) is_mentor
FROM login_history lh
INNER JOIN user__roles ur ON ur.entity_id = lh.uid;


DROP VIEW IF EXISTS muser_login_history_aggregated;

CREATE VIEW muser_login_history_aggregated AS
SELECT uid, login, is_student, is_mentor
FROM (
  SELECT lh.uid, MIN(lh.login) AS login,
    FROM_UNIXTIME(lh.login, '%Y-%m-%d') as 'login_date',
    MAX(IF(ur.roles_target_id = 'student', 1, NULL)) is_student,
    MAX(IF(ur.roles_target_id = 'mentor', 1, NULL)) is_mentor
  FROM login_history lh
  INNER JOIN user__roles ur ON ur.entity_id = lh.uid
  GROUP BY lh.uid, login_date
) AS lha;


DROP VIEW IF EXISTS muser_application_action_dates;

CREATE VIEW muser_application_action_dates AS
SELECT fid, round_nid, 'favorited' AS action, created AS action_date
FROM muser_applications
UNION
  SELECT fid, round_nid, 'submitted' AS action, submitted_date AS action_date
  FROM muser_applications
  WHERE is_submitted = 1
  AND submitted_date IS NOT NULL
UNION
  SELECT fid, round_nid, 'reviewed' AS action, review_date AS action_date
  FROM muser_applications
  WHERE is_submitted = 1
  AND review_date IS NOT NULL
UNION
  SELECT fid, round_nid, 'decided' AS action, decision_date AS action_date
  FROM muser_applications
  WHERE is_submitted = 1
  AND decision_date IS NOT NULL
UNION
  SELECT fid, round_nid, 'declined' AS action, decision_date AS action_date
  FROM muser_applications
  WHERE is_submitted = 1
  AND status = 'declined'
  AND decision_date IS NOT NULL
UNION
  SELECT fid, round_nid, 'accepted' AS action, decision_date AS action_date
  FROM muser_applications
  WHERE is_submitted = 1
  AND status = 'accepted'
  AND decision_date IS NOT NULL
UNION
  SELECT fid, round_nid, 'contract_signed_student' AS action, contract_date_student AS action_date
  FROM muser_applications
  WHERE is_submitted  = 1
  AND is_contracted = 1
  AND contract_date_student IS NOT NULL
UNION
  SELECT fid, round_nid, 'contract_signed_mentor' AS action, contract_date_mentor AS action_date
  FROM muser_applications
  WHERE is_submitted  = 1
  AND is_contracted = 1
  AND contract_date_mentor IS NOT NULL;


DROP VIEW IF EXISTS muser_application_action_dates_summary;

CREATE VIEW muser_application_action_dates_summary AS
SELECT fid, round_nid, action_date,
  IF(`action` = 'favorited', 1, NULL) favorited,
  IF(`action` = 'submitted', 1, NULL) submitted,
  IF(`action` = 'reviewed', 1, NULL) reviewed,
  IF(`action` = 'decided', 1, NULL) decided,
  IF(`action` = 'declined', 1, NULL) declined,
  IF(`action` = 'accepted', 1, NULL) accepted,
  IF(`action` = 'contract_signed_student', 1, NULL) contract_signed_student,
  IF(`action` = 'contract_signed_mentor', 1, NULL) contract_signed_mentor
FROM muser_application_action_dates;


DROP VIEW IF EXISTS muser_user_actions_aggregated;

CREATE VIEW muser_user_actions_aggregated AS
SELECT uid, action_timestamp, is_student, is_mentor
FROM (
       SELECT mua.uid, MIN(mua.timestamp) AS action_timestamp,
         FROM_UNIXTIME(mua.timestamp, '%Y-%m-%d') as 'action_timestamp_date',
         MAX(IF(ur.roles_target_id = 'student', 1, NULL)) is_student,
         MAX(IF(ur.roles_target_id = 'mentor', 1, NULL)) is_mentor
      FROM muser_user_actions mua
        LEFT JOIN user__roles ur ON ur.entity_id = mua.uid
      GROUP BY mua.uid, action_timestamp_date
     ) AS muaa;
