<?php

namespace Drupal\muser_system\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\muser_system\RoundDatesTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RelatedProjectsPlaceholder' block.
 *
 * @Block(
 *  id = "muser_related_projects_placeholder_block",
 *  admin_label = @Translation("Muser Related Projects Placeholder"),
 * )
 */
class RelatedProjectsPlaceholder extends BlockBase implements ContainerFactoryPluginInterface {

  use RoundDatesTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build[] = [
      '#attached' => [
        'library' => ['muser_system/related-projects'],
      ],
      '#type' => 'inline_template',
      '#template' => '<div class="related-projects" aria-live="polite"><div class="related-projects__title-section"><div class="related-projects__autoclose-timer"><div class="related-projects__autoclose-timer-bar"></div></div><h2 class="related-projects__title related-projects__title--is-plural"><span class="related-projects__project-count"></span><span class="related-projects__title-text-singular">' . $this->t('Related Project') . '</span><span class="related-projects__title-text-plural">Related Projects</span></h2><button class="related-projects__minimize"><i class="fa fa-chevron-down"></i></button></div><div class="related-projects__reason" id="related-projects__reason"></div><div id="related-projects"></div></div>',
    ];

    return $build;

  }

}
