/**
 * @file
 * Message template overrides.
 */

((Drupal) => {
  /**
   * Overrides message theme function.
   *
   * @param {object} message
   *   The message object.
   * @param {string} message.text
   *   The message text.
   * @param {object} options
   *   The message context.
   * @param {string} options.type
   *   The message type.
   * @param {string} options.id
   *   ID of the message, for reference.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.message = ({ text }, { type, id }) => {
    const messagesTypes = Drupal.Message.getMessageTypeLabels();
    const messageWrapper = document.createElement('div');
    let icon;
    switch (type) {
      case 'error':
        icon = 'fas fa-times-circle';
        break;
      case 'warning':
        icon = 'fas fa-exclamation-triangle';
        break;
      default:
        icon = 'fas fa-check-circle';
    }

    messageWrapper.setAttribute(
      'class',
      `messages messages--${type === 'status' ? 'success' : type} messages--${type} messages-list__item`,
    );
    messageWrapper.setAttribute(
      'role',
      type === 'error' || type === 'warning' ? 'alert' : 'status',
    );
    messageWrapper.setAttribute('aria-labelledby', `${id}-title`);
    messageWrapper.setAttribute('data-drupal-message-id', id);
    messageWrapper.setAttribute('data-drupal-message-type', type);

    messageWrapper.innerHTML = `
      <span class="message__icon"><i class="${icon}" aria-hidden="true"></i></span>
      <h2 id="${id}-title" class="visually-hidden">${messagesTypes[type]}</h2>
      ${text}
  `;
    return messageWrapper;
  };
})(Drupal);
