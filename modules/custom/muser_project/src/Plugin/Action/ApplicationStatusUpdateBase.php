<?php

namespace Drupal\muser_project\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldUpdateActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Base class for flagging status update actions.
 */
abstract class ApplicationStatusUpdateBase extends FieldUpdateActionBase {

  /**
   * Account of user updating status values.
   *
   * @var \Drupal\Core\Session\AccountInterface;
   */
  private AccountInterface|null $account;

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {

    $this->account = $account;

    $round = muser_project_get_round_for_flagging($object);
    if (!$round || !muser_project_round_in_period($round->id(), 'acceptance')) {
      $result = AccessResult::forbidden()
        ->addCacheableDependency($object);
    }
    else {
      $access_service = \Drupal::service('muser_project.flagging_access_check');
      $result = $access_service->access($account, $object);
    }

    return $return_as_object ? $result : $result->isAllowed();

  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $objects) {
    parent::executeMultiple($objects);
    // Clear the review info block's build array cache.
    Cache::invalidateTags(['application_review_count:' . $this->account->id()]);
  }

}
