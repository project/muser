<?php

namespace Drupal\muser_system\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\muser_system\RoundDatesTrait;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RoundCountDown' block.
 *
 * @Block(
 *  id = "muser_round_countdown_block",
 *  admin_label = @Translation("Muser round countdown block"),
 * )
 */
class RoundCountDown extends BlockBase implements ContainerFactoryPluginInterface {

  use RoundDatesTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => ['current_round'],
      ],
    ];
    if ($round_nid = muser_project_get_current_round()) {
      $round = Node::load($round_nid);
    }

    if (empty($round)) {
      return $build;
    }

    $data = $this->getRoundData($round);

    $build[] = [
      '#attached' => [
        'library' => ['muser_system/round-countdown'],
      ],
      '#type' => 'inline_template',
      '#template' => '<script>muserRoundData = ' . json_encode($data) . '</script>
        <div class="round-countdown__text"><i class="far fa-clock"></i> <span class="round-countdown__label">Countdown...</span> <span class="round-countdown__time"></span><button class="round-countdown__action round-countdown__action--show-disclaimer"><i class="far fa-info-circle"></i></button></div>
        <button class="round-countdown__action round-countdown__action--collapse"><i class="far fa-times"></i></button>
        <button class="round-countdown__action round-countdown__action--expand" style="display: none"><i class="far fa-clock"></i></button>
        <div class="round-countdown__disclaimer" style="display:none;">'
        . $this->t('This timer uses the clock on your device and is only an estimate. The actual time remaining is determined on the website.')
        . '</div>',
      '#cache' => [
        'tags' => [
          'node:' . $round_nid,
          'current_round',
        ],
      ],
    ];

    $renderer = \Drupal::service('renderer');
    $renderer->addCacheableDependency($build, $round);

    return $build;

  }

}
