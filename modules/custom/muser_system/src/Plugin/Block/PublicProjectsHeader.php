<?php

namespace Drupal\muser_system\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Public Projects Header' block.
 *
 * @Block(
 *  id = "muser_public_projects_header_block",
 *  admin_label = @Translation("Muser public projects header block"),
 * )
 */
class PublicProjectsHeader extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * CopyrightMessage constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => ['config:muser_system.settings'],
      ],
    ];

    $config = $this->configFactory->get('muser_system.settings');
    $message = $config->get('projects_page_message');
    if (!empty($message['value'])) {
      $build['projects_page_message'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          $config->get('projects_page_message_type') => [
            check_markup($message['value'], $message['format']),
          ],
        ],
      ];
    }

    return $build;

  }

}
