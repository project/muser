<?php

namespace Drupal\muser_system\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'ColorFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "muser_system_colorformatter",
 *   label = @Translation("Color Swatch Formatter"),
 *   field_types = {
 *     "string",
 *     "list_string"
 *   }
 * )
 */
class ColorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    /**
     * @var integer $delta
     * @var \Drupal\options\Plugin\Field\FieldType\ListStringItem $item
     */
    foreach ($items as $delta => $item) {
      $options = $item->getFieldDefinition()->getSetting('allowed_values');
      $value = $options[$item->value] ?? $item->value;
      $element[$delta] = [
        '#markup' => '<span class="color-swatch color-swatch--' . $item->value . '"><span class="color-swatch__text">' . $value . '</span></span>',
      ];
    }

    return $element;
  }

}
