<?php

namespace Drupal\muser_system\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\Entity\File;

/**
 * Class MuserConfig.
 */
class MuserConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'muser_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'muser_system.settings',
      'muser_base.settings',
      'system.site',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $basic_site_config = $this->config('system.site');

    $form['basic_site_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Basic Site Settings'),
      '#open' => TRUE,
    ];
    $form['basic_site_settings']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => $basic_site_config->get('name'),
    ];
    $form['basic_site_settings']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#description' => $this->t("The <i>From</i> address in automated emails sent during registration and new password requests, and other notifications.<br>Use an address ending in your site's domain to help prevent this email being flagged as spam."),
      '#default_value' => $basic_site_config->get('mail'),
    ];

    $config = $this->config('muser_system.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General information'),
      '#open' => TRUE,
    ];
    $form['general']['school_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School name'),
      '#description' => $this->t('The name of the college or university. May be used as a token in emails, etc.'),
      '#required' => TRUE,
      '#default_value' => $config->get('school_name'),
    ];

    $form['general']['blog_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Blog title'),
      '#description' => $this->t('The title to use for the site Blog. Note that any menu items will need to be changed separately.'),
      '#required' => TRUE,
      '#default_value' => $config->get('blog_title') ?: $this->t('Blog'),
    ];

    $form['general']['blog_block_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Blog block title'),
      '#description' => $this->t('The title to use for the block on the home page that shows recent blog posts.'),
      '#required' => TRUE,
      '#default_value' => $config->get('blog_block_title') ?: $this->t('Recent Blog Posts'),
    ];

    $form['general']['copyright_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Copyright message text'),
      '#description' => $this->t('If set, this text will be displayed in the footer after the year.'),
      '#default_value' => $config->get('copyright_message'),
    ];

    $form['rounds'] = [
      '#type' => 'details',
      '#title' => $this->t('Rounds'),
      '#open' => TRUE,
    ];
    $form['rounds']['default_num_applications'] = [
      '#type' => 'number',
      '#title' => $this->t('Default number of applications per Student per Round'),
      '#description' => $this->t('The default number of applications per student that will be allowed for a new Round. Can be changed per-Round.<br/>Setting this to 0 for a Round means that there is no limit.'),
      '#size' => 5,
      '#default_value' => $config->get('default_num_applications') ?? MUSER_NUM_APPLICATIONS,
      '#min' => 0,
      '#attributes' => [
        'step' => 1,
      ],
    ];

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance'),
      '#open' => FALSE,
    ];
    $form['appearance']['appearance_info'] = [
      '#type' => 'markup',
      '#markup' => '<p>'
        . $this->t('Optional. Here you may set a custom logo and favicon for the site. If these are not set, Muser defaults will be used.')
        . '</p><p>'
        . $this->t('If set, click "Remove" and re-save settings to revert to Muser defaults.')
        . '</p>',
    ];
    $form['appearance']['logo_fid'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Logo'),
      '#upload_location' => 'public://muser/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif jpg jpeg png svg'),
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif jpg jpeg png svg'],
        'file_validate_size' => [2000000],
      ],
      '#default_value' => [$config->get('logo_fid') ?? ''],
    ];
    $form['appearance']['favicon_fid'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Favicon'),
      '#upload_location' => 'public://muser/',
      '#multiple' => FALSE,
      '#description' => $this->t('Your shortcut icon, or favicon, is displayed in the address bar and bookmarks of most browsers.<br>Allowed extensions: gif jpg jpeg png'),
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif jpg jpeg png'],
        'file_validate_size' => [2000000],
      ],
      '#default_value' => [$config->get('favicon_fid') ?? ''],
    ];

    $form['application_help'] = [
      '#type' => 'details',
      '#title' => $this->t('Application help for students'),
      '#open' => FALSE,
    ];

    $form['application_help']['application_process'] = [
      '#type' => 'details',
      '#title' => $this->t('Application process'),
      '#open' => TRUE,
    ];
    $form['application_help']['application_process']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('The first time a new Student favorites a Project, a pop-up window will open telling them what to do next.'),
    ];
    $form['application_help']['application_process']['application_process_instructions_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pop-up window title'),
      '#default_value' => $config->get('application_process_instructions_title') ?? NULL,
    ];
    $value = $config->get('application_process_instructions_content');
    $form['application_help']['application_process']['application_process_instructions_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Pop-up window text'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];

    $form['application_help']['essay_guidelines'] = [
      '#type' => 'details',
      '#title' => $this->t('Essay'),
      '#open' => TRUE,
    ];
    $form['application_help']['essay_guidelines']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('These guidelines will be shown on the "My applications" page to help Students with their essay. This might mention a recommended length, remind them to not include their name, etc.'),
    ];
    $value = $config->get('application_essay_guidelines');
    $form['application_help']['essay_guidelines']['application_essay_guidelines'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Guidelines'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];

    $definition = FieldConfig::loadByName('flagging', 'favorites', 'field_status');
    $settings = $definition->getSettings();
    $form['#application_statuses'] = $settings['allowed_values'];
    $form['status'] = [
      '#type' => 'details',
      '#title' => $this->t('Application status values'),
      '#open' => FALSE,
    ];
    $form['status']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('These are the descriptions of the status values for <em>Applications</em>.'),
    ];
    foreach ($form['#application_statuses'] as $key => $text) {
      $name = 'application_status_' . $key;
      $value = $config->get($name);
      $form['status'][$name] = [
        '#type' => 'text_format',
        '#title' => $text,
        '#format' => $value['format'] ?? 'basic_html',
        '#default_value' => $value['value'] ?? NULL,
      ];
    } // Loop thru status values.

    $form['contracts'] = [
      '#type' => 'details',
      '#title' => $this->t('Expectations and Structure agreements'),
      '#open' => FALSE,
    ];

    $current_round = muser_project_get_current_round(TRUE);

    $form['contracts']['contracts_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Expectations and Structure agreements?'),
      '#description' => $this->t('If checked, an additional date set will be added to the end of each round for Expectations and Structure agreements management. Enabling this setting will add additional email templates.'),
      '#default_value' => $config->get('contracts_enabled') ?? FALSE,
    ];

    if (!_muser_system_can_change_contracts_enabled()) {
      $form['contracts']['contracts_enabled']['#disabled'] = 'disabled';
      $form['contracts']['contracts_enabled_help'] = [
        '#type' => 'markup',
        '#markup' => $this->t("You can not change this setting due to the <a href='@link' target='_blank'>currently active round</a>.", ["@link" => $current_round->toUrl()->toString()]),
      ];
    }
    else {
      $form['contracts']['contracts_enabled_help'] = [
        '#type' => 'markup',
        '#markup' => $this->t("You can not change this setting during an active round. If you want to use Expectations and Structure agreements, you must enable them before the round starts."),
      ];
    }

    $form['contracts']['contracts_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Expectations and Structure Agreements Settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['contracts']['contracts_settings']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('These fields are used for various parts of the Expectations and Structure agreements feature.'),
      '#states' => [
        'visible' => [
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['contracts']['contracts_settings']['contract_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expectations and Structure agreements label'),
      '#description' => $this->t('This is what will be displayed next to the checkbox on the project form. An example of this text might be "Use Expectations and Structure agreements on this project?"'),
      '#default_value' => $config->get('contract_label'),
      '#states' => [
        'visible' => [
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $value = $config->get('contract_description');
    $form['contracts']['contracts_settings']['contract_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Expectations and Structure agreement description'),
      '#default_value' => $config->get('contract_description'),
      '#description' => $this->t('Appears below the checkbox and label set above. This can be used for clarification around what is expected of mentors that choose to offer an Expectations and Structure agreement for their project.'),
      '#states' => [
        'visible' => [
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['contracts']['contracts_settings']['enable_contract_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Expectations and Structure agreements pop-up window?'),
      '#default_value' => $config->get('enable_contract_modal') ?? FALSE,
      '#description' => $this->t('When enabled, this will open a pop-up window when a mentor checks the "Use a Mentor-Student Expectations and Structure agreements..." checkbox to ensure they understand the requirements.'),
      '#states' => [
        'visible' => [
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['contracts']['contracts_settings']['contract_require_short_confirmation_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require confirmation text?'),
      '#default_value' => $config->get('contract_require_short_confirmation_text') ?? TRUE,
      '#description' => $this->t('When enabled, mentors will be required to type "I Understand" to check the box, instead of just clicking "accept".'),
      '#states' => [
        'visible' => [
          ':input[name="enable_contract_modal"]' => ['checked' => TRUE],
          ':input[name="contracts_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $value = $config->get('contract_short_confirmation_text');
    $form['contracts']['contracts_settings']['contract_short_confirmation_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agreement pop-up window short confirmation text'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $config->get('contract_short_confirmation_text') ?? $this->t("I understand"),
      '#description' => $this->t('The text that mentors must enter (case-insensitive) in order to click the "accept" button.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_contract_modal"]' => ['checked' => TRUE],
          ':input[name="contract_require_short_confirmation_text"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $value = $config->get('contract_modal_text');
    $form['contracts']['contracts_settings']['contract_modal_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Agreement pop-up window text'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
      '#description' => $this->t("This text will be displayed in the pop-up window described above. There's no need to describe the confirmation text that the mentor must enter, it will already be there."),
      '#states' => [
        'visible' => [
          ':input[name="enable_contract_modal"]' => ['checked' => TRUE],
          ':input[name="contract_require_short_confirmation_text"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['dashboard'] = [
      '#type' => 'details',
      '#title' => $this->t('Dashboard'),
      '#open' => FALSE,
    ];
    $form['dashboard']['dashboard_info'] = [
      '#type' => 'markup',
      '#markup' => '<p>'
      . $this->t('Select charts to be shown on the admin Dashboard.')
      . '</p><p>'
      . $this->t('Note that some charts may not be displayed depending on the active period of the current round. For example, the "Student Actions by Day" chart will not be shown during the project posting period.')
      . '</p>',
    ];
    /** @var \Drupal\muser_system\Charts $charts */
    $charts = \Drupal::service('muser_system.charts');
    $options = [];
    foreach ($charts->chartsInfo as $chart_id => $info) {
      if (str_contains($chart_id, 'agreement') && !$config->get('contracts_enabled')) {
        // Contracts are not enabled-- don't show contract charts.
        continue;
      }
      if (!empty($info['args']['required_parameters'])) {
        // Can't use this chart because parameters are needed.
        continue;
      }
      if (!empty($info['args']['multi_round'])) {
        // Can't use this on dashboard because it's not for a single round.
        continue;
      }
      $options[$chart_id] = $info['label'];
    }
    $form['dashboard']['dashboard_charts_to_show'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Charts to show'),
      '#options' => $options,
      '#default_value' => $config->get('dashboard_charts_to_show') ?? [],
    ];

    $form['hero_block'] = [
      '#type' => 'details',
      '#title' => $this->t('Hero block'),
      '#open' => FALSE,
    ];

    $form['hero_block']['hero_block_info'] = [
      '#type' => 'markup',
      '#markup' => '<p>'
        . $this->t('The "Hero" block (shown in the colored section at the top of the home page by default) can display a carousel and/or a custom message for users based on their role.')
        . '</p>',
    ];

    $query = \Drupal::database()->select('block_content_field_data', 'bc');
    $query->addField('bc', 'id');
    $query->addField('bc', 'info');
    $query->condition('bc.type', 'carousel');
    $query->orderBy('bc.info');
    $options = [];
    foreach ($query->execute() as $row) {
      $options[$row->id] = $row->info;
    }

    if ($options) {
      $form['hero_block']['hero_block_display_order'] = [
        '#type' => 'select',
        '#title' => $this->t('Display order'),
        '#description' => $this->t('Determines the order in which the Hero message and carousel will be shown.'),
        '#options' => [
          'message_then_carousel' => $this->t('Show Hero message first, then carousel'),
          'carousel_then_message' => $this->t('Show carousel first, then Hero message'),
        ],
        '#default_value' => $config->get('hero_block_display_order') ?? 'message_then_carousel',
        '#states' => [
          'visible' => [
            ':input[name="show_hero_block_messages"]' => ['checked' => TRUE],
            ':input[name="hero_block_carousel_bid"]' => ['!value' => ''],
          ],
        ],
      ];

      $form['hero_block']['hero_block_carousel_bid'] = [
        '#type' => 'select',
        '#title' => $this->t('Carousel block'),
        '#description' => $this->t('If set, this Carousel block will be shown at the top of the site home page.'),
        '#options' => ['' => t('- Select block -')] + $options,
        '#default_value' => $config->get('hero_block_carousel_bid'),
      ];
    }
    else {
      $form['hero_block']['hero_block_carousel'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . $this->t('No Carousel blocks exist.') . '</p>',
      ];
    }

    $options = [
      'block_content_type' => 'carousel',
      'destination' => '/admin/config/system/muser',
    ];
    $url = Url::fromRoute('block_content.add_form', $options)->toString();
    $form['hero_block']['hero_block_carousel_create'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Create a new <a href="@url">Carousel block</a>.', ['@url' => $url]) . '</p>',
    ];

    $form['hero_block']['show_hero_block_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Hero block messages (below) at the top of the site home page.'),
      '#default_value' => $config->get('show_hero_block_messages'),
    ];
    $form['hero_block']['hero_block_messages'] = [
      '#type' => 'details',
      '#title' => $this->t('Messages'),
      '#open' => TRUE,
    ];
    $form['hero_block']['hero_block_messages']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('These are the messages that will be shown to users with each role. May include tokens.'),
    ];
    $value = $config->get('hero_block_anonymous');
    $form['hero_block']['hero_block_messages']['hero_block_anonymous'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Anonymous'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];
    $value = $config->get('hero_block_student');
    $form['hero_block']['hero_block_messages']['hero_block_student'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Student'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];
    $value = $config->get('hero_block_mentor');
    $form['hero_block']['hero_block_messages']['hero_block_mentor'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Mentor'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];
    $form['hero_block']['hero_block_messages']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user', 'muser'],
      '#show_restricted' => TRUE,
    ];

    $url = Url::fromRoute('view.projects.page')->toString();
    $form['projects_page'] = [
      '#type' => 'details',
      '#title' => $this->t('Projects page'),
      '#open' => FALSE,
    ];
    $form['projects_page']['projects_page_help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Optional. Specify a message to be shown at the top of the public <a target="_blank" href="@url">Projects</a> page.<br>Leave blank to show no message.', ['@url' => $url]),
    ];
    $form['projects_page']['projects_page_message_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message type'),
      '#description' => $this->t('This will control the color and styling of the message box.'),
      '#options' => [
        'status' => $this->t('Status'),
        'warning' => $this->t('Warning'),
      ],
      '#default_value' => $config->get('projects_page_message_type') ?? 'status',
    ];
    $value = $config->get('projects_page_message');
    $form['projects_page']['projects_page_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message text'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];

    $form['per_round_review'] = [
      '#type' => 'details',
      '#title' => $this->t('Review profile pop-up messages'),
      '#open' => FALSE,
    ];

    $form['per_round_review']['per_round_review_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable review profile pop-up'),
      '#description' => $this->t('When checked, this will enable a pop-up that will appear once per round for all users to remind them to review their profile. The pop-up text can be configured.'),
      '#default_value' => $config->get('per_round_review_profile') ?? FALSE,
    ];

    $form['per_round_review']['help_text'] = [
      '#type' => 'markup',
      '#id' => "per_round_review__help_text",
      '#markup' => $this->t('If enabled, you can set the messages that will be shown in review profile pop-up.'),
    ];

    $value = $config->get('per_round_review_profile_text_student');
    $form['per_round_review']['per_round_review_profile_text_student'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Student message'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
      '#states' => [
        'visible' => [
          ':input[name="per_round_review_profile"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $value = $config->get('per_round_review_profile_text_mentor');
    $form['per_round_review']['per_round_review_profile_text_mentor'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Mentor message'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
      '#states' => [
        'visible' => [
          ':input[name="per_round_review_profile"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Role descriptions'),
      '#open' => FALSE,
    ];
    $form['roles']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('These are the descriptions of the <em>Student</em> and <em>Mentor</em> roles that users will see when completing their profile.'),
    ];
    $value = $config->get('role_description_student');
    $form['roles']['role_description_student'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Student'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];
    $value = $config->get('role_description_mentor');
    $form['roles']['role_description_mentor'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Mentor'),
      '#format' => $value['format'] ?? 'basic_html',
      '#default_value' => $value['value'] ?? NULL,
    ];

    $form['student_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Student information'),
      '#open' => FALSE,
    ];
    $url = Url::fromRoute('view.student_information.page')->toString();
    $form['student_information']['allow_student_info_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow students to opt-in to sharing information with all mentors?'),
      '#description' => $this->t('If checked, students will be able to opt-in to sharing their information with all mentors and mentors will have access to <a href="@url">a page showing information for all students who have opted-in</a>.', ['@url' => $url]),
      '#default_value' => $config->get('allow_student_info_access') ?? FALSE,
    ];
    $url = Url::fromRoute('entity.taxonomy_vocabulary.overview_form', ['taxonomy_vocabulary' => 'demographic_information'])->toString();
    $form['student_information']['request_demographic_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Request demographic information from students?'),
      '#description' => $this->t('If checked, students will see a "Demographic information" field on their user profile.<br>Options for this field are set on the <a href="@url">Demographic information taxonomy vocabulary page</a>.', ['@url' => $url]),
      '#default_value' => $config->get('request_demographic_info') ?? FALSE,
    ];
    $form['student_information']['allow_demographic_info_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include student demographic information on profile and applications?'),
      '#description' => $this->t('If checked, site admins and mentors will see demographic information of students.<br>On applications, mentors will see demographic information only after blind review is complete.'),
      '#default_value' => $config->get('allow_demographic_info_access') ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="request_demographic_info"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $options = ['drupal' => $this->t('Standard Drupal user management')];
    $module_handler = \Drupal::moduleHandler();
    if ($module_handler->moduleExists('basicshib')) {
      $options['basicshib'] = $this->t('Basic Shibboleth Authentication');
    }
    if ($module_handler->moduleExists('miniorange_saml')) {
      $options['miniorange_saml'] = $this->t('miniOrange SAML authentication');
    }
    if ($module_handler->moduleExists('simplesamlphp_auth')) {
      $options['simplesamlphp_auth'] = $this->t('simpleSAMLphp authentication');
    }

    $form['user_management'] = [
      '#type' => 'details',
      '#title' => $this->t('User management'),
      '#open' => FALSE,
    ];
    if (count($options) > 1) {
      $form['user_management']['user_login_method'] = [
        '#type' => 'radios',
        '#title' => $this->t('User login method'),
        '#description' => $this->t('How the site will manage user logins.'),
        '#options' => $options,
        '#default_value' => $config->get('user_login_method') ?? 'drupal',
      ];

      $form['user_management']['disable_login_page'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Disable Drupal login page?'),
        '#description' => $this->t('This will disable the standard Drupal login page found at /user/login.'),
        '#default_value' => $config->get('disable_login_page') ?? FALSE,
        "#states" => [
          'visible' => [
            ':input[name="user_login_method"]' => ['!value' => 'drupal'],
          ],
        ],
      ];
      $form['user_management']['enable_muser_login_page'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable custom login page?'),
        '#description' => $this->t('This will enable a custom login page found at /muser/login.<br>Will be helpful if admin users do not have access to SSO for some reason.'),
        '#default_value' => $config->get('enable_muser_login_page') ?? FALSE,
        "#states" => [
          'visible' => [
            ':input[name="user_login_method"]' => ['!value' => 'drupal'],
            ':input[name="disable_login_page"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['user_management']['disable_forgot_password'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Disable "Forgot password" page?'),
        '#description' => $this->t('This will disable the "Forgot password" page found at /user/password.'),
        '#default_value' => $config->get('disable_forgot_password') ?? FALSE,
        "#states" => [
          'visible' => [
            ':input[name="user_login_method"]' => ['!value' => 'drupal'],
          ],
        ],
      ];
      $form['user_management']['user_management_restricted'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Restrict user management?'),
        '#description' => $this->t('If checked, users will not be allowed to change their email address, password, etc. via their User profile page.'),
        '#default_value' => $config->get('user_management_restricted') ?? FALSE,
        "#states" => [
          'visible' => [
            ':input[name="user_login_method"]' => ['!value' => 'drupal'],
          ],
        ],
      ];
    }
    else {
      $modules = [
        '<a href="https://www.drupal.org/project/basicshib">Basic Shibboleth Authentication</a>',
        '<a href="https://www.drupal.org/project/miniorange_saml">miniOrange SAML authentication</a>',
        '<a href="https://www.drupal.org/project/simplesamlphp_auth">simpleSAMLphp Authentication</a>',
      ];
      $form['user_management']['user_management_method_text'] = [
        '#type' => 'markup',
        '#markup' => '<p>'
        . $this->t('The site is using Standard Drupal user management. Install and enable Drupal SSO modules to add more options.')
        . '</p><p>'
        . $this->t('Supported modules:')
        . '</p><ul><li>' . implode('</li><li>', $modules) . '</li></ul>',
      ];
      $form['user_management']['user_management_method'] = [
        '#type' => 'hidden',
        '#value' => 'drupal',
      ];
    }

    $form['user_management']['email_restrictions'] = [
      '#type' => 'details',
      '#title' => $this->t('Email address restrictions'),
      '#states' => [
        'visible' => [
          ':input[name="user_login_method"]' => ['value' => 'drupal'],
        ],
      ],
    ];
    $form['user_management']['email_restrictions']['allowed_email_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed email address domains'),
      '#description' => $this->t('If set, users will only be allowed to use email addresses from these domains <em>when registering</em>. Administrative users will still be able to create users with any email address.<br/>Enter one per line, starting with "@" (e.g. <em>@example.com</em>).'),
      '#default_value' => $config->get('allowed_email_domains'),
    ];
    $form['user_management']['email_restrictions']['allowed_email_domains_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to users'),
      '#description' => $this->t('Will be shown on registration form with the email address field. Will also be used as the error message if they use an invalid email address.'),
      '#default_value' => $config->get('allowed_email_domains_message'),
    ];

    $options = [
      '' => $this->t('- Select a value -'),
    ];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('how_hear');
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    if (count($options) > 1) {
      $form['user_profiles'] = [
        '#type' => 'details',
        '#title' => $this->t('User profiles'),
        '#open' => FALSE,
      ];
      $form['user_profiles']['how_hear_about_other_tid'] = [
        '#type' => 'select',
        '#title' => $this->t('"Other" term for "How did you hear about Muser" profile question'),
        '#description' => $this->t('If users choose this value, they will see a "Please specify..." text field .'),
        '#options' => $options,
        '#default_value' => $config->get('how_hear_about_other_tid'),
      ];
    }
    else {
      $form['how_hear_about_other_tid'] = [
        '#type' => 'hidden',
        '#value' => '',
      ];
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $config = $this->config('muser_system.settings');
    $basic_site_config = $this->config('system.site');
    $theme_config = $this->config('muser_base.settings');

    $basic_site_config->set('name', $values['name']);
    $basic_site_config->set('mail', $values['mail']);
    $basic_site_config->save();

    $save_theme_config = FALSE;
    foreach (['logo', 'favicon'] as $image_type) {
      $key = $image_type . '_fid';
      $upload_fid = $values[$key][0] ?? '';
      if ($upload_fid != $config->get($key)) {
        // File has changed.
        $save_theme_config = TRUE;
        if ($upload_fid) {
          $file = File::load($upload_fid);
          $file->setPermanent();
          $file->save();
          $theme_value = [
            'use_default' => 0,
            'path' => $file->getFileUri(),
          ];
          if ($image_type == 'favicon') {
            $theme_value['mimetype'] = $file->getMimeType();
          }
        }
        else {
          $theme_value = [
            'use_default' => 1,
          ];
        }
        $config->set($key, $upload_fid);
        $theme_config->set($image_type, $theme_value);
      } // Has file changed?
    } // Loop thru uploaded file types.

    $config->set('school_name', $values['school_name']);
    $config->set('blog_title', $values['blog_title']);
    $config->set('blog_block_title', $values['blog_block_title']);
    $config->set('copyright_message', $values['copyright_message']);
    $config->set('default_num_applications', $values['default_num_applications']);
    $config->set('per_round_review_profile', $values['per_round_review_profile']);
    $config->set('per_round_review_profile_text_student', $values['per_round_review_profile_text_student']);
    $config->set('per_round_review_profile_text_mentor', $values['per_round_review_profile_text_mentor']);

    if (empty($values['user_login_method'])) {
      $values['user_login_method'] = 'drupal';
    }
    if ($values['user_login_method'] == 'drupal') {
      $values['user_management_restricted'] = FALSE;
      $values['disable_login_page'] = FALSE;
      $values['enable_muser_login_page'] = FALSE;
      $values['disable_forgot_password'] = FALSE;
      $config->set('allowed_email_domains', $values['allowed_email_domains']);
      $config->set('allowed_email_domains_message', $values['allowed_email_domains_message']);
    }
    else {
      $config->set('allowed_email_domains', NULL);
      $config->set('allowed_email_domains_message', NULL);
    }

    $config->set('user_login_method', $values['user_login_method']);
    $config->set('disable_login_page', $values['disable_login_page']);
    $config->set('enable_muser_login_page', $values['enable_muser_login_page']);
    $config->set('disable_forgot_password', $values['disable_forgot_password']);
    $config->set('user_management_restricted', $values['user_management_restricted']);

    $config->set('how_hear_about_other_tid', $values['how_hear_about_other_tid']);

    $config->set('allow_student_info_access', $values['allow_student_info_access']);
    $config->set('request_demographic_info', $values['request_demographic_info']);
    $config->set('allow_demographic_info_access', $values['allow_demographic_info_access']);

    if (!empty($values['hero_block_display_order'])) {
      $config->set('hero_block_display_order', $values['hero_block_display_order']);
    }
    else {
      $config->set('hero_block_display_order', 'message_then_carousel');
    }
    $config->set('show_hero_block_messages', $values['show_hero_block_messages']);
    $config->set('hero_block_carousel_bid', $values['hero_block_carousel_bid'] ?? NULL);
    $config->set('hero_block_anonymous', $values['hero_block_anonymous']);
    $config->set('hero_block_student', $values['hero_block_student']);
    $config->set('hero_block_mentor', $values['hero_block_mentor']);

    $config->set('role_description_student', $values['role_description_student']);
    $config->set('role_description_mentor', $values['role_description_mentor']);

    $config->set('contracts_enabled', $values['contracts_enabled']);
    $config->set('contract_label', $values['contract_label']);
    $config->set('contract_description', $values['contract_description']);
    $config->set('enable_contract_modal', $values['enable_contract_modal']);
    $config->set('contract_require_short_confirmation_text', $values['contract_require_short_confirmation_text']);
    $config->set('contract_short_confirmation_text', $values['contract_short_confirmation_text']);
    $config->set('contract_modal_text', $values['contract_modal_text']);

    $config->set('dashboard_charts_to_show', $values['dashboard_charts_to_show']);

    $config->set('application_essay_guidelines', $values['application_essay_guidelines']);

    $config->set('application_process_instructions_title', $values['application_process_instructions_title']);
    $config->set('application_process_instructions_content', $values['application_process_instructions_content']);

    $config->set('projects_page_message_type', $values['projects_page_message_type']);
    $config->set('projects_page_message', $values['projects_page_message']);

    foreach ($form['#application_statuses'] as $key => $text) {
      $name = 'application_status_' . $key;
      $config->set($name, $values[$name] ?? '');
    } // Loop thru status values.

    $config->save();

    // Clear the current round cache tag so the home page block will be correct.
    Cache::invalidateTags(['current_round']);

    if ($save_theme_config) {
      // Flush the cache if the theme settings changed.
      $theme_config->save();
      drupal_flush_all_caches();
    }

    parent::submitForm($form, $form_state);

  }

}
