(function ($, once) {
  // Begin jQuery
  Drupal.behaviors.muserCarouselBlock = {
    attach: function (context, settings) {
      let $carousel = $(once('carousel--processed', '.field--name-field-carousel-slides', context));

      if (!$carousel.length) {
        return;
      }

      $carousel.slick({
        dots: $carousel.children().length > 1,
        fade: true,
        nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><i class="fa fa-chevron-right"></i></button>',
        prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><i class="fa fa-chevron-left"></i></button>'
      });
    }
  };
})(jQuery, once);