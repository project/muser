<?php

namespace Drupal\muser_project\Plugin\Action;

/**
 * Provides an action to update the flagging status to Accepted.
 *
 * @Action(
 *   id = "muser_project_update_flagging_status__accepted",
 *   label = @Translation("Set selected to Accepted"),
 *   type = "flagging",
 *   category = @Translation("Muser")
 * )
 */
class ApplicationStatusUpdateAccepted extends ApplicationStatusUpdateBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToUpdate() {
    return ['field_status' => 'accepted'];
  }

}
