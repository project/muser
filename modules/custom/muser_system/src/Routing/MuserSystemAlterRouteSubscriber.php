<?php

namespace Drupal\muser_system\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class MuserSystemAlterRouteSubscriber.
 *
 * @package Drupal\muser_system\Routing
 * Listens to the dynamic route events.
 */
class MuserSystemAlterRouteSubscriber extends RouteSubscriberBase {

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    $admin_routes = ['view.rounds.page_1', 'view.administer_projects.page_1'];
    $user_routes = [
      'view.my_projects.page',
      'view.my_favorites.page',
      'view.applications.page_new',
      'view.applications.page_review',
      'view.applications.page_accepted',
      'view.applications.page_declined',
    ];

    $config = $this->configFactory->get('muser_system.settings');
    $user_login_method = $config->get('user_login_method');

    foreach ($collection->all() as $name => $route) {

      // We need to take control of this default method for the Flag controller
      // so that we can hook into its actions and add our own.
      if ($name == 'flag.action_link_flag') {
        $route->setDefault('_controller', 'Drupal\muser_system\Controller\FlagController::flag');
      }

      // Set admin routes so these use the admin theme.
      if (in_array($name, $admin_routes)) {
        $route->setOption('_admin_route', TRUE);
      }

      // Add additional bundle restriction to just show on projects to these routes.
      if ($name == 'view.administer_projects.page_1') {
        $route->setRequirement('_muser_project_bundle_access_check', 'TRUE');
      }

      // Give the user that matches the user param in the view access.
      if (in_array($name, $user_routes)) {
        $route->setRequirement('_muser_project_user_access_check', 'TRUE');
      }

      // Do not link to node add form for project_rounds.
      if ($name == 'node.add') {
        $route->setRequirement('_muser_system_node_add_access', 'TRUE');
      }

      // Alter form used for delete multiple.
      if ($name == 'entity.node.delete_multiple_form' || $name == 'node.multiple_delete_confirm') {
        $route->setDefault('_form', 'Drupal\muser_system\Form\MuserNodeDeleteMultiple');
      }

      // Disable Drupal user routes if not using Drupal auth (and set).
      if ($user_login_method !== 'drupal') {
        if ($name === 'user.login' && $config->get('disable_login_page')) {
          $route->setRequirement("_access", 'FALSE');
        }
        if ($name === 'user.pass' && $config->get('disable_forgot_password')) {
          $route->setRequirement("_access", 'FALSE');
        }
      }

    } // Loop thru collection.

  }

}
