(function ($, Drupal, once) {
  Drupal.behaviors.muserApplicationStatusCounts = {
    attach: function (context, settings) {

      let $block = $(once('muser-application-review-header-block--processed',
        '.block-muser-application-review-header-block'));

      if ($block.length === 0) {
        return;
      }

      $('button.download-by-project').on('click', function (e) {
        e.preventDefault();
        $('.download-by-project').toggle();
        return false;
      });

      let $local_tasks_block = $('.block-local-tasks-block');
      let $local_tasks = $local_tasks_block.find('.tabs a')
      let applications_per_status_data = $block.attr('data-applications-per-status');

      if (!applications_per_status_data) {
        console.log("Don't have applications_per_status_data, not updating local tasks.")
        return;
      }

      let count_per_status = JSON.parse(applications_per_status_data);

      if (count_per_status && Object.keys(count_per_status).length > 0) {
        Object.keys(count_per_status).forEach((status) => {
          let count = count_per_status[status];
          let url_status = ""
          if (status !== 'pending') {
            url_status = '/' + status.replace('_', '-');
          }
          let task_selector = '[href$="applications' + url_status + '"]'
          let $task = $local_tasks.filter(task_selector)
          if ($task.length > 0 && !isNaN(parseInt(count))) {
            $(once('task-count-processed', $task)).append('<span class="count-applications-by-status"> (' + Number(count) + ')</span>')
          }
        })
      }
    }
  }
})(jQuery, Drupal, once);
