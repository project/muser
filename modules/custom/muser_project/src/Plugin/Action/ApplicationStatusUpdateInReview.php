<?php

namespace Drupal\muser_project\Plugin\Action;

/**
 * Provides an action to update the flagging status to In Review.
 *
 * @Action(
 *   id = "muser_project_update_flagging_status__in_review",
 *   label = @Translation("Set selected to In Review"),
 *   type = "flagging",
 *   category = @Translation("Muser")
 * )
 */
class ApplicationStatusUpdateInReview extends ApplicationStatusUpdateBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToUpdate() {
    return ['field_status' => 'in_review'];
  }

}
