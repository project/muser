<?php

declare(strict_types=1);

namespace Drupal\oneclick_unsubscribe\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Configure One-Click Unsubscribe settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'oneclick_unsubscribe_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['oneclick_unsubscribe.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['mail_types'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mail types to alter'),
      '#description' => $this->t('Indicate types of mail to add "List-Unsubscribe" headers to.<br>Values should be of the format <em>module:key</em> with one value per line.<br>Use <em>module:*</em> to indicate all mail from a given module should be affected. Use <em>*</em> to indicate all mail sent should be affected.'),
      '#default_value' => $this->config('oneclick_unsubscribe.settings')->get('mail_types'),
    ];

    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('user', 'user');
    $ignore = [
      'uid',
      'uuid',
      'name',
      'pass',
      'mail',
      'langcode',
      'preferred_langcode',
      'default_langcode',
      'timezone',
      'init',
      'roles',
    ];
    $options = [];
    foreach ($fields as $id => $field) {
      if (in_array($id, $ignore)) {
        continue;
      }
      if (!($field instanceof FieldConfig)) {
        if (!method_exists($field, 'getType')) {
          continue;
        }
        if ($field->getType() != 'boolean' && $field->getType() != 'string') {
          continue;
        }
      }
      $options[$id] = $field->getLabel();
    }
    asort($options);

    $form['update_field'] = [
      '#type' => 'select',
      '#title' => $this->t('User field to update'),
      '#description' => $this->t('On a successful unsubscribe request, this user field will be updated to the value specified below.'),
      '#options' => ['' => $this->t('- Select a field -')] + $options,
      '#default_value' => $this->config('oneclick_unsubscribe.settings')->get('update_field'),
    ];
    $form['update_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User field update value'),
      '#description' => $this->t('On a successful unsubscribe request, the field above will be set to this value. If not set, field will be set to 0 or FALSE.'),
      '#default_value' => $this->config('oneclick_unsubscribe.settings')->get('update_value'),
      "#states" => [
        'visible' => [
          ':input[name="update_field"]' => ['!value' => ''],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $mail_types = array_filter(array_map('trim', explode("\n", $form_state->getValue('mail_types', ''))));
    $this->config('oneclick_unsubscribe.settings')
      ->set('mail_types', implode("\n", $mail_types))
      ->set('update_field', $form_state->getValue('update_field'))
      ->set('update_value', $form_state->getValue('update_value'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
