/**
 * @file
 * Dashboard functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserDashboard = {
    attach: function (context, settings) {

      $(once('muser-dashboard-round-select--processed', '#muser-system-round-select')).each( function () {
        let form = this;
        $(this).find('select').on('change', function () {
          form.submit();
        });
      });

      $(once('muser-dashboard-scheduled-emails--processed', 'button.scheduled-emails-toggle')).each( function () {
        $(this).on('click', function () {
          $('.scheduled-emails-toggle').toggle();
        });
      });

      $(once('stats--processed', '#dashboard-stats')).each( function () {

        let timeOut = 0;
        let url = '/api/round-data/';
        url += window.location.search;

        let updateStats = function() {

          $.get(url, {}, (data) => {
            if (!data['status']) {
              $('.dashboard-stat').html('0');
              return;
            }
            timeOut = data['timeout'];
            for (let key in data['stats']) {
              let id = '#dashboard--' + key;
              let oldValue = parseInt($(id).html());
              let newValue = parseInt(data['stats'][key]);
              if (oldValue !== newValue) {
                $(id).addClass('changing');
                setTimeout(function () {
                  $(id).html(newValue);
                  $(id).removeClass('changing');
                }, 1000);
              }
            }
            if (timeOut) {
              setTimeout(updateStats, timeOut * 1000);
            }
          });
        }

        updateStats();

      });

    }
  };
})(jQuery, Drupal, once);
