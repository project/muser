(function ($, once) {
  // Begin jQuery
  Drupal.behaviors.muserCheckboxes = {
    attach: function (context, settings) {
      let checkboxes = $('.checkbox__wrapper input', context);
      $(once('checkbox__input--processed', checkboxes)).on('change keyup keydown onfocusout focus blur', function (e) {
        let $this = $(this);
        $this.parent().removeClass('checked').addClass($this.is(':checked') ? 'checked' : '');
      });
    }
  };
  Drupal.behaviors.muserForms = {
    attach: function (context, settings) {
      let form_items = $('.js-form-item input, .js-form-item textarea', context);
      form_items.on('change keyup keydown onfocusout focus blur', function (e) {
        let $this = $(this);

        if ((e.type === 'blur' || e.type === 'change') && !$this.val()) {
          $this.parents('.js-form-item').removeClass('item-filled');
          return;
        }

        $this.parents('.js-form-item').addClass('item-filled');
      });

      let load_filled_classes = function () {
        let form_items = $('form:not(.views-exposed-form) .js-form-item input, form:not(.views-exposed-form) .js-form-item textarea', context);
        form_items.each(function () {
          $(this).change();
        });
      };

      load_filled_classes();
      $(document).ready(function () {
        load_filled_classes();
        setTimeout(load_filled_classes, 500);
      });
    }
  };
  Drupal.behaviors.muserTooltipClose = {
    attach: function (context, settings) {
      let closeable_tooltips = $(once('tooltip__close--processed','.tooltip__close', context)).click(function (e) {
        e.preventDefault();
        $(this).parents('.tooltip').addClass('tooltip--closed');
      });
    }
  };
  Drupal.behaviors.muserMobileMenu = {
    attach: function (context, settings) {
      let header_menus = $('.header__menus');
      let header_menu_region = $('.region-header-menu');

      let updateWindowSizeTabIndexes = function () {
        if (window.matchMedia('(max-width: 991px)').matches) {
          header_menus.find('a').attr('tabindex', -1);
        } else {
          header_menus.find('a').attr('tabindex', '0');
        }
      };

      updateWindowSizeTabIndexes();
      let allow_menu_call = true;
      $(window).on('resize', function () {
        if (allow_menu_call) {
          allow_menu_call = false;
          setTimeout(function () {
            allow_menu_call = true;
            updateWindowSizeTabIndexes();
          }, 100);
        }
      });
      let menu_toggle = $(once('header__menu-toggle--processed', '.header__menu-toggle', context)).click(function (e) {
        e.preventDefault();
        header_menu_region.toggleClass('header-menu--open');

        if (header_menu_region.hasClass('header-menu--open')) {
          header_menus.find('a').attr('tabindex', 0);
        } else {
          header_menus.find('a').attr('tabindex', -1);
        }
      });
      window.addEventListener('keyup', function (e) {
        e = e || window.event;
        let isEscape = false;

        if ("key" in e) {
          isEscape = e.key === "Escape" || e.key === "Esc";
        } else {
          isEscape = e.keyCode === 27;
        }

        if (isEscape) {
          $('.region-header-menu').removeClass('header-menu--open');
          header_menus.find('a').attr('tabindex', -1);
        }
      }, false);
      $(document).mouseup(function (e) {
        // if the target of the click isn't the container nor a descendant of the container
        if (header_menu_region.hasClass('header-menu--open') && !header_menu_region.is(e.target) && header_menu_region.has(e.target).length === 0) {
          header_menu_region.removeClass('header-menu--open');
          header_menus.find('a').attr('tabindex', -1);
        }
      });
    }
  };
  Drupal.behaviors.muserYoDawgIHeardYouLikeModals = {
    attach: function (context, settings) {
      let confirmation_forms = $(once('confirmation-form--processed', '.unflag-confirm-form.confirmation'));
      let destination_regex = /^.*\?.*(destination=([^&\n\r]+_wrapper_format(%3D|=)drupal_modal[^&\n\r]*)).*?$/;

      let form_action_replacer = function (match, full_query_item, destination, equal, offset, string) {
        return string.replace(destination, encodeURIComponent(window.location.pathname + window.location.search));
      };

      let link_regex = /(^.*)(\?.*)(_wrapper_format(%3D|=)drupal_modal&?)(.*?)$/;

      let link_href_replacer = function (match, path, qs_before, qs_modal, equals, qs_after, offset, string) {
        let new_qs = window.location.search.replace(/^\?/, '');

        if (new_qs.length !== 0) {
          new_qs += '&';
        }

        return string.replace(path, window.location.pathname).replace(qs_modal, new_qs);
      };

      confirmation_forms.each(function (key) {
        let confirmation_form = $(confirmation_forms[key]);
        let action = confirmation_form.attr('action'); // the following regex updates destination urls that would send you to a ajax data page.

        let new_action = action.replace(destination_regex, form_action_replacer);
        confirmation_form.attr('action', new_action); // Also update any non-ajax links that return ajax data.

        let non_ajax_links = $('a:not(.use-ajax)', confirmation_form);
        non_ajax_links.each(function (key) {
          let link = $(non_ajax_links[key]);
          let new_href = link.attr('href').replace(link_regex, link_href_replacer);
          link.attr('href', new_href);
        });
      });
    }
  };
  Drupal.behaviors.muserDropdownSideSelection = {
    attach: function (context, settings) {
      let all_details = $('details');

      let update_side_select = function (detail) {
        let $details = $(detail);
        let $details_wrapper = $('.details-wrapper', detail);
        let rect_det = $details[0].getBoundingClientRect();
        let rect_det_wrap = $details_wrapper[0].getBoundingClientRect();
        let window_width = $(window).width();

        if (rect_det.left + rect_det_wrap.width >= window_width) {
          $details_wrapper.addClass('details-wrapper--fall-right');
        } else {
          $details_wrapper.removeClass('details-wrapper--fall-right');
        }
      };

      $(once('details--drop-processed', all_details)).each(function (detail_key) {
        let detail = all_details[detail_key];
        update_side_select(detail);
        let allow_call = true;
        $(window).on('resize', function () {
          if (allow_call) {
            allow_call = false;
            setTimeout(function () {
              allow_call = true;
              update_side_select(detail);
            }, 100);
          }
        });
      });
      $(once('details--click-processed', all_details)).on('toggle', function () {
        update_side_select($(this));
      });
    }
  };
  Drupal.behaviors.muserNoVertFlexProjects = {
    attach: function (context, settings) {
      if (navigator.userAgent.indexOf("Chrome/") === -1 && navigator.userAgent.indexOf("Safari/") !== -1 && navigator.userAgent.indexOf("Version/10.") !== -1) {
        $(body).addClass('no-vert-flex-projects');
      }
    }
  };
  Drupal.behaviors.muserHoverTooltips = {
    attach: function (context, settings) {
      let tooltips = $(once('multiple-terms--processed', '.multiple-terms', context)).attr('tabindex', "0").on('mouseenter focus', function (e) {
        let $this = $(this);
        $this.addClass('tooltip--active');
      }).on('mouseleave blur', function (e) {
        $(this).removeClass('tooltip--active');
      });
      window.addEventListener('keyup', function (e) {
        e = e || window.event;
        let isEscape = false;

        if ("key" in e) {
          isEscape = e.key === "Escape" || e.key === "Esc";
        } else {
          isEscape = e.keyCode === 27;
        }

        if (isEscape) {
          tooltips.removeClass('tooltip--active');
        }
      }, false);
    }
  };

  Drupal.behaviors.muserRelatedProjects = {
    attach: function (context, settings) {
      if ($(context).hasClass('related-projects__content')) {
        let $related_projects_content = $(once('related-projects-processed', '.related-projects__content'));
        let $related_projects = $related_projects_content.parents('.related-projects');
        const timer_max_length = 10000;
        const timer_interval = 100;
        let timer_current_length = timer_max_length;
        let $autoclose_bar = $related_projects.find('.related-projects__autoclose-timer-bar');
        let $autoclose_timer = $related_projects.find('.related-projects__autoclose-timer');
        let $minimize_button = $related_projects.find('.related-projects__minimize');
        let $projects_markup = $related_projects.find('.view-content');
        let $related_projects_count = $related_projects.find('.related-projects__project-count');
        let $related_projects_title = $related_projects.find('.related-projects__title');
        let $related_projects_title_section = $related_projects.find('.related-projects__title-section');
        let $related_projects_reason = $related_projects.find('.related-projects__reason');
        let projects_count = $projects_markup.children().length;

        if (projects_count === 1) {
          $related_projects_title.removeClass('related-projects__title--is-plural');
        } else {
          $related_projects_title.addClass('related-projects__title--is-plural');
        }

        $related_projects_count.text(projects_count + ' ');
        let autoclose_interval = 0;

        if ($projects_markup.length === 0) {
          $related_projects.removeClass('related-projects--has-projects');
          return;
        }
        $related_projects.addClass('related-projects--has-projects');

        let delayedClose = function () {
          autoclose_interval = setInterval(function () {
            timer_current_length -= timer_interval;
            $autoclose_bar.attr('style', 'max-width: ' + timer_current_length / timer_max_length * 100 + '%;');

            if (timer_current_length < -1000) {
              clearInterval(autoclose_interval);
              $autoclose_timer.removeClass('related-projects__autoclose-timer--timer-active');
              $minimize_button.click();
            }
          }, timer_interval);
        };

        if (Drupal.muserGetCookie('muserRelatedProjectsState') === 'minimized') {
          $related_projects.addClass('related-projects--minimized');
          $related_projects.addClass('related-projects--flash')
          setTimeout(function () {
            $related_projects.removeClass('related-projects--flash')
          }, 3000)
          // flash the item now
        }
        else {
          $related_projects.removeClass('related-projects--minimized');
          $autoclose_bar.attr('style', 'max-width: 100%;');
          $autoclose_timer.addClass('related-projects__autoclose-timer--timer-active');
          delayedClose();
        }

        $minimize_button.on('click', function (e) {
          if ($related_projects.hasClass('related-projects--minimized')) {
            return;
          }
          clearInterval(autoclose_interval);
          $autoclose_timer.removeClass('related-projects__autoclose-timer--timer-active');
          $related_projects.addClass('related-projects--minimized');
          Drupal.muserSetCookie('muserRelatedProjectsState', 'minimized', 365)
          e.stopPropagation();
        });
        $related_projects.hover(function () {
          clearInterval(autoclose_interval);
        }, function () {
          if ($autoclose_timer.hasClass('related-projects__autoclose-timer--timer-active')) {
            delayedClose();
          }
        });
        $related_projects.click(function () {
          $autoclose_timer.removeClass('related-projects__autoclose-timer--timer-active');
        }); // $('')

        $related_projects_title_section.click(function () {
          if ($related_projects.hasClass('related-projects--minimized')) {
            $related_projects.removeClass('related-projects--minimized');
            Drupal.muserSetCookie('muserRelatedProjectsState', 'open', 365)
          }
        })

        if ($projects_markup.length > 0) {
          $projects_markup[0].addEventListener("wheel", function (e) {
            if (e.deltaX === 0 && e.deltaY !== 0) {
              $projects_markup[0].scrollBy((e.wheelDeltaY * -1), 0)
              e.preventDefault();
            }
            clearInterval(autoclose_interval);
            $autoclose_timer.removeClass('related-projects__autoclose-timer--timer-active');
          })
          $related_projects_reason[0].addEventListener("wheel", function (e) {
            e.preventDefault();
          })
          $related_projects_title_section[0].addEventListener("wheel", function (e) {
            if (!$related_projects.hasClass('related-projects--minimized')) {
              e.preventDefault();
            }
          })
        }
      }

      let linkSelector = '#related-projects a.project__favorite, #related-projects a.node--type-project, #drupal-modal .from-related a.project__favorite';
      let resetLinks = false;
      $(once('related-projects-flagging-links--processed', linkSelector, context)).each(function () {
        let href = $(this).attr('href');
        href += (href.includes('?') ? '&' : '?') + 'related=1';
        $(this).attr('href', href);
        resetLinks = true;
      });

      if (resetLinks) {
        Drupal.muserResetAjaxLinks(linkSelector, document.body);
      }
    }
  };

  Drupal.behaviors.muserProjectSearchReset = {
    attach: function (context, settings) {
      $(once('projects-page-reset--processed', '#views-exposed-form-projects-page .button-reset')).each( function () {
        $(this).unbind('click').attr('type', 'reset').on('click', function (e) {
          window.location = $(this).parents('form').attr('action');
        });
      });
    }
  };

  Drupal.muserResetAjaxLinks = function (selector, container) {
    $(selector).unbind('click');
    // Remove the once() so it will get processed again.
    once.remove('ajax', selector);
    // Attach the ajax action to these links.
    Drupal.ajax.bindAjaxLinks(container);
  };

})(jQuery, once);
