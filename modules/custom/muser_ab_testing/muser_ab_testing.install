<?php

/**
 * Implements hook_schema().
 */
function muser_ab_testing_schema() {
  $schema['muser_ab_testing_cohort_membership'] = array(
    'description' => 'User Cohort Membership',
    'fields' => array(
      'uid' => [
        'description' => 'User ID, links to User table.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'test_nid' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
        'description' => 'Node id of the A/B Test, links to the node table.',
      ),
      'cohort' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Cohort Identifier',
      ),
      'create_timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Timestamp',
        'default' => 0,
        'size' => 'big',
      ),
    ),
    'foreign keys' => [
      'uid' => [
        'table' => 'users',
        'columns' => [
          'uid' => 'uid',
        ],
      ],
      'test_nid' => [
        'table' => 'node',
        'columns' => [
          'test_nid' => 'nid',
        ],
      ],
    ],
    'primary key' => ['uid', 'test_nid'],
    'indexes' => [
      'uid' => ['uid'],
      'test_nid' => ['test_nid'],
      'cohort' => ['cohort'],
      'timestamp' => ['create_timestamp'],
    ],
  );
  return $schema;
}

/**
 * Add timestamp field to the Cohort tracking table.
 */
function muser_ab_testing_update_10101(&$sandbox) {
  $table_schema = [
    'fields' => [
      'create_timestamp' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Timestamp',
        'default' => 0,
        'size' => 'big',
      ],
    ],
    'indexes' => [
      'create_timestamp' => ['create_timestamp'],
    ],
  ];

  $connection = \Drupal::service('database');
  $schema = $connection->schema();

  $schema->addField('muser_ab_testing_cohort_membership', 'create_timestamp', $table_schema['fields']['create_timestamp']);
  $schema->addIndex('muser_ab_testing_cohort_membership', 'create_timestamp', ['create_timestamp'], $table_schema);

  // Set all records as created now.
  $connection->update('muser_ab_testing_cohort_membership')
    ->fields(['create_timestamp' => \Drupal::time()->getCurrentTime()])
    ->execute();

  return t('Timestamp field has been added to the Cohort tracking table.');
}
