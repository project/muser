<?php

namespace Drupal\muser_system;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;

/**
 * Provides date information for rounds.
 */
trait RoundDatesTrait {

  use StringTranslationTrait;
  /**
   * Returns date-related info for a round node.
   *
   * @param \Drupal\node\Entity\Node $round
   *   The round node.
   *
   * @return array
   *   Array of date info.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getRoundData(Node $round): array {
    $data = [];
    $dates = $this->getDates($round);
    foreach ($dates as $date) {
      foreach (['start', 'end'] as $type) {
        if (empty($date[$type]['future'])) {
          continue;
        }
        $data[] = [
          'text' => $date[$type]['until_text'],
          'ended_text' => $date[$type]['ended_text'],
          'timestamp' => $date[$type]['until_timestamp'] * 1000,
        ];
      }
    }
    return $data;
  }


  /**
   * Returns date-related info for a round node.
   *
   * @param \Drupal\node\Entity\Node $round
   *   The round node.
   *
   * @return array
   *   Array of date info.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getDates(Node $round): array {

    $fields = [
      'field_post_projects' => [
        'start' => $this->t('First day to post projects'),
        'end' => $this->t('Last day to post projects'),
        'until_start' => $this->t('Project posting begins'),
        'ended_start' => $this->t('Project posting has begun'),
        'until_end' => $this->t('Project posting ends'),
        'ended_end' => $this->t('Project posting has ended'),
      ],
      'field_apply' => [
        'start' => $this->t('First day to apply for projects'),
        'end' => $this->t('Last day to apply for projects'),
        'until_start' => $this->t('Application submission begins'),
        'ended_start' => $this->t('Application submission has begun'),
        'until_end' => $this->t('Application submission ends'),
        'ended_end' => $this->t('Application submission has ended'),
      ],
      'field_accept_applications' => [
        'start' => $this->t('First day to review applications'),
        'end' => $this->t('Last day to review applications'),
        'until_start' => $this->t('Application review begins'),
        'ended_start' => $this->t('Application review has begun'),
        'until_end' => $this->t('Application review ends'),
        'ended_end' => $this->t('Application review has ended'),
      ],
      'field_sign_contracts' => [
        'start' => $this->t('First day to accept agreements'),
        'end' => $this->t('Last day to accept agreements'),
        'until_start' => $this->t('Agreements period begins'),
        'ended_start' => $this->t('Agreements period has begun'),
        'until_end' => $this->t('Agreements period ends'),
        'ended_end' => $this->t('Agreements period has ended'),
      ],
    ];

    $dates = [];
    $user_tz = new \DateTimeZone(date_default_timezone_get());
    $now = new DrupalDateTime();

    foreach ($fields as $field => $text) {
      /** @var \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem $date_field */
      $date_field = $round->get($field)->get(0);
      if (!$date_field || $date_field->isEmpty()) {
        continue;
      }
      $values = $date_field->getValue();
      foreach (['value', 'end_value'] as $type) {
        $date = new DrupalDateTime($values[$type], DateTimeItemInterface::STORAGE_TIMEZONE);
        $date->setTimezone($user_tz);
        $diff = $date->diff($now);
        $key = ($type == 'value') ? 'start' : 'end';
        $dates[$field][$key] = [
          'date' => $date->format('l, F j \a\t g:ia'),
          'future' => $diff->invert,
          'class' => ($diff->invert) ? 'future' : 'past',
          'text' => $text[$key],
          'until_text' => $text['until_' . $key],
          'ended_text' => $text['ended_' . $key],
          'until_timestamp' => $date->getTimestamp(),
        ];
      } // Alternate start/end.
    } // Loop thru different dates.

    return $dates;

  }

}
