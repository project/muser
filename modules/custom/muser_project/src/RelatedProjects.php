<?php

declare(strict_types=1);

namespace Drupal\muser_project;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flag\FlagInterface;

/**
 * @todo Add class description.
 */
final class RelatedProjects {

  use StringTranslationTrait;

  /**
   * Maximum number of projects to return.
   */
  const MAX_PROJECTS = 6;

  /**
   * Constructs a RelatedProjects object.
   */
  public function __construct(
    private readonly Connection $connection,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly RendererInterface $renderer,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly AccountProxyInterface $currentUser,
  ) {}

  /**
   * Return markup for related projects.
   */
  public function getRelatedProjects($project_round_nid, FlagInterface $flag, $account): array {

    $storage = $this->entityTypeManager->getStorage('node');
    if ($project_round = $storage->load($project_round_nid)) {
      $flagging = muser_project_get_flagging($project_round, $flag, $account);
      $flagged_project = $project_round->field_project->entity;
    }

    if (empty($flagging) || empty($flagged_project)) {
      return [];
    }

    $projects = [];
    if ($project_nids = $this->getProjectsByCategory($project_round, $flagged_project, $account)) {
      $projects['reason'] = $this->t('These projects have similar categories to the one you just favorited.');
      $builder = $this->entityTypeManager->getViewBuilder('node');
      $nodes = $storage->loadMultiple($project_nids);
      foreach ($nodes as $project) {
        $build = $builder->view($project, 'teaser');
        $projects['list'][] = [
          '#type' => 'container',
          'project' => $build,
        ];
      }
    }

    return $projects;

  }

  /**
   * Build the basic query to retrieve projects.
   */
  public function getBaseProjectsQuery($project_round, $project, $account): SelectInterface {
    $query = $this->connection->select('muser_round_projects', 'mrp')
      ->condition('mrp.round_nid', $project_round->field_round->target_id)
      ->condition('mrp.project_nid', $project->id(), '<>')
      ->condition('mrp.status', 1);
    $query->join('node__field_num_students', 'nfns', 'nfns.entity_id = mrp.project_nid AND nfns.deleted = 0');
    $query->join('node__field_use_contract', 'nfuc', 'nfuc.entity_id = mrp.project_nid AND nfuc.deleted = 0');
    $query->join('user__field_has_star', 'ufhs', 'ufhs.entity_id = mrp.project_uid AND ufhs.deleted = 0');
    $query->leftJoin('muser_applications_counts', 'mac', 'mac.project_round_nid = mrp.project_round_nid');
    // Make sure the user hasn't already favorited it.
    $query->leftJoin('muser_applications',
      'ma',
      'mrp.project_nid = ma.project_nid AND mrp.round_nid = ma.round_nid AND ma.application_uid = :uid',
      [':uid' => $account->id()]);
    $query->addField('mrp', 'project_nid');
    $query->isNull('ma.fid');
    return $query;
  }

  /**
   * Return array of projects from a query.
   */
  public function buildProjectsArray($query): array {
    $projects = [];
    $results = $query->execute();
    foreach ($results as $row) {
      $projects[] = $row->project_nid;
    }
    return $projects;
  }

  /**
   * Return randomly-selected projects.
   */
  public function getProjectsByRandom($project_round, $project, $account): array {

    $query = $this->getBaseProjectsQuery($project_round, $project, $account);

    $query->addExpression('RAND()', 'random');
    $query->orderBy('random');
    $query->range(0, rand(1, self::MAX_PROJECTS));

    return $this->buildProjectsArray($query);

  }

  /**
   * Return projects related by category.
   */
  public function getProjectsByCategory($project_round, $project, $account): array {

    $query = $this->getBaseProjectsQuery($project_round, $project, $account);

    $categories = [];
    foreach ($project->field_categories as $item) {
      $categories[] = $item->target_id;
    }

    foreach (['matching_cats', 'total_cats'] as $alias) {
      $table_alias = 'nfc_' . $alias;
      $match_count_query[$alias] = $this->connection->select('node__field_categories', $table_alias);
      $match_count_query[$alias]->where($table_alias . '.entity_id = mrp.project_nid AND ' . $table_alias . '.deleted = 0');
      $match_count_query[$alias]->addExpression('COUNT(' . $table_alias . '.field_categories_target_id)');
    }
    $match_count_query['matching_cats']->condition('nfc_matching_cats.field_categories_target_id', $categories, 'IN');
    $query->addExpression('(' . $match_count_query['matching_cats']->__toString() . ')', 'matching_cats');
    $query->addExpression('(' . $match_count_query['total_cats']->__toString() . ')', 'total_cats');

    $query->join('node__field_categories', 'nfc', 'nfc.entity_id = mrp.project_nid AND nfc.deleted = 0');
    $query->condition('nfc.field_categories_target_id', $categories, 'IN');

    $query->addField('nfuc', 'field_use_contract_value', 'has_contract');
    $query->addField('nfns', 'field_num_students_value', 'num_students');
    $query->addField('ufhs', 'field_has_star_value', 'has_star');
    $query->fields('mac', ['favorited', 'submitted']);

    $query->groupBy('mrp.project_nid');
    $query->groupBy('nfuc.field_use_contract_value');
    $query->groupBy('nfns.field_num_students_value');
    $query->groupBy('ufhs.field_has_star_value');
    $query->groupBy('mac.favorited');
    $query->groupBy('mac.submitted');

    // Can't order by the percentage of matched cats due to Drupal query
    // limitation.
    // $query->orderBy('(matching_cats/total_cats)', 'desc');
    $query->orderBy('matching_cats', 'desc');
    $query->orderBy('total_cats', 'asc');
    $query->orderBy('has_contract', 'desc');
    $query->orderBy('has_star', 'desc');
    $query->orderBy('num_students', 'desc');
    $query->orderBy('favorited', 'asc');
    $query->orderBy('submitted', 'asc');

    $query->range(0, self::MAX_PROJECTS);

    return $this->buildProjectsArray($query);

  }

}
