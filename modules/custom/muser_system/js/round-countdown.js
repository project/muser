/**
 * @file
 * View filter functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserRoundCountdown = {
    attach: function (context, settings) {

      let timeOut = 1000;
      let offset = 0;

      let toDuration = function (seconds) {

        timeOut = 1000 * 60;
        seconds = Number(seconds);
        let totalHours = seconds / 3600;
        let w = Math.floor(seconds / (3600 * 24) / 7);
        let d = Math.floor(seconds % (3600 * 24 * 7) / (3600 * 24));
        let h = Math.floor(seconds % (3600 * 24) / 3600);
        let m = Math.floor(seconds % 3600 / 60);
        let s = Math.floor(seconds % 60);
        let parts = [];
        let wDisplay = w > 0 ? w + (w === 1 ? ' week' : ' weeks') : '';
        let dDisplay = d > 0 ? d + (d === 1 ? ' day' : ' days') : '';
        let hDisplay = h > 0 ? h + (h === 1 ? ' hour' : ' hours') : '';
        let mDisplay = m > 0 ? m + (m === 1 ? ' minute' : ' minutes') : '';
        let sDisplay = s > 0 ? s + (s === 1 ? ' second' : ' seconds') : '';

        if (w > 0) {
          if (w === 1 && d === 0) {
            parts.push('7 days');
            parts.push(hDisplay);
          }
          else {
            parts.push(wDisplay);
            parts.push(dDisplay);
          }
        }
        else if (d > 0) {
          parts.push(dDisplay);
          parts.push(hDisplay);
        }
        else {
          // Less than a day.
          if (h === 0 && m === 0) {
            if (s === 0) {
              timeOut = 1000 * 20;
              return '';
            }
            else {
              timeOut = 1000;
              return Drupal.t('in less than a minute');
            }
          }
          timeOut = (totalHours > 1) ? 1000 * 10 : 1000;
          parts.push(hDisplay);
          parts.push(mDisplay);
        }

        return Drupal.t('in @time', {'@time': parts.filter(n => n).join(', ')});
      }

      let showDuration = function (seconds) {
        let inRound = false;
        const now = new Date();
        const timestamp = now.getTime() + offset;
        for (let i = 0; i < muserRoundData.length; i++) {
          if (timestamp <= muserRoundData[i]['timestamp']) {
            inRound = true;
            let numSeconds = (muserRoundData[i]['timestamp'] - timestamp) / 1000;
            let duration = toDuration(numSeconds);
            if (duration) {
              $('.round-countdown__label').text(muserRoundData[i]['text']);
              $('.round-countdown__time').text(duration);
            }
            else {
              $('.round-countdown__label').text(muserRoundData[i]['ended_text']);
              $('.round-countdown__time').text('');
            }
            $('.block-muser-round-countdown-block').fadeIn();
            setTimeout(showDuration, timeOut);
            break;
          }
        } // Loop thru times.
        if (!inRound) {
          // Not in round, hide the whole thing.
          $('.block-muser-round-countdown-block').fadeOut();
        }
      }
      let updateTimeFromServer = function() {
        let cb = new Date().getTime();
        $.get("/api/current-round-data?cb=" + cb, {}, (data) => {
          muserRoundData = data['dates']
          const local_timestamp = new Date().getTime();
          offset = data['server_timestamp'] - local_timestamp;
        })
      }


      $(once('round-countdown--processed', '.block-muser-round-countdown-block')).each( function () {
        setTimeout(showDuration, timeOut);
        $('.round-countdown__action--show-disclaimer').on('click', function () {
          $('.round-countdown__disclaimer').show();
          $('.round-countdown__text').hide();
          Drupal.muserSetCookie('muserRoundCountdownState', 'default', 365)
        });
        $('.round-countdown__action--collapse').on('click', function () {
          if ($('.round-countdown__disclaimer').is(":hidden")) {
            $('.round-countdown__action--expand').show();
            $('.round-countdown__text').hide()
            $('.round-countdown__action--collapse').hide()
            Drupal.muserSetCookie('muserRoundCountdownState', 'collapsed', 365)
          }
          else {
            $('.round-countdown__disclaimer').hide();
            $('.round-countdown__text').show()
            Drupal.muserSetCookie('muserRoundCountdownState', 'default', 365)
          }
        });
        $('.round-countdown__action--expand').on('click', function() {
          $('.round-countdown__action--expand').hide();
          $('.round-countdown__text').show()
          $('.round-countdown__action--collapse').show()
          Drupal.muserSetCookie('muserRoundCountdownState', 'default', 365)
        })
        setTimeout(showDuration, timeOut);

        const stateOnLoad = Drupal.muserGetCookie('muserRoundCountdownState');
        // if (stateOnLoad === 'disclaimer') {
        //   $('.round-countdown__disclaimer').show();
        //   $('.round-countdown__text').hide();
        // }
        if (stateOnLoad === 'collapsed') {
          $('.round-countdown__action--expand').show();
          $('.round-countdown__text').hide()
          $('.round-countdown__action--collapse').hide()
        }

        updateTimeFromServer();
        setInterval(updateTimeFromServer, 5 * 60 * 1000)
      });


    }
  };
})(jQuery, Drupal, once);
