<?php

use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Pre-populate muser_user_actions table.
 */
function muser_user_post_update_prepopulate_muser_user_actions(&$sandbox) {

  $db = Database::getConnection();

  // Users created.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select ufd.uid, 'user', 'user', 'created', '', ufd.created
  from users_field_data ufd
  where ufd.login <> 0";
  $db->query($sql);

  // User logins (when a user logs in, the field is updated).
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select ufd.uid, 'user', 'user', 'login', '', ufd.login
  from users_field_data ufd
  where ufd.login <> 0 ";
  $db->query($sql);

  // Nodes created.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select nfd.uid, 'node', nfd.`type`, 'created', '', nfd.created
  from node_field_data nfd
  where nfd.uid <> 0 ";
  $db->query($sql);

  // Nodes updated.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select nfd.uid, 'node', nfd.`type`, 'updated', '', nfd.changed
  from node_field_data nfd
  where nfd.uid <> 0 ";
  $db->query($sql);

  // Flagging created.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select f.uid, 'flagging', 'favorites', 'created: pending', '', f.created
    from flagging f
    where f.uid <> 0 ";
  $db->query($sql);

  // Flagging reviewed.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select project_uid, 'flagging', 'favorites', 'updated: reviewed', '', review_date
    from muser_applications ma
    where project_uid <> 0
    and review_date IS NOT NULL ";
  $db->query($sql);

  // Flagging student contract updated.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select application_uid, 'flagging', 'favorites', 'updated: accepted', '', contract_date_student
    from muser_applications ma
    where project_uid <> 0
    and contract_date_student IS NOT NULL ";
  $db->query($sql);

  // Flagging mentor contract updated.
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
  select project_uid, 'flagging', 'favorites', 'updated: accepted', '', contract_date_mentor
    from muser_applications ma
    where project_uid <> 0
    and contract_date_mentor IS NOT NULL ";
  $db->query($sql);

  // Set the action_date for all rows.
  $now = new DrupalDateTime();
  $tz_offset = $now->getOffset();
  $sql = "UPDATE {muser_user_actions}
    SET action_date = FROM_UNIXTIME((timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d')
    WHERE action_date = '' ";
  $db->query($sql, [':offset' => $tz_offset]);

}

/**
 * Add logins to muser_user_actions table.
 */
function muser_user_post_update_add_logins_to_user_actions(&$sandbox) {
  $db = Database::getConnection();
  $sql = "insert into {muser_user_actions}
    (uid, entity_type, bundle, action, action_date, timestamp)
    SELECT uid, 'user', 'user', 'login', FROM_UNIXTIME(login, '%Y-%m-%d'), login
    FROM login_history";
  $db->query($sql);
}
