<?php

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;

function _muser_base_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['logo_header_style'] = [
    '#type' => 'select',
    '#options' => [
      'normal_header_normal_logo' => 'Normal Header, Normal Logo (max 48x150px)',
      'normal_header_large_logo' => 'Normal Header, Large Logo (max 64x200px)',
      'tall_header_large_logo' => 'Tall Header, Large Logo (max 100x200px)',
    ],
    '#title' => t('Logo/header style'),
    '#default_value' => theme_get_setting('logo_header_style'),
  ];

  $colorset_options = [];
  $colorsets = muser_base_get_colorsets();
  foreach ($colorsets as $key => $colorset) {
    if (!empty($colorset['name'])) {
      $colorset_options[$key] = $colorset['name'];
    }
  }
  $colorset_options['custom'] = t('Custom');
  $form['colorset'] = [
    '#type' => 'select',
    '#attributes' => ['data-colorset-options' => json_encode($colorsets)],
    '#options' => $colorset_options,
    '#title' => t('Site Accent Color Set'),
    '#default_value' => theme_get_setting('colorset'),
    '#description' => t("The color set for the site."),
  ];

  $condition = ['select[name="colorset"]' => ['value' => 'custom']];
  $form['custom_primary_color'] = [
    '#type' => 'textfield',
    '#title' => t('Primary Accent Color'),
    '#attributes' => [
      'class' => ['color-field'],
      'data-color' => 'PRIMARY_COLOR',
    ],
    '#default_value' => theme_get_setting('custom_primary_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];
  $form['custom_secondary_color'] = [
    '#type' => 'textfield',
    '#title' => t('Secondary Accent Color'),
    '#attributes' => [
      'class' => ['color-field'],
      'data-color' => 'SECONDARY_COLOR',
    ],
    '#default_value' => theme_get_setting('custom_secondary_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_background_color'] = [
    '#type' => 'textfield',
    '#title' => t('Site Background Color'),
    '#attributes' => [
      'class' => ['color-field'],
      'data-color' => 'BACKGROUND_COLOR',
    ],
    '#default_value' => theme_get_setting('custom_background_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_title_color'] = [
    '#type' => 'textfield',
    '#title' => t('Title Color'),
    '#attributes' => [
      'class' => ['color-field'],
      'data-color' => 'TITLE_COLOR',
    ],
    '#default_value' => theme_get_setting('custom_title_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_text_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text Color'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_text_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_status_message_color'] = [
    '#type' => 'textfield',
    '#title' => t('Status Message Color'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_status_message_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_error_message_color'] = [
    '#type' => 'textfield',
    '#title' => t('Error Message Color'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_error_message_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_warning_message_color'] = [
    '#type' => 'textfield',
    '#title' => t('Warning Message Color'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_warning_message_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_message_status_text_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text color for status messages.'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_message_status_text_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_message_warning_text_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text color for warning messages.'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_message_warning_text_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_message_error_text_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text color for error messages.'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_message_error_text_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_text_over_primary_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text Color over Primary Color. Set dark over light colors and light over dark colors.'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_text_over_primary_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_text_over_secondary_color'] = [
    '#type' => 'textfield',
    '#title' => t('Text Color over Secondary Color. Set dark over light colors and light over dark colors.'),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_text_over_secondary_color'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['custom_primary_text_over_white'] = [
    '#type' => 'textfield',
    '#title' => t("Primary Color Text over white. If your default primary color doesn't have sufficient contrast over white, set a darker color."),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_primary_text_over_white'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];
  $form['custom_secondary_text_over_white'] = [
    '#type' => 'textfield',
    '#title' => t("Secondary Color Text over white. If your default secondary color doesn't have sufficient contrast over white, set a darker color."),
    '#attributes' => ['class' => ['color-field'], 'data-color' => 'TEXT_COLOR'],
    '#default_value' => theme_get_setting('custom_secondary_text_over_white'),
    '#states' => [
      'visible' => $condition,
      'required' => $condition,
    ],
  ];

  $form['#submit'][] = 'muser_base_handle_form_submit';
  $form['#attached']['library'][] = 'muser_base/theme-settings-form';
}

function muser_base_get_colorsets() {
  $colorsets = [
    'colorset_devil' => [
      'name' => t('Devil (Dark blue)'),
      'PRIMARY_COLOR' => '#001A57',
      'SECONDARY_COLOR' => '#339898',
      'BACKGROUND_COLOR' => '#F3F2F1',
      'TITLE_COLOR' => '#001A57',
      'TEXT_COLOR' => '#262626',
      'MESSAGE_ERROR_BG' => '#C84E00',
      'MESSAGE_WARNING_BG' => '#E89923',
      'MESSAGE_STATUS_BG' => '#A1B70D',
      'MESSAGE_ERROR_TEXT' => '#FFFFFF',
      'MESSAGE_WARNING_TEXT' => '#FFFFFF',
      'MESSAGE_STATUS_TEXT' => '#FFFFFF',
      'TEXT_OVER_PRIMARY' => '#FFFFFF',
      'TEXT_OVER_SECONDARY' => '#FFFFFF',
      'PRIMARY_TEXT_OVER_WHITE' => 'var(--primary)',
      'SECONDARY_TEXT_OVER_WHITE' => 'var(--secondary)',
    ],
    'colorset_wolfy' => [
      'name' => t('Wolfy (Red)'),
      'PRIMARY_COLOR' => '#CC0000',
      'SECONDARY_COLOR' => '#427E93',
      'BACKGROUND_COLOR' => '#F3F2F1',
      'TITLE_COLOR' => '#001A57',
      'TEXT_COLOR' => '#262626',
      'MESSAGE_ERROR_BG' => '#D14905',
      'MESSAGE_WARNING_BG' => '#FDD726',
      'MESSAGE_STATUS_BG' => '#6F7D1C',
      'MESSAGE_ERROR_TEXT' => '#FFFFFF',
      'MESSAGE_WARNING_TEXT' => '#262626',
      'MESSAGE_STATUS_TEXT' => '#262626',
      'TEXT_OVER_PRIMARY' => '#FFFFFF',
      'TEXT_OVER_SECONDARY' => '#FFFFFF',
      'PRIMARY_TEXT_OVER_WHITE' => 'var(--primary)',
      'SECONDARY_TEXT_OVER_WHITE' => 'var(--secondary)',
    ],
    'colorset_ram' => [
      'name' => t('Ram (Light blue)'),
      'PRIMARY_COLOR' => '#4B9CD3',
      'SECONDARY_COLOR' => '#2A696A',
      'BACKGROUND_COLOR' => '#F3F2F1',
      'TITLE_COLOR' => '#001A57',
      'TEXT_COLOR' => '#262626',
      'MESSAGE_ERROR_BG' => '#DADDC0',
      'MESSAGE_WARNING_BG' => '#E89923',
      'MESSAGE_STATUS_BG' => '#A1B70D',
      'MESSAGE_ERROR_TEXT' => '#FFFFFF',
      'MESSAGE_WARNING_TEXT' => '#262626',
      'MESSAGE_STATUS_TEXT' => '#262626',
      'TEXT_OVER_PRIMARY' => '#FFFFFF',
      'TEXT_OVER_SECONDARY' => '#FFFFFF',
      'PRIMARY_TEXT_OVER_WHITE' => 'var(--primary)',
      'SECONDARY_TEXT_OVER_WHITE' => 'var(--secondary)',
    ],
  ];
  // Allow modules to alter/add colorsets.
  \Drupal::moduleHandler()->alter('muser_colorsets', $colorsets);

  // Allow overrides in settings.local.php.
  if ($custom_colorsets = \Drupal::service('settings')::get('custom_colorsets')) {
    $colorsets += $custom_colorsets;
  }

  // Allow overrides in a YAML file.
  $yaml_colorset_file = 'public://colorsets.yml';
  if (file_exists($yaml_colorset_file) && $yaml_colorsets = file_get_contents($yaml_colorset_file)) {
    try {
      $yaml_colorsets = Yaml::decode($yaml_colorsets);
    }
    catch (\Exception $exception) {
      \Drupal::messenger()->addError(
        t('Custom colorset YAML file could not be parsed correctly.<br>Error:<br>@error', [
          '@error' => $exception->getMessage(),
        ]));
      $yaml_colorsets = [];
    }
    if ($yaml_colorsets) {
      $colorsets += $yaml_colorsets;
    }
  }

  return $colorsets;

}

function muser_base_get_accent_colors($colorset = NULL, $form_values = NULL) {
  $colorset = $colorset ?: theme_get_setting('colorset', 'muser_base');

  if (strpos($colorset, 'colorset_') !== FALSE) {
    return muser_base_get_colorsets()[$colorset];
  }
  $colorset = [
    'PRIMARY_COLOR' => $form_values['custom_primary_color']['#value'] ?? theme_get_setting('custom_primary_color', 'muser_base'),
    'SECONDARY_COLOR' => $form_values['custom_secondary_color']['#value'] ?? theme_get_setting('custom_secondary_color', 'muser_base'),
    'BACKGROUND_COLOR' => $form_values['custom_background_color']['#value'] ?? theme_get_setting('custom_background_color', 'muser_base'),
    'TITLE_COLOR' => $form_values['custom_title_color']['#value'] ?? theme_get_setting('custom_title_color', 'muser_base'),
    'TEXT_COLOR' => $form_values['custom_text_color']['#value'] ?? theme_get_setting('custom_text_color', 'muser_base'),
    'MESSAGE_ERROR_BG' => $form_values['custom_error_message_color']['#value'] ?? theme_get_setting('custom_error_message_color', 'muser_base'),
    'MESSAGE_WARNING_BG' => $form_values['custom_warning_message_color']['#value'] ?? theme_get_setting('custom_warning_message_color', 'muser_base'),
    'MESSAGE_STATUS_BG' => $form_values['custom_status_message_color']['#value'] ?? theme_get_setting('custom_status_message_color', 'muser_base'),
    'MESSAGE_ERROR_TEXT' => $form_values['custom_message_error_text_color']['#value'] ?? theme_get_setting('custom_message_error_text_color', 'muser_base'),
    'MESSAGE_WARNING_TEXT' => $form_values['custom_message_warning_text_color']['#value'] ?? theme_get_setting('custom_message_warning_text_color', 'muser_base'),
    'MESSAGE_STATUS_TEXT' => $form_values['custom_message_status_text_color']['#value'] ?? theme_get_setting('custom_message_status_text_color', 'muser_base'),
    'TEXT_OVER_PRIMARY' => $form_values['custom_text_over_primary_color']['#value'] ?? theme_get_setting('custom_text_over_primary_color', 'muser_base'),
    'TEXT_OVER_SECONDARY' => $form_values['custom_text_over_secondary_color']['#value'] ?? theme_get_setting('custom_text_over_secondary_color', 'muser_base'),
  ];

  return $colorset;

}

function muser_base_create_accent_css($values = NULL) {
  if ($values) {
    $colorset = muser_base_get_accent_colors($values['colorset']['#value'], $values);
  }
  else {
    $colorset = muser_base_get_accent_colors();
  }

  /** @var \Drupal\Core\State\State $state */
  $state = \Drupal::service('state');
  $state->set('muser_theme_colorset', $colorset);

  $theme_folder = \Drupal::service('extension.path.resolver')
    ->getPath('theme', 'muser_base');

  $css = file_get_contents($theme_folder . '/css/variables.css');
  unset($colorset['name']);
  $css = strtr($css, $colorset);
  $site_css_dir = 'public://site-css/';

  $file_system = \Drupal::service('file_system');
  $file_system->prepareDirectory($site_css_dir, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY);
  $file_system->saveData($css, $site_css_dir . 'site-variables.css', TRUE);
}

//:root {
//  --primary: PRIMARY_COLOR;
//  --secondary: SECONDARY_COLOR;
//  --background-color: BACKGROUND_COLOR;
//
//  --title-color: TITLE_COLOR;
//  --text-color: TEXT_COLOR;
//}

function muser_base_handle_form_submit($form, FormStateInterface $form_state) {
  muser_base_create_accent_css($form);
  drupal_flush_all_caches();
}
