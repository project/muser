/**
 * @file
 * User form functionality.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.muserUserForm = {
    attach: function (context, settings) {
      $(once('user-form--processed', '.user-form', context)).each(function () {

        $('.form-item-current-pass').hide();
        $('.form-item-pass').hide();

        $('#edit-name, #edit-mail').on('paste', function () {
          $('.form-item-current-pass').show();
        });
        $('#edit-name, #edit-mail').keyup(function () {
          $('.form-item-current-pass').show();
        });
        $('.change-pass').on('click', function () {
          $('.form-item-current-pass').show().find('input').focus();
          $('.form-item-pass').show();
          $(this).hide();
          return false;
        });

      });
    }
  };
})(jQuery, Drupal, once);
