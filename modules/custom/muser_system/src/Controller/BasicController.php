<?php

namespace Drupal\muser_system\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\flag\Entity\Flagging;
use Drupal\muser_system\RoundDatesTrait;
use Drupal\muser_system\ScheduledEmails;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for basic Muser pages.
 */
class BasicController extends ControllerBase {

  use RoundDatesTrait;

  const SECONDS_PER_DAY = 24 * 60 * 60;

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * BasicController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Show essay guidelines.
   *
   * @return array
   *   Build array.
   */
  public function essayGuidelines() {

    $build = [
      '#cache' => [
        'tags' => ['config:muser_system.settings'],
      ],
    ];

    $config = $this->configFactory->get('muser_system.settings');

    if (!$message = $config->get('application_essay_guidelines')) {
      return $build;
    }

    $markup = check_markup($message['value'], $message['format']);

    /** @var \Drupal\Core\Utility\Token $token_service */
    $token_service = \Drupal::token();
    $markup = $token_service->replace($markup);

    $build[] = [
      '#type' => 'markup',
      '#markup' => '<div class="essay-guidelines__text">'
      . $markup
      . '</div>',
      '#cache' => [
        'tags' => ['config:muser_system.settings'],
      ],
    ];

    return $build;

  }

  /**
   * @param string $config_key
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function configItemWithTokens(string $config_key) {

    $value = $this->requestStack->getCurrentRequest()->request->get('value');
    if (!$format = $this->requestStack->getCurrentRequest()->request->get('format')) {
      $format = 'raw';
    }

    switch ($config_key) {
      case 'mentor_request_email_body':
      case 'mentor_grant_email_body':
      case 'new_project_email_body':
      case 'post_projects_start_email_body':
      case 'post_projects_end_email_body':
      case 'review_applications_start_email_body':
      case 'review_applications_end_email_body':
      case 'student_apply_start_email_body':
      case 'student_apply_end_email_body':
      case 'after_round_email_body':
      case 'student_accepted_email_body':
      case 'student_rejected_email_body':
      case 'contract_reminder_mentor_start_email_body':
      case 'contract_reminder_mentor_end_email_body':
      case 'contract_reminder_student_start_email_body':
      case 'contract_reminder_student_end_email_body':
        // Valid configuration setting to preview-- do nothing.
        break;
      default:
        return new Response($this->t('Invalid configuration setting.'));
    }

    // Load sample data.
    $node = $this->getSampleProject();
    $account = $node->getOwner();
    $muser_data['project'] = $node;
    $muser_data['round'] = muser_project_get_current_round(TRUE);
    $muser_data['flagging'] = $this->getSampleFlagging();

    /** @var \Drupal\Core\Utility\Token $token_service */
    $token_service = \Drupal::token();

    $text = $token_service->replace($value, [
      'user' => $account,
      'node' => $node,
      'muser' => $muser_data,
    ]);

    if ($format == 'raw') {
      $text = nl2br(htmlentities($text, ENT_QUOTES|ENT_SUBSTITUTE, NULL, FALSE));
    }
    else {
      $text = check_markup($text, $format);
    }

    return new Response($text);

  }

  private function getSampleProject() {
    $nids = \Drupal::entityQuery('node')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'project')
      ->condition('uid', 1, '<>')
      ->sort('created', 'DESC')
      ->accessCheck(FALSE)
      ->execute();
    $nid = ($nids) ? reset($nids) : NULL;
    if ($nid) {
      return Node::load($nid);
    }
    return NULL;
  }

  private function getSampleFlagging() {
    $query = \Drupal::database()->select('muser_applications', 'ma');
    $query->addField('ma', 'fid');
    $query->condition('ma.is_submitted', 1);
    $query->orderBy('ma.fid', 'DESC');
    if ($fid = $query->execute()->fetchField()) {
      return Flagging::load($fid);
    }
    return NULL;
  }

  /**
   * Show reports landing page.
   *
   * @return array
   *   Build array.
   */
  public function reports() {

    $reports = [
      [
        'title' => $this->t('Applications'),
        'view' => 'administer_applications',
        'display' => 'page_1',
        'export_display' => 'export',
      ],
      [
        'title' => $this->t('Application counts'),
        'view' => 'application_counts',
        'display' => 'page_1',
        'export_display' => 'export',
      ],
      [
        'title' => $this->t('A/B Test User Cohorts'),
        'view' => 'ab_test_user_cohorts',
        'display' => 'page_1',
        'export_display' => 'data_export_user_cohorts',
      ],
    ];

    $report_links = [];
    foreach ($reports as $report) {
      $report_links[] = [
        'link' => Link::createFromRoute($report['title'], 'view.' . $report['view'] . '.' . $report['display'])->toString(),
        'export' => Link::createFromRoute($this->t('Download CSV'), 'view.' . $report['view'] . '.' . $report['export_display'])->toString(),
      ];
    } // Loop thru reports.

    $round_links = [];

    $view = Views::getView('rounds');
    $view->setDisplay('page_1');
    $view->setCurrentPage(0);
    $view->render();
    if ($view->result) {
      foreach ($view->result as $row) {
        $round = [
          'name' => $row->_entity->label(),
          'links' => [],
        ];
        if ($row->_entity->field_is_current->value) {
          $round['name'] .= ' (' . $this->t('Current') . ')';
        }

        $options = [
          'round' => $row->_entity->id(),
        ];
        foreach ($reports as $report) {
          $round['links'][] = [
            'link' => Link::createFromRoute($report['title'], 'view.' . $report['view'] . '.' . $report['display'], $options)->toString(),
            'export' => Link::createFromRoute($this->t('CSV'), 'view.' . $report['view'] . '.' . $report['export_display'], $options)->toString(),
          ];
        } // Loop thru reports.

        $round_links[] = $round;

      } // Loop thru rows.
    } // Got rounds>

    return [
      '#theme' => 'muser_reports',
      '#report_links' => $report_links,
      '#round_links' => $round_links,
    ];

  }

  public function muserLogin() {

    $config = $this->configFactory->get('muser_system.settings');
    if ($config->get('user_login_method') === 'drupal'
      || !$config->get('disable_login_page')
      || !$config->get('enable_muser_login_page')
    ) {
      // Show 404 if not configured to use this custom login page.
      throw new NotFoundHttpException();
    }

    $form = \Drupal::formBuilder()->getForm('Drupal\user\Form\UserLoginForm');
    return [
      'user_login_form' => $form,
    ];

  }

  public function currentRoundData() {
    if ($round_nid = muser_project_get_current_round()) {
      $round = Node::load($round_nid);
    }

    $data = [];
    $now = new DrupalDateTime();
    $data['server_timestamp'] = $now->getTimestamp() * 1000;
    $data['dates'] = [];
    if (!empty($round)) {
      $data['dates'] = $this->getRoundData($round);
    }
    return new JsonResponse($data);
  }

  /**
   * Get data for Google chart.
   *
   * @param string $chart_id
   *   The ID of the chart data to return.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON of chart data for Google charts.
   */
  public function chartsData($chart_id) {
    $query = $this->requestStack->getCurrentRequest()->query->all();
    /** @var \Drupal\muser_system\Charts $charts */
    $charts = \Drupal::service('muser_system.charts');
    $definition = $charts->getChartDefinition($chart_id, $query);
    // @todo Deal with caching.
    return new JsonResponse($definition);
  }

  /**
   * Get data for dashboard.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON of round data for dashboard.
   */
  public function roundData() {

    $current_round_nid = muser_project_get_current_round();
    if (!$round_nid = \Drupal::request()->get('round')) {
      $round_nid = $current_round_nid;
    }

    /** @var \Drupal\muser_system\Charts $charts */
    $charts = \Drupal::service('muser_system.charts');

    if (!$round_nid || !$dates = $charts->getRoundDates($round_nid)) {
      return new JsonResponse([]);
    }

    $select = \Drupal::database()->select('muser_applications_aggregation', 'maa');
    $select->addExpression('COUNT(favorited)', 'num_favorited');
    $select->addExpression('COUNT(submitted)', 'num_submitted');
    $select->addExpression('COUNT(was_reviewed)', 'num_reviewed');
    $select->addExpression('COUNT(accepted) + COUNT(declined)', 'num_decided');
    $select->condition('maa.round_nid', $round_nid);
    $application_counts = $select->execute()->fetchAssoc();

    $select = \Drupal::database()->select('muser_round_projects', 'mrp');
    $select->addExpression('COUNT(project_nid)', 'num_projects');
    $select->condition('mrp.round_nid', $round_nid);
    $project_count = $select->execute()->fetchField();

    $now = new DrupalDateTime();
    $tz_offset = $now->getOffset();

    $users = [
      'mentors' => 0,
      'students' => 0,
    ];
    $select = \Drupal::database()->select('muser_user_actions_aggregated', 'mua');
    $select->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') >= :start", [
      ':start' => $dates['start'],
      ':offset' => $tz_offset,
    ]);
    $select->where("FROM_UNIXTIME((mua.action_timestamp + :offset - TIMESTAMPDIFF(SECOND, UTC_TIMESTAMP(), NOW())), '%Y-%m-%d') <= :end", [
      ':end' => $dates['end'],
      ':offset' => $tz_offset,
    ]);
    $select->addExpression('COUNT(DISTINCT mua.uid)', 'num_users');
    $select->addField('mua', 'is_student');
    $select->addField('mua', 'is_mentor');
    $select->groupBy('is_student');
    $select->groupBy('is_mentor');
    foreach ($select->execute() as $row) {
      if ($row->is_mentor) {
        $users['mentors'] += $row->num_users;
      }
      elseif ($row->is_student) {
        $users['students'] += $row->num_users;
      }
    }

    $timeout = 0;
    if ($round_nid == $current_round_nid) {
      $timeout = 60;
      $current_period = muser_project_get_current_period($round_nid);
      if (!$current_period || $current_period == 'before' || $current_period == 'after') {
        $timeout = 300;
      }
    }

    $data = [
      'status' => 1,
      'timeout' => $timeout,
      'stats' => [
        'projects' => $project_count ?? 0,
        'active_users__students' => $users['students'] ?? 0,
        'active_users__mentors' => $users['mentors'] ?? 0,
        'applications__favorited' => $application_counts['num_favorited'] ?? 0,
        'applications__submitted' => $application_counts['num_submitted'] ?? 0,
        'applications__reviewed' => $application_counts['num_reviewed'] ?? 0,
        'applications__decided' => $application_counts['num_decided'] ?? 0,
      ],
    ];

    // @todo Deal with caching.
    return new JsonResponse($data);

  }

  /**
   * Returns dashboard content.
   *
   * @return array
   *   Build array.
   */
  public function adminDashboard() {

    if (!$current_round = muser_project_get_current_round(TRUE)) {
      if (\Drupal::currentUser()->hasPermission('create round content')) {
        $manage_url = Url::fromRoute('view.rounds.page_1')->toString();
        $create_url = Url::fromRoute('node.add', ['node_type' => 'round'])->toString();
        $build['current_round'] = [
          '#theme' => 'status_messages',
          '#message_list' => [
            'warning' => [
              $this->t('There is no <em>current Round</em> set. You may <a href="@manage_url">manage Rounds</a> or <a href="@create_url">create a new Round</a>.', [
                '@manage_url' => $manage_url,
                '@create_url' => $create_url,
              ]),
            ],
          ],
        ];
      }
      $build['current_round']['#cache'] = [
        'tags' => [
          'current_round',
          'user.roles',
        ],
      ];
      return $build;
    }

    $scheduled_emails = $this->getScheduledEmailsInfo($current_round);

    $build = [];

    $build['current_round'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'dashboard-section dashboard-section__current-round',
      ],
    ];
    $build['current_round']['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Current Round: <a href="@url">@label</a>', [
        '@url' => Url::fromRoute('entity.node.edit_form', ['node' => $current_round->id()])->toString(),
        '@label' => $current_round->label(),
      ]),
    ];

    $current_period = muser_project_get_current_period($current_round->id());
    $current_period_text = '';
    switch ($current_period) {

      case 'before':
        $date_field = $current_round->get('field_post_projects')->get(0);
        if ($date_text = $this->getDatesText($date_field, TRUE)) {
          $current_period_text = $this->t('Project posting begins @date.', [
            '@date' => $date_text,
          ]);
        }
        break;

      case 'posting':
        $date_field = $current_round->get('field_post_projects')->get(0);
        if ($date_text = $this->getDatesText($date_field)) {
          $current_period_text = $this->t('Project posting period: @dates', [
            '@dates' => $date_text,
          ]);
        }
        break;

      case 'application':
        $date_field = $current_round->get('field_apply')->get(0);
        if ($date_text = $this->getDatesText($date_field)) {
          $current_period_text = $this->t('Student application period: @dates', [
            '@dates' => $date_text,
          ]);
        }
        break;

      case 'acceptance':
        $date_field = $current_round->get('field_accept_applications')->get(0);
        if ($date_text = $this->getDatesText($date_field)) {
          $current_period_text = $this->t('Application acceptance period: @dates', [
            '@dates' => $date_text,
          ]);
        }
        break;

      case 'contract':
        $date_field = $current_round->get('field_sign_contracts')->get(0);
        if ($date_text = $this->getDatesText($date_field)) {
          $current_period_text = $this->t('Agreements period: @dates', [
            '@dates' => $date_text,
          ]);
        }
        break;

      case 'after':
        $current_period_text = $this->t('The round has ended.');
        break;

    }

    if ($current_period_text) {
      $build['current_round']['info'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => 'dashboard-section__current-round-info',
        ],
        '#value' => $current_period_text,
      ];
    }

    if ($scheduled_emails['upcoming_emails']) {
      $build['current_round']['upcoming_emails'] = [
        '#type' => 'inline_template',
        '#attributes' => [
          'class' => 'dashboard-section__current-round-upcoming_emails',
        ],
        '#template' => '<div class="dashboard-section__current-round-upcoming_emails">'
          . '<strong>{{ label }}:</strong> '
          . implode(', ', $scheduled_emails['upcoming_emails'])
          . '</div>',
        '#context' => [
          'label' => $this->formatPlural(count($scheduled_emails['upcoming_emails']),
            'Upcoming email',
            'Upcoming emails'),
        ],
      ];
    }

    $summary_text = [];
    if ($scheduled_emails['counts']['error']) {
      $summary_text[] = $this->formatPlural($scheduled_emails['counts']['error'],
        '1 error',
        '@count errors');
    }
    if ($scheduled_emails['counts']['sent']) {
      $summary_text[] = $this->t('@count sent', ['@count' => $scheduled_emails['counts']['sent']]);
    }
    if ($scheduled_emails['counts']['future']) {
      $summary_text[] = $this->t('@count to be sent', ['@count' => $scheduled_emails['counts']['future']]);
    }

    $build['current_round']['email_toggle'] = [
      '#type' => 'inline_template',
      '#template' => '<button class="scheduled-emails-toggle" style="display:none">'
        . $this->t('Hide all scheduled emails')
        . '</button><button class="scheduled-emails-toggle">'
        . $this->t('Show all scheduled emails') . '</button> {{ summary }}',
      '#context' => [
        'summary' => implode(', ', $summary_text),
      ],
    ];
    $url = Url::fromRoute('muser_system.email_settings')->toString();
    $build['current_round']['emails'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Scheduled emails <a href="@url">Update email settings</a>', ['@url' => $url]),
      '#items' => $scheduled_emails['items'],
      '#wrapper_attributes' => [
        'class' => [
          'dashboard-section__current-round-scheduled_emails',
          'scheduled-emails-toggle',
        ],
        'style' => 'display:none',
      ],
    ];

    $view = Views::getView('mentor_requests');
    $view->setDisplay('block_1');
    // Set the current page so it doesn't get it from the URL.
    $view->setCurrentPage(0);
    $mentor_requests_view = $view->render();
    if ($view->result) {
      // Only show block if there are results.
      $build['mentor_requests'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'dashboard-section dashboard-section__mentor-requests',
        ],
      ];
      $build['mentor_requests']['header'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Mentors Awaiting Approval'),
      ];
      $build['mentor_requests']['view'] = $mentor_requests_view;
    }

    $view = Views::getView('ab_tests');
    $view->setDisplay('block_1');
    // Set the current page so it doesn't get it from the URL.
    $view->setCurrentPage(0);
    $ab_tests_view = $view->render();
    if ($view->result) {
      // Only show block if there are results.
      $build['ab_tests'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'dashboard-section dashboard-section__ab-tests',
        ],
      ];
      $build['ab_tests']['header'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Active A/B Tests'),
      ];
      $build['ab_tests']['view'] = $ab_tests_view;
    }

    $build['per_round_data'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'dashboard-section dashboard-section__per-round-data',
      ],
    ];
    $build['per_round_data']['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Round Data'),
    ];

    $build['per_round_data']['#attached']['library'][] = 'muser_system/dashboard';
    $build['per_round_data']['round_selector'] = \Drupal::formBuilder()->getForm('\Drupal\muser_system\Form\RoundSelectForm');

    if (!$round_nid = \Drupal::request()->get('round')) {
      $round_nid = $current_round->id();
    }

    $build['#cache'] = [
      'tags' => [
        'node:' . $round_nid,
        'current_round',
        'config:muser_system.settings',
      ],
    ];

    $period = muser_project_get_current_period($round_nid);
    if ($round_nid != $current_round->id() && !$period) {
      $build['per_round_data']['no_data'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            $this->t('This round has not started yet.'),
          ],
        ],
      ];
      \Drupal::moduleHandler()->alter('muser_admin_dashboard', $build, $round_nid);
      return $build;
    }

    $build['per_round_data']['stats'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'dashboard-stats',
        'class' => 'dashboard-section dashboard-section__per-round-data__stats',
      ],
      [
        '#theme' => 'muser_dashboard_stats',
      ],
    ];

    if (\Drupal::service('settings')::get('ignore_date_checking') && \Drupal::request()->get('ignore_dates')) {
      $period = 'after';
    }

    switch ($period) {

      case 'posting':
        $chart_list = [
          'projects-posted-by-day',
          'project-openings-posted-by-day',
          'projects-for-student-primary-interests',
          'projects-by-category',
          'projects-by-years',
          'projects-by-compensation',
          'openings-for-student-primary-interests',
          'openings-by-category',
          'openings-by-years',
          'openings-by-compensation',
          'actions-by-day',
          'logins-by-day',
        ];
        break;

      case 'application':
        $chart_list = [
          'student-actions-by-day',
          'applicants-by-compensation',
          'applicants-by-years',
          'projects-posted-by-day',
          'project-openings-posted-by-day',
          'projects-for-student-primary-interests',
          'projects-by-category',
          'projects-by-years',
          'projects-by-compensation',
          'openings-for-student-primary-interests',
          'openings-by-category',
          'openings-by-years',
          'openings-by-compensation',
          'actions-by-day',
          'logins-by-day',
        ];
        break;

      case 'acceptance':
        $chart_list = [
          'mentor-actions-by-day',
          'projects-posted-by-day',
          'project-openings-posted-by-day',
          'projects-for-student-primary-interests',
          'projects-by-category',
          'projects-by-years',
          'projects-by-compensation',
          'openings-for-student-primary-interests',
          'openings-by-category',
          'openings-by-years',
          'openings-by-compensation',
          'student-actions-by-day',
          'applicants-by-compensation',
          'applicants-by-years',
          'actions-by-day',
          'logins-by-day',
        ];
        break;

      case 'contract':
        $chart_list = [
          'agreement-actions-by-day',
          'projects-posted-by-day',
          'project-openings-posted-by-day',
          'projects-for-student-primary-interests',
          'projects-by-category',
          'projects-by-years',
          'projects-by-compensation',
          'openings-for-student-primary-interests',
          'openings-by-category',
          'openings-by-years',
          'openings-by-compensation',
          'student-actions-by-day',
          'applicants-by-compensation',
          'applicants-by-years',
          'mentor-actions-by-day',
          'actions-by-day',
          'logins-by-day',
        ];
        break;

      case 'before':
        $chart_list = [
          'actions-by-day',
          'logins-by-day',
        ];
        break;

      case 'after':
      default:
        $chart_list = [
          'projects-posted-by-day',
          'project-openings-posted-by-day',
          'projects-for-student-primary-interests',
          'projects-by-category',
          'projects-by-years',
          'projects-by-compensation',
          'openings-for-student-primary-interests',
          'openings-by-category',
          'openings-by-years',
          'openings-by-compensation',
          'student-actions-by-day',
          'applicants-by-compensation',
          'applicants-by-years',
          'mentor-actions-by-day',
          'agreement-actions-by-day',
          'actions-by-day',
          'logins-by-day',
        ];
        break;

    }

    $config = $this->configFactory->get('muser_system.settings');
    if (!$charts_to_show = array_keys(array_filter($config->get('dashboard_charts_to_show', []) ?? []))) {
      // None set. Use default.
      $charts_to_show = [
        'projects-for-student-primary-interests',
        'actions-by-day',
        'agreement-actions-by-day',
        'mentor-actions-by-day',
        'openings-for-student-primary-interests',
        'projects-posted-by-day',
        'student-actions-by-day',
      ];
    }
    $chart_list = array_intersect($chart_list, $charts_to_show);

    $this->getCharts($build['per_round_data'], $chart_list);
    if (!_muser_system_contracts_enabled()) {
      $build['per_round_data']['agreement-actions-by-day']['#access'] = FALSE;
    }

    \Drupal::moduleHandler()->alter('muser_admin_dashboard', $build, $round_nid);

    return $build;

  }

  protected function getScheduledEmailsInfo($current_round) {

    $mailer = new ScheduledEmails($this->configFactory->get('muser_system.settings'), $this->state(), \Drupal::time(), $current_round);
    $schedule = $mailer->getEmailSchedule();
    $field_defs = $current_round->getFieldDefinitions();

    $sorted = [];
    foreach ($schedule as $key => $info) {
      $info['type'] = $key;
      if ($field = $field_defs[$info['field']] ?? NULL) {
        $info['label'] = str_replace($this->t('email sent'), '', $field->getLabel());
      } else {
        $info['label'] = $info['index'];
      }
      $id = str_pad($info['scheduled_timestamp'], 15, '0', STR_PAD_LEFT)
        . '|' . str_pad($info['index'], 3, '0', STR_PAD_LEFT);
      // Show emails to be sent in the next 7 days.
      $info['soon'] = ($info['status'] == 'future' && $info['until_send'] < (7 * self::SECONDS_PER_DAY));
      $sorted[$id] = $info;
    } // Loop thru email info.

    ksort($sorted);

    $items = [];
    $upcoming_emails = [];
    $counts = [
      'sent' => 0,
      'future' => 0,
      'error' => 0,
    ];
    foreach ($sorted as $id => $info) {
      switch ($info['status']) {
        case 'sent':
          $emoji = '✅';
          $status = $this->t('Sent. Scheduled %date', ['%date' => $info['scheduled']]);
          break;
        case 'future':
          $emoji = '⏱️';
          $status = $this->t('To be sent %date', ['%date' => $info['scheduled']]);
          break;
        case 'error':
          $emoji = '❌';
          $status = $this->t('Error:  @error', ['@error' => $info['error']]);
          break;
      }
      $counts[$info['status']]++;
      $line = $this->t('@emoji @label - @status', [
        '@emoji' => $emoji,
        '@label' => $info['label'],
        '@status' => $status,
      ]);
      $classes = ['email-status--' . $info['status']];
      if ($info['soon']) {
        $upcoming_emails[] = $this->t('@label (<span title="@date">In @until</span>)', [
          '@label' => $info['label'],
          '@until' => $info['until'],
          '@date' => $info['scheduled'],
        ]);
        $classes[] = 'email-status--soon';
      }
      $items[] = [
        '#wrapper_attributes' => ['class' => $classes],
        '#markup' => $line,
      ];
    }

    return [
      'upcoming_emails' => $upcoming_emails,
      'items' => $items,
      'counts' => $counts,
    ];

  }

  protected function getDatesText($date_field, $start_only = FALSE) {
    $user_tz = new \DateTimeZone(date_default_timezone_get());
    if (!$date_field || $date_field->isEmpty()) {
      return FALSE;
    }
    $values = $date_field->getValue();
    $date = new DrupalDateTime($values['value'], DateTimeItemInterface::STORAGE_TIMEZONE);
    $date->setTimezone($user_tz);
    $text = $date->format('l, F j \a\t g:ia');
    if (!$start_only) {
      $date = new DrupalDateTime($values['end_value'], DateTimeItemInterface::STORAGE_TIMEZONE);
      $date->setTimezone($user_tz);
      $text .= ' - ' . $date->format('l, F j \a\t g:ia');
    }
    return $text;
  }

  protected function getCharts(&$build, $chart_list) {
    /** @var \Drupal\muser_system\Charts $charts */
    $charts = \Drupal::service('muser_system.charts');
    foreach ($chart_list as $chart_id) {
      $query_string = '';
      if ($chart_id == 'projects-for-student-primary-interests' || $chart_id == 'openings-for-student-primary-interests') {
        $query_string = 'num=99';
      }
      $build[$chart_id] = $charts->getChartMarkup($chart_id, $query_string);
    }
  }

  /**
   * Show all charts.
   *
   * @return array
   *   Build array.
   */
  public function allCharts() {
    $build = [];

    $build['help'] = [
      '#type' => 'details',
      '#title' => $this->t('Help'),
      '#attributes' => [
        'id' => 'top',
      ],
      '#open' => FALSE,
    ];
    $build['help']['info'] = [
      '#type' => 'container',
      [
        '#markup' => '<h3>' . $this->t('Adding charts to content') . '</h3>'
        . '<p>' . $this->t('Add charts to pages and blog posts by adding a token to the body like:') . '<br><em>[muser:chart:chart-id]</em></p>'
        . '<p>' . $this->t('See the chart IDs for various charts below.') . '</p>'
        . '<p><strong>' . $this->t('Available options:') . '</strong></p>'
        . '<p>' . $this->t('To show a chart for a particular round, add the round ID to the token:') . '<br><em>[muser:chart:chart-id:round=123]</em></p>'
        . '<p>' . $this->t('For non-date-based charts (e.g. Projects by Category), you may specify the number of items to show:') . '<br><em>[muser:chart:chart-id:num=50]</em></p>'
        . '<p>' . $this->t('For category charts, you may sort by name rather than the count:') . '<br><em>[muser:chart:chart-id:sort=name]</em></p>'
        . '<p>' . $this->t('To specify multiple options for a single chart, separate each option with &') . '<br><em>[muser:chart:chart-id:round=123&num=50&sort=name]</em></p>',
      ],
    ];

    $build['links'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Available charts'),
      '#items' => [],
      '#attributes' => ['class' => 'link-list'],
    ];

    $current_round = muser_project_get_current_round(TRUE);
    $build['per_round_data'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'dashboard-section dashboard-section__per-round-data',
      ],
    ];
    $build['per_round_data']['#attached']['library'][] = 'muser_system/dashboard';
    $build['per_round_data']['round_selector'] = \Drupal::formBuilder()->getForm('\Drupal\muser_system\Form\RoundSelectForm');

    if (!$round_nid = \Drupal::request()->get('round')) {
      $round_nid = ($current_round) ? $current_round->id() : 0;
    }
    $build['#cache'] = [
      'tags' => [
        'node:' . $round_nid,
        'current_round',
      ],
    ];

    $single_chart = \Drupal::request()->get('chart');
    $top_link = $this->t('<a class="top" href="#top">Back to top</a>');

    /** @var \Drupal\muser_system\Charts $charts */
    $charts = \Drupal::service('muser_system.charts');

    foreach ($charts->chartsInfo as $chart_id => $chart_info) {

      if ($single_chart && $single_chart != $chart_id) {
        // Only want to show one and this isn't it.
        continue;
      }

      if (str_contains($chart_id, 'agreement') && !_muser_system_contracts_enabled()) {
        // Skip agreement charts if not enabled.
        continue;
      }

      $link_id = 'link--' . $chart_id;
      $build['links']['#items'][] = $this->t('<a href="#@id">@label</a>', [
        '@id' => $link_id,
        '@label' => $chart_info['label'],
      ]);

      $build[$chart_id] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => $link_id,
          'class' => ['muser-chart-demo-container'],
        ],
      ];

      $parameters = '';
      if (!empty($chart_info['args']['required_parameters'])) {
        $parameters = ':' . implode('=?&', $chart_info['args']['required_parameters']) . '=?';
      }

      $build[$chart_id][$chart_id . '_token'] = [
        '#markup' => $this->t('<strong>Token:</strong> @token', ['@token' => '[muser:chart:' . $chart_id . $parameters . ']']),
      ];

      if (empty($chart_info['args']['required_parameters'])) {
        $build[$chart_id][$chart_id . '_chart'] = $charts->getChartMarkup($chart_id);
      }
      else {
        // Can't demo this chart because parameters are needed.
        $build[$chart_id][$chart_id . '_title'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['muser-chart-label'],
          ],
          '#value' => $chart_info['label'],
        ];
        $params = implode(', ', $chart_info['args']['required_parameters']);
        $build[$chart_id][$chart_id . '_note'] = [
          '#type' => 'markup',
          '#markup' => $this->formatPlural(count($chart_info['args']['required_parameters']),
            'No demo for this chart because %params parameter is required.',
            'No demo for this chart because parameters (%params) are required.',
            ['%params' => $params]),
        ];
      }

      $build[$chart_id][$chart_id . '_back_to_top'] = [
        '#type' => 'container',
        'link' => [
          '#type' => 'markup',
          '#markup' => $top_link,
        ],
      ];

    } // Loop thru charts.

    return $build;
  }

}
