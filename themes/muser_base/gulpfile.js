const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const babel = require('gulp-babel');
const concat = require('gulp-concat');

const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const sassGlob = require('gulp-sass-glob');

const del = require('del');

const paths = {
  styles: {
    src: 'assets/scss/**/*.scss',
    dest: 'css/'
  },
  scripts: {
    src: 'assets/js/**/*.js',
    dest: 'js/'
  }
};

function clean() {
  // You can use multiple globbing patterns as you would with `gulp.src`,
  // for example if you are using del 2.0 or above, return its promise
  return del([ 'css', 'js' ]);
}


/*
 * Define our tasks using plain functions
 */
function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(cleanCSS())
    // pass in options to the stream
    // .pipe(rename({
    //   basename: 'styles',
    //   suffix: '.min'
    // }))
    .pipe(gulp.dest(paths.styles.dest));
}

function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    .pipe(babel())
    // .pipe(concat('main.min.js'))
    .pipe(gulp.dest(paths.scripts.dest));
}


const build = gulp.series(clean, gulp.parallel(styles, scripts));

function watch() {
  build();
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, styles);
}

exports.watch = watch
exports.default = build;
