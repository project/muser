<?php

namespace Drupal\muser_project\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'terms_with_icons' formatter.
 *
 * @FieldFormatter(
 *   id = "terms_with_icons",
 *   label = @Translation("Terms with Icons"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TermsWithIcons extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    if (!$terms = $this->getEntitiesToView($items, $langcode)) {
      return $elements;
    }

    foreach ($items as $index => $item) {
      $term = $terms[$index];
      if (!$icon = $term->field_icon->value) {
        $icon = 'fas fa-tag';
      }
      $elements[] = [
        '#markup' => $this->t('<i class="@icon"></i> <span class="term">@name</span>', [
          '@icon' => $icon,
          '@name' => $term->label(),
        ]),
      ];
    } // Loop thru items.

    return $elements;

  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for taxonomy terms.
    return $field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'taxonomy_term';
  }

}
