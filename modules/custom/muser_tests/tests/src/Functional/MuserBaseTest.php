<?php

namespace Drupal\Tests\muser_tests\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Contains various tests for checking the functionality of a muser website.
 */
class MuserBaseTest extends BrowserTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node'];

  /**
   * We use the muser profile because we want to test the muser website.
   *
   * @var string
   */
  protected $profile = 'muser';

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'muser_base';

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @var bool
   *
   * @see \Drupal\Core\Config\Development\ConfigSchemaChecker
   */
  protected $strictConfigSchema = FALSE;

  /**
   * An array for containing the various roles.
   *
   * @var array|string[]
   */
  protected $roles = [];

  /**
   * Sets up the other test functions.
   */
  protected function setUp() {
    parent::setUp();
    $random_string = uniqid();
    $this->roles = [
      'anonymous' => $random_string . '|anonymous/site admin|' . $random_string,
      'student' => $random_string . '|student|' . $random_string,
      'mentor' => $random_string . '|mentor|' . $random_string,
      'site_admin' => $random_string . '|anonymous/site admin|' . $random_string,
    ];
  }

  /**
   * Tests that the home page loads.
   */
  public function testHomePageStatus() {

    foreach ($this->roles as $role => $value) {

      $this->loginWithRole($role);

      // Checking home page.
      $this->drupalGet('<front>');
      $this->assertSession()->statusCodeEquals(200);

    } // Loop through roles.

  }

  /**
   * Tests that the home page shows the correct hero message.
   */
  public function testHomePageHero() {

    // Set hero message for each role.
    $config = $this->config('muser_system.settings');
    foreach ($this->roles as $role => $value) {

      if ($role == 'site_admin') {
        continue;
      }

      $message = [
        'value' => $value,
        'format' => 'basic_html',
      ];
      $config->set('hero_block_' . $role, $message);

    }
    $config->save();

    // Create user and log in as each role and then check hero message at
    // homepage.
    foreach ($this->roles as $role => $value) {

      $this->loginWithRole($role);

      // Checking home page.
      $this->drupalGet('<front>');

      $this->assertSession()->pageTextContains($value);

    } // Loop through roles.
  }

  /**
   * Tests that a nonexistent page shows the correct status code.
   */
  public function testPageNotFound() {

    $this->loginWithRole('anonymous');

    // Checking for page that does not exist.
    $this->drupalGet('page/not/found');
    $this->assertSession()->statusCodeEquals(404);

  }

  /**
   * Logs in as a given role.
   *
   * @param string $role
   *   The chosen role.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function loginWithRole($role) {
    $account = $this->drupalCreateUser();
    if ($role != 'anonymous') {
      $account->addRole($role);
      $account->save();
    }
    $this->drupalLogin($account);
  }

}
