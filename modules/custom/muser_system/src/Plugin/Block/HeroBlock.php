<?php

namespace Drupal\muser_system\Plugin\Block;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Provides a 'Hero' block.
 *
 * @Block(
 *  id = "muser_hero_block",
 *  admin_label = @Translation("Muser hero block"),
 * )
 */
class HeroBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * HeroText constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = User::load($current_user->id());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => [
          'config:muser_system.settings',
        ],
      ],
    ];

    $config = $this->configFactory->get('muser_system.settings');
    if (!empty($config->get('hero_block_display_order'))
      && $config->get('hero_block_display_order') == 'carousel_then_message') {
      $this->buildCarousel($build, $config->get('hero_block_carousel_bid'));
      $this->buildHeroMessage($build, $config);
    }
    else {
      $this->buildHeroMessage($build, $config);
      $this->buildCarousel($build, $config->get('hero_block_carousel_bid'));
    }

    return $build;

  }

  /**
   * Add the carousel to the build array if needed.
   *
   * @param array $build
   *   The build array.
   * @param int $bid
   *   The block id.
   */
  private function buildCarousel(array &$build, $bid) {
    if ($bid) {
      $block = BlockContent::load($bid);
      $build[] = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block);
    } // Show carousel?
  }

  /**
   * Add the hero message to the build array if needed.
   *
   * @param array $build
   *   The build array.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The config object.
   */
  private function buildHeroMessage(array &$build, ImmutableConfig $config) {
    if ($config->get('show_hero_block_messages')) {
      $round = NULL;
      if ($round_nid = muser_project_get_current_round()) {
        $round = Node::load($round_nid);
      }
      $build[] = muser_system_get_hero_text($this->currentUser, $round, $config);
      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($build, $round);
      $renderer->addCacheableDependency($build, $this->currentUser);
    } // Show messages?
  }

}
