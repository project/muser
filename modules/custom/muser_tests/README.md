1. Copy phpunit.xml.dist as phpunit.xml 'cp phpunit.xml.dist phpunit.xml'
2. Create output directory as shown in BROWSER_OUTPUT_DIRECTORY in phpunit.xml.dist 'mkdir web/sites/simpletest/browser_output'
3. Change directory to directory that contains phpunit.xml
4. If you are using lando run './vendor/bin/phpunit web/profiles/muser/modules/custom/muser_tests/tests/'
