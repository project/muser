<?php

declare(strict_types=1);

namespace Drupal\oneclick_unsubscribe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a One-Click Unsubscribe form.
 */
final class UnsubscribeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'oneclick_unsubscribe_unsubscribe';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL): array {

    $post_vars = $request->request->all();
    $get_vars = $request->query->all();

    $mail = $get_vars['mail'] ?? '';
    $checksum = $get_vars['checksum'] ?? '';

    if (!$mail || $checksum != oneclick_unsubscribe_get_checksum($mail, $get_vars, $post_vars)) {
      $form['messages'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'error' => [
            $this->t('Invalid unsubscription request.'),
          ],
        ],
      ];
      return $form;
    }

    $form['#action'] = Url::fromRoute('oneclick_unsubscribe.unsubscribe', [], ['query' => $get_vars])->toString();

    $form['confirmation'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you want to unsubscribe the email address %email?', ['%email' => $mail]),
      '#parents' => [],
    ];

    $form['List-Unsubscribe'] = [
      '#type' => 'hidden',
      '#value' => 'One-Click',
    ];
    $form['redirect'] = [
      '#type' => 'hidden',
      '#value' => '1',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Unsubscribe'),
      ],
      'cancel' => [
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => Url::fromRoute('<front>'),
        '#attributes' => [
          'class' => [
            'button',
          ],
        ],
      ],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Do nothing.
  }

}
